#pragma once

#include <glm\glm.hpp>
#include <memory>
#include <vector>

#include "ComponentBase.h"
#include "EntityComponentManager.h"

class PositionComponent : public ComponentBase
{
public:
	/**
	* Constructs a new PositionComponent
	*
	* @param entityId Parent id
	*/
	PositionComponent(std::size_t entityId);
	PositionComponent();
	~PositionComponent();

	/**
	* Recieves a message and processes it
	*
	* @param message The message
	* @param data Optional data paremeter
	*/
	void recieveMessage(char message[], void* data);

	/**
	* Set up the position component by giving it a heightmap and tileSize.
	* This is used to calculate Y position
	*
	* @param heightMap Heightmap
	* @param tileSize Size of a tile in the heightmap
	*/
	void setUp(std::shared_ptr<std::vector<std::vector<int>>> heightMap, int tileSize);

	/**
	* Sets position of the entity
	*
	* @param position New position
	*/
	void setPosition(glm::vec2& position);

	/**
	* Retuns the position of the entity
	*
	* @return Entity's position
	*/
	glm::vec3& getPosition();

private:
	void updatePos(glm::vec2& offset);

	glm::vec3 position;

	std::shared_ptr<std::vector<std::vector<int>>> heightMap;
	int tileSize;
};
