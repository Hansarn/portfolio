#include "PathingComponent.h"

PathingComponent::PathingComponent()
{}

PathingComponent::PathingComponent(std::size_t entityId) : ComponentBase(entityId)
{}

PathingComponent::~PathingComponent()
{}

void PathingComponent::recieveMessage(char message[], void * data)
{
	if (strcmp(message, "MoveHere") == 0)
	{
		path = AStar::aStar(EntityComponentManager::getInstance().getEntityPos(parentId), *(glm::vec2*)data);
		pathIndex = 0;
		if (path.size() != 0)
		{
			EntityComponentManager::getInstance().sendMessageToComponent(PHYSICS_COMPONENT, parentId, "MoveHere", (void*)&path[pathIndex++]);
		}
	}
	else if (strcmp(message, "ReachedDestination") == 0)
	{
		if (path.size() != pathIndex)
		{
			EntityComponentManager::getInstance().sendMessageToComponent(PHYSICS_COMPONENT, parentId, "MoveHere", (void*)&path[pathIndex++]);
		}
		else
		{
			EntityComponentManager::getInstance().sendMessageToComponent(PHYSICS_COMPONENT, parentId, "Stop");
			EntityComponentManager::getInstance().sendMessageToComponent(INPUT_COMPONENT, parentId, "InputCompleted");
		}
	}
}