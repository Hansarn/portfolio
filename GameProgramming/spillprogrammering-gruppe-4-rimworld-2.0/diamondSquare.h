#pragma once
#include <iostream>
#include <vector>
#include <random>
#include <algorithm>

namespace diamondSquare
{
	/**
	* Does diamond square with the given square, range and optional values
	*
	* @param size The size of the diamond square
	* @param range Range of the diamond square
	* @param values Optional corner values for diamond square
	*/
	std::vector<std::vector<int>> diamondSquare(int size, int range, int values[4]);
}