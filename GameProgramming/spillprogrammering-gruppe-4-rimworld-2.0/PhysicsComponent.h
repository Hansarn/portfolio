#pragma once

#include "ComponentBase.h"
#include "EntityComponentManager.h"

class PhysicsComponent : public ComponentBase
{
public:
	/**
	* Constructs a new PhysicsComponent
	*
	* @param entityId Parent id
	*/
	PhysicsComponent(std::size_t& entityId);
	PhysicsComponent();
	~PhysicsComponent();

	/**
	* Recieves a message and processes it
	*
	* @param message The message
	* @param data Optional data paremeter
	*/
	void recieveMessage(char message[], void* data);

	/**
	* Updates the physics, aka moves the entity
	*
	* @param deltaTime Time since last physics update
	*/
	void update(float deltaTime);
private:
	glm::vec2 velocity = { 0.0f, 0.0f };
	float speed = 0.0f;
	float dist;
};
