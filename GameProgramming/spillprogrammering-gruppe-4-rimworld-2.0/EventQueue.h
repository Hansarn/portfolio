#pragma once

#include <vector>

template <class T>
/**
* the event queue for
*/
class EventQueue
{
private:
	T* eventQueue;

	int head;
	int tail;

public:
	const int size = 20;
	/**
	* Create an eventqueue with a max size of 20
	*/
	EventQueue()
	{
		eventQueue = new T[size];
	}

	~EventQueue()
	{
		delete[] eventQueue;
	}
	/**
	* pulls event out of the queue and puts it in the param
	* @param is where the pulled event is placed
	*/
	bool pullEvent(T& event)
	{
		if (head == tail)
		{
			return false;
		}

		event = eventQueue[head];

		head = (head + 1) % size;

		return true;
	}
	/**
	* pushed the event on to back of the queue
	* @param event to be put in the queue
	*/
	void pushEvent(T& event)
	{
		eventQueue[tail] = event;
		tail = (tail + 1) % size;
	}
};
