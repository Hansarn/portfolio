#include "Map.h"

Map::Map()
{
	std::random_device rd;
	randomGenerator = std::mt19937_64(rd());
	std::uniform_int_distribution<int> distributorResources(0, Resources::Resource::NUM_OF_RESOURCES - 1);
	std::uniform_int_distribution<int> distributorTerrain(0, NUM_OF_TERRAIN_TYPES - 1);
	std::uniform_real_distribution<double> distributorHelper(0, 1000.0);
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> colors;
	std::vector<unsigned int> indices;

	int values[4] = { 50, 50, -200, -200 };

	int range = 100;
	std::vector<std::vector<int>> temp = diamondSquare::diamondSquare(9, range, values);
	heightMap = std::make_shared<std::vector<std::vector<int>>>(temp);
	randomGen::PerlinNoise perlin;

	std::vector<std::vector<bool>> lakeMap;
	std::vector<std::vector<terrain>> map;
	std::vector<uint32_t> resMap;
	std::vector<std::vector<int>> treeMap;

	map.resize(temp.size());
	for (auto i = 0; i < map.size(); ++i)
	{
		map[i].resize(temp[i].size());
	}
	//resMap.resize(temp.size() * temp.size() * 4, -1);

	double resourceValues[Resources::NUM_OF_RESOURCES];

	for (int i = 0; i < Resources::NUM_OF_RESOURCES; ++i)
	{
		resourceValues[i] = distributorHelper(randomGenerator);
	}

	lakeMap = mapGeneration::generateWater(temp, 10, 10);

	map = terrainGenerator::generateMap(9, temp, lakeMap);
	//	mapGeneration::terrainGen(map, temp, lakeMap, 100, 1);

	std::vector<uint32_t> data;
	data.resize(map.size() * map.size());

	for (size_t x = 0; x < map.size() - 1; ++x)
	{
		std::vector<Tile> tempV;
		for (size_t y = 0; y < map[x].size() - 1; ++y)
		{
			Tile tempT;

			data[x * map.size() + y] = (int)map[x][y];
			tempT.ter = map[x][y];
			tempT.height = temp[x][y];

			//tempT.walkSpeed = temp[x][y];
			tempT.walkSpeed = 10;

			int resource = -1;

			for (int i = 0; i < Resources::NUM_OF_RESOURCES; ++i)
			{
				double value = perlin.noise((double)x / 10.0 + resourceValues[i], (double)y / 10.0 + resourceValues[i]);

				if (resource == -1 && value > 0.3)
				{
					resource = i;
				}
			}

			if (resource != -1)
			{
				tempT.res = (Resources::Resource)resource;
			}
			else
			{
				tempT.res = Resources::Resource::NUM_OF_RESOURCES;
			}

			tempT.buildable = true;
			tempV.push_back(tempT);

			vertices.push_back(glm::vec3(x * size, temp[x][y], y * size));
			vertices.push_back(glm::vec3((x + 1) * size, temp[x + 1][y], y * size));
			vertices.push_back(glm::vec3(x * size, temp[x][y + 1], (y + 1) * size));
			vertices.push_back(glm::vec3((x + 1) * size, temp[x + 1][y + 1], (y + 1) * size));

			colors.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
			colors.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
			colors.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
			colors.push_back(glm::vec3(1.0f, 1.0f, 0.0f));

			resMap.push_back(resource);
			resMap.push_back(resource);
			resMap.push_back(resource);
			resMap.push_back(resource);

			indices.push_back(vertices.size() - 4);
			indices.push_back(vertices.size() - 3);
			indices.push_back(vertices.size() - 2);

			indices.push_back(vertices.size() - 3);
			indices.push_back(vertices.size() - 2);
			indices.push_back(vertices.size() - 1);
		}
		tiles.push_back(tempV);
	}

	//Really didn't want to use pointers, but c++ reeeeaaally wanted it
	grassBundle = new GrassBundle(map, temp, size);

	amountOfIndices = indices.size();

	glGenVertexArrays(1, &vertexArrayObject);
	glGenBuffers(1, &vertexBufferObject);
	glGenBuffers(1, &elementBufferObject);
	glGenBuffers(1, &colorBufferObject);
	glGenBuffers(1, &resourceBufferObject);
	glBindVertexArray(vertexArrayObject);

	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertices[0]), &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GL_FLOAT), NULL);

	glBindBuffer(GL_ARRAY_BUFFER, colorBufferObject);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(colors[0]), &colors[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GL_FLOAT), NULL);

	glBindBuffer(GL_ARRAY_BUFFER, resourceBufferObject);
	glBufferData(GL_ARRAY_BUFFER, resMap.size() * sizeof(resMap[0]), &resMap[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(2);
	glVertexAttribIPointer(2, 1, GL_INT, sizeof(resMap[0]), NULL);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(indices[0]), &indices[0], GL_STATIC_DRAW);

	glBindVertexArray(0);

	groundTextureUnit = textureHandler::loadFromFile("./spillprogrammering-gruppe-4-rimworld-2.0/rimworld 2 sprites/groundTexture.jpg");
	glGenTextures(1, &terrainIndexTexture);

	glBindTexture(GL_TEXTURE_2D, terrainIndexTexture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);	// set texture wrapping to GL_REPEAT (default wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI, map.size(), map.size(), 0, GL_RED_INTEGER, GL_UNSIGNED_INT, data.data());

	subTile tile = { sf::Vector2i(1,1), sf::Vector2i(tiles.size(),tiles[0].size()) , sf::Vector2i(tiles.size() / 2,tiles[0].size() / 2) };
	subTiles.push_back(tile);

	for (size_t i = 0; i < 15; i++)
	{
		makePawn();
	}
}

Map::~Map()
{
	delete grassBundle;
}

void Map::update(Resources& resources)
{
	//for (size_t x = 0; x < subTiles.size(); x++)
	//{
	//	if (subTiles[x].entities.size() > MAXPAWNS)
	//	{
	//		subDivide(x);
	//	}
	//}
	int pawns = entities.size();
	for (building b : buildings.buildings)
	{
		pawns += b.entities.size();
	}

	int entitiesKillNr = 0;
	for (size_t i = 0; i < pawns; i++)
	{
		int foodlost = 0;
		while (foodlost < 5)
		{
			int noFood = 0;
			for (size_t j = Resources::FISH; j < Resources::FISH + 5; j++)
			{
				if (foodlost < 5 && resources.addTradeGood((Resources::TradeGoods)j, -1))
				{
					foodlost++;
				}
				else
				{
					noFood++;
				}
			}
			if (noFood == 5 && foodlost != 5)
			{
				entitiesKillNr++;
				break;
			}
		}
	}
	if (entities.size() >= entitiesKillNr)
	{
		for (int i = 0; i < entitiesKillNr; ++i)
		{
			std::size_t entity = entities.back();
			EntityComponentManager::getInstance().deleteEntity(entity);
			entities.pop_back();
		}
	}
	else
	{
		std::size_t size = entities.size();
		for (int i = 0; i < size; ++i)
		{
			std::size_t entity = entities.back();
			EntityComponentManager::getInstance().deleteEntity(entity);
			entities.pop_back();
		}
		entitiesKillNr -= size;

		for (building b : buildings.buildings)
		{
			if (b.entities.size() >= entitiesKillNr)
			{
				for (int i = 0; i < entitiesKillNr; ++i)
				{
					std::size_t entityToKill = b.entities.back();
					EntityComponentManager::getInstance().deleteEntity(entityToKill);
					b.entities.pop_back();
				}
				entitiesKillNr = 0;
			}
			else
			{
				size = b.entities.size();
				for (int i = 0; i < size; ++i)
				{
					std::size_t entityToKill = b.entities.back();
					EntityComponentManager::getInstance().deleteEntity(entityToKill);
					b.entities.pop_back();
				}
				entitiesKillNr -= size;
			}
		}
	}
}

void Map::updateWorkerCount(int building, unsigned int newWorkerCount, Resources& resources)
{
	buildings.buildings[building].allowedEntities = newWorkerCount;

	while (entities.size() > 0 && buildings.buildings[building].entities.size() < newWorkerCount)
	{
		buildings.buildings[building].entities.push_back(entities[0]);
		InputComponent* component = (InputComponent*)EntityComponentManager::getInstance().getComponent(INPUT_COMPONENT, entities[0]);
		component->moveHere(glm::vec2(buildings.buildings[building].position.x * size, buildings.buildings[building].position.y * size), true);
		entities[0] = entities.back();
		entities.pop_back();

		ItemSet temp = buildings.buildings[building].type.levels[0].outPut;

		for (std::size_t j = 0; j < temp.resWares.size(); ++j)
		{
			resources.addResourcePerTick(temp.resWares[j].resItem, temp.resWares[j].amount / buildings.buildings[building].type.levels[0].maxPawns);
		}

		for (std::size_t j = 0; j < temp.tradeWares.size(); ++j)
		{
			resources.addTradeGoodsPerTick(temp.tradeWares[j].tradeItem, temp.tradeWares[j].amount / buildings.buildings[building].type.levels[0].maxPawns);
		}

		ItemSet tempCost = buildings.buildings[building].type.levels[0].consumed;

		for (std::size_t j = 0; j < tempCost.resWares.size(); ++j)
		{
			resources.addResourceLossPerTick(tempCost.resWares[j].resItem, tempCost.resWares[j].amount / buildings.buildings[building].type.levels[0].maxPawns);
		}

		for (std::size_t j = 0; j < tempCost.tradeWares.size(); ++j)
		{
			resources.addTradeGoodsLossPerTick(tempCost.tradeWares[j].tradeItem, tempCost.tradeWares[j].amount / buildings.buildings[building].type.levels[0].maxPawns);
		}
	}

	while (buildings.buildings[building].entities.size() > newWorkerCount)
	{
		entities.push_back(buildings.buildings[building].entities[0]);
		InputComponent* component = (InputComponent*)EntityComponentManager::getInstance().getComponent(INPUT_COMPONENT, buildings.buildings[building].entities[0]);
		component->moveHere(glm::vec2(buildings.buildings[building].position.x * size, buildings.buildings[building].position.y * size));
		buildings.buildings[building].entities[0] = buildings.buildings[building].entities.back();
		buildings.buildings[building].entities.pop_back();

		ItemSet temp = buildings.buildings[building].type.levels[0].outPut;

		for (std::size_t j = 0; j < temp.resWares.size(); ++j)
		{
			resources.addResourcePerTick(temp.resWares[j].resItem, -temp.resWares[j].amount / buildings.buildings[building].type.levels[0].maxPawns);
		}

		for (std::size_t j = 0; j < temp.tradeWares.size(); ++j)
		{
			resources.addTradeGoodsPerTick(temp.tradeWares[j].tradeItem, -temp.tradeWares[j].amount / buildings.buildings[building].type.levels[0].maxPawns);
		}

		ItemSet tempCost = buildings.buildings[building].type.levels[0].outPut;

		for (std::size_t j = 0; j < tempCost.resWares.size(); ++j)
		{
			resources.addResourceLossPerTick(tempCost.resWares[j].resItem, -tempCost.resWares[j].amount / buildings.buildings[building].type.levels[0].maxPawns);
		}

		for (std::size_t j = 0; j < tempCost.tradeWares.size(); ++j)
		{
			resources.addTradeGoodsLossPerTick(tempCost.tradeWares[j].tradeItem, -tempCost.tradeWares[j].amount / buildings.buildings[building].type.levels[0].maxPawns);
		}
	}
}

void Map::drawTilesOnly(GLuint shaderProgram, int houseId)
{
	textureHandler::bindTexture(groundTextureUnit, GL_TEXTURE_2D, 0);
	glUniform1i(glGetUniformLocation(shaderProgram, "texture0"), 0);

	textureHandler::bindTexture(terrainIndexTexture, GL_TEXTURE_2D, 1);
	glUniform1i(glGetUniformLocation(shaderProgram, "texture1"), 1);

	int resourceToDraw = -1;

	if (houseId != -1)
	{
		resourceToDraw = (int)buildings.buildingTypes[houseId].tileReq;
	}

	glUniform1i(glGetUniformLocation(shaderProgram, "tileToDraw"), resourceToDraw);

	glUniform1i(glGetUniformLocation(shaderProgram, "allResources"), (int)Resources::NUM_OF_RESOURCES);

	glBindVertexArray(vertexArrayObject);

	glDrawElements(GL_TRIANGLES, amountOfIndices, GL_UNSIGNED_INT, NULL);

	glBindVertexArray(0);
}

void Map::drawBuildings(GLuint shaderProgram, int level)
{
	for (int i = 0; i < buildings.buildings.size(); ++i)
	{
		glm::vec3 pos;

		pos.x = buildings.buildings[i].position.x * size;
		pos.z = buildings.buildings[i].position.y * size;
		pos.y = tiles[buildings.buildings[i].position.x][buildings.buildings[i].position.y].height;

		glm::mat4 mat = glm::translate(glm::mat4(), pos);
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "model"), 1, false, glm::value_ptr(mat));
		glUniform1i(glGetUniformLocation(shaderProgram, "id"), i);

		meshManager::drawMesh(meshManager::loadMesh(buildings.buildings[i].type.levels[level].model));
	}
}

void Map::drawPlaceBuildingGhost(int selectedBuildingType, sf::Vector2i buildPos, bool canAfford, GLuint shaderProgram, int level, ResearchHandler* research)
{
	glm::vec3 color;
	if (canBuildCheck(buildPos, selectedBuildingType, canAfford, research))
	{
		color = glm::vec3(0.0f, 0.0f, 1.0f);
	}
	else
	{
		color = glm::vec3(1.0f, 0.0f, 0.0f);
	}
	glm::vec3 pos;
	pos.x = (buildPos.x / size) * size;
	pos.y = tiles[buildPos.x / size][buildPos.y / size].height;
	pos.z = (buildPos.y / size) * size;
	glm::mat4 mat = glm::translate(glm::mat4(), pos);
	glUniform3fv(glGetUniformLocation(shaderProgram, "color"), 1, glm::value_ptr(color));
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "model"), 1, false, glm::value_ptr(mat));

	meshManager::drawMesh(meshManager::loadMesh(buildings.buildingTypes[selectedBuildingType].levels[level].model));
}

void Map::drawFlora(GLuint shaderProgram, Camera& camera, double timeElapsed)
{
	grassBundle->draw(shaderProgram, camera, timeElapsed);
}

void Map::subDivide(int index)
{
	std::vector<sf::Vector2i> newTilePos;
	std::vector<sf::Vector2i> newTileSize;
	std::vector<subTile> newTiles;
	sf::Vector2i tileSize = { subTiles.at(index).size.x / 2, subTiles.at(index).size.y / 2 };

	subTile tempTile;

	tempTile.size = tileSize;

	for (size_t i = 0; i < posNeg.size(); i++)
	{
		tempTile.position = { subTiles.at(index).position.x - (tileSize.x / 2 * posNeg.at(i).x), subTiles.at(index).position.y - (tileSize.y / 2 * posNeg.at(i).y) };
		tempTile.posNeg = posNeg.at(i);
		newTiles.push_back(tempTile);
	}

	for (size_t x = 0; x < subTiles.at(index).entities.size(); x++)
	{
		std::size_t entity = subTiles[index].entities[x];
		glm::vec2 position = glm::vec2(EntityComponentManager::getInstance().getEntityPos(entity).x / pow(2, 4), EntityComponentManager::getInstance().getEntityPos(entity).y / pow(2, 4));
		for (size_t y = 0; y < newTiles.size(); y++)
		{
			if (position.x >= newTiles[y].position.x - (tileSize.x / 2) && position.x < newTiles[y].position.x + (tileSize.x / 2)
				&& position.y >= newTiles[y].position.y - (tileSize.y / 2) && position.y < newTiles[y].position.y + (tileSize.y / 2))
			{
				newTiles[y].entities.push_back(entity);
			}
		}
	}
	subTiles.erase(subTiles.begin() + index);
	subTiles.insert(subTiles.end(), newTiles.begin(), newTiles.end());
}

int Map::merge(int index)
{
	sf::Vector2i modifier = { subTiles.at(index).size.x * subTiles.at(index).posNeg.x,subTiles.at(index).size.y * subTiles.at(index).posNeg.y };

	subTile temp;
	temp.size = { subTiles.at(index).size.x * 2, subTiles.at(index).size.y * 2 };
	temp.position = { subTiles.at(index).position.x + (subTiles.at(index).size.x / 2 * subTiles.at(index).posNeg.x), subTiles.at(index).position.y + (subTiles.at(index).size.y / 2 * subTiles.at(index).posNeg.y) };
	std::vector<int> oldTiles;
	oldTiles.push_back(index);

	sf::Vector2i oppositeTile = { subTiles.at(index).position.x * modifier.x, subTiles.at(index).position.y * modifier.y };

	for (size_t i = 0; i < subTiles.size(); i++)
	{
		if (subTiles.at(i).position == sf::Vector2i(subTiles.at(index).position.x, oppositeTile.y) ||
			subTiles.at(i).position == sf::Vector2i(oppositeTile.x, subTiles.at(index).position.y) ||
			subTiles.at(i).position == oppositeTile)
		{
			oldTiles.push_back(i);
		}
	}
	if (oldTiles.size() < 4)
	{
		std::vector<bool> exists = { false,false,false,false };
		for (size_t x = 0; x < posNeg.size(); x++)
		{
			for (size_t y = 0; y < oldTiles.size(); y++)
			{
				if (posNeg.at(x) == subTiles.at(oldTiles.at(y)).posNeg)
				{
					exists.at(x) = true;
					y = oldTiles.size();
				}
			}
		}
		for (size_t i = 0; i < posNeg.size(); i++)
		{
			if (!exists.at(i))
			{
				oldTiles.push_back(searchForSub(sf::Vector2i(subTiles.at(index).size.x / 2, subTiles.at(index).size.y / 2),
					sf::Vector2i(temp.position.x - (subTiles.at(index).size.x / 2 * posNeg.at(i).x),
						temp.position.y - (subTiles.at(index).size.y / 2 * posNeg.at(i).y))));
			}
		}
	}
	for (size_t i = 0; i < oldTiles.size(); i++)
	{
		temp.entities.insert(temp.entities.begin(), subTiles.at(oldTiles.at(i)).entities.begin(), subTiles.at(oldTiles.at(i)).entities.end());
		subTiles.erase(subTiles.begin() + oldTiles.at(i));
	}

	if ((temp.size.x / (temp.position.x - (temp.size.x / 2))) % 2 == 0)
	{
		temp.posNeg.x = 1;
	}
	else
	{
		temp.posNeg.x = -1;
	}

	if ((temp.size.y / (temp.position.y - (temp.size.y / 2))) % 2 == 0)
	{
		temp.posNeg.y = 1;
	}
	else
	{
		temp.posNeg.y = -1;
	}

	subTiles.push_back(temp);
	return subTiles.size() - 1;
}

int Map::searchForSub(sf::Vector2i size, sf::Vector2i pos)
{
	for (size_t x = 0; x < subTiles.size(); x++)
	{
		for (size_t y = 0; y < posNeg.size(); y++)
		{
			if (subTiles.at(x).position == sf::Vector2i(pos.x + (size.x / 2 * posNeg.at(y).x), pos.y + (size.x / 2 * posNeg.at(y).y)))
			{
				return merge(x);
			}
		}
	}
	for (size_t i = 0; i < posNeg.size(); i++)
	{
		searchForSub(sf::Vector2i(size.x / 2, size.y / 2), sf::Vector2i(pos.x + (size.x / 2 * posNeg.at(i).x), pos.y + (size.x / 2 * posNeg.at(i).y)));
	}
}

int Map::countPawnsSubtiles(sf::Vector2i size, sf::Vector2i pos)
{
	int pawnCount = 0;
	std::vector<sf::Vector2i> foundPosNeg;
	std::vector<bool> exists = { false,false,false,false };
	for (size_t x = 0; x < subTiles.size(); x++)
	{
		for (size_t y = 0; y < posNeg.size(); y++)
		{
			if (subTiles.at(x).position == sf::Vector2i(pos.x + (size.x / 2 * posNeg.at(y).x), pos.y + (size.x / 2 * posNeg.at(y).y)))
			{
				pawnCount += subTiles.at(x).entities.size();
				foundPosNeg.push_back(subTiles.at(x).posNeg);
			}
		}
	}
	for (size_t i = 0; i < posNeg.size(); i++)
	{
		for (size_t j = 0; j < foundPosNeg.size(); j++)
		{
			if (posNeg.at(i) == foundPosNeg.at(j))
			{
				exists.at(i) == true;
			}
		}
	}
	/*	sf::Vector2i position = sf::Vector2i(subTiles.at(index).position.x + (subTiles.at(index).size.x / 2 * subTiles.at(index).posNeg.x), subTiles.at(index).position.x + (subTiles.at(index).size.x / 2 * subTiles.at(index).posNeg.x));
	sf::Vector2i tempSize = subTiles.at(index).size / 2;
	for (size_t i = 0; i < posNeg.size(); i++)
	{
	if (!exists.at(i))
	{
	pawnCount += countPawnsSubtiles(tempSize, sf::Vector2i(position.x + (tempSize.x * posNeg.at(i).x), position.y + (tempSize.y * posNeg.at(i).y)));
	}
	}*/
	return pawnCount;
}

int Map::countPawns(int index)
{
	int pawnCount = 0;
	std::vector<sf::Vector2i> foundPosNeg;
	std::vector<bool> exists = { false,false,false,false };

	for (size_t x = 0; x < subTiles.size(); x++)
	{
		if (sf::Vector2i(subTiles.at(index).position.x + (subTiles.at(index).size.x *		 subTiles.at(index).posNeg.x), subTiles.at(index).position.y + (subTiles.at(index).size.y * subTiles.at(index).posNeg.y)) == subTiles.at(x).position
			|| sf::Vector2i(subTiles.at(index).position.x, subTiles.at(index).position.y + (subTiles.at(index).size.y *	 subTiles.at(index).posNeg.y)) == subTiles.at(x).position
			|| sf::Vector2i(subTiles.at(index).position.x + (subTiles.at(index).size.x *		 subTiles.at(index).posNeg.x), subTiles.at(index).position.y) == subTiles.at(x).position)
		{
			pawnCount += subTiles.at(x).entities.size();
			foundPosNeg.push_back(subTiles.at(x).posNeg);
		}
	}
	for (size_t i = 0; i < posNeg.size(); i++)
	{
		for (size_t j = 0; j < foundPosNeg.size(); j++)
		{
			if (posNeg.at(i) == foundPosNeg.at(j))
			{
				exists.at(i) == true;
			}
		}
	}
	sf::Vector2i position = sf::Vector2i(subTiles.at(index).position.x + (subTiles.at(index).size.x / 2 * subTiles.at(index).posNeg.x), subTiles.at(index).position.x + (subTiles.at(index).size.x / 2 * subTiles.at(index).posNeg.x));
	sf::Vector2i tempSize = subTiles.at(index).size / 2;
	for (size_t i = 0; i < posNeg.size(); i++)
	{
		if (!exists.at(i))
		{
			pawnCount += countPawnsSubtiles(tempSize, sf::Vector2i(position.x + (tempSize.x * posNeg.at(i).x), position.y + (tempSize.y * posNeg.at(i).y)));
		}
	}
	return pawnCount;
}

std::vector<int> Map::findPawns(int index, int amount)
{
	int subTileIndex;
	int pawnCount = 0;
	std::vector<int> closestTiles;
	std::vector<int> foundPawns;
	for (size_t i = 0; i < subTiles.size(); i++)
	{
		if (buildings.buildings.at(index).position.x > subTiles.at(i).position.x && buildings.buildings.at(index).position.x <= subTiles.at(i).position.x + subTiles.at(i).size.x
			&&	buildings.buildings.at(index).position.y > subTiles.at(i).position.y && buildings.buildings.at(index).position.y <= subTiles.at(i).position.y + subTiles.at(i).size.y)
		{
			subTileIndex = i;
			i = subTiles.size();
		}
	}/*
	if (!subTiles.at(subTileIndex).entities.empty())
	{
		for (size_t i = 0; i < subTiles.at(subTileIndex).entities.size(); i++)
		{
			if (pawnCount < amount)
			{
				pawnCount++;
				foundPawns.push_back(subTiles.at(subTileIndex).entities.at(i));
			}
		}
	}
	int subSize = subTiles.at(subTileIndex).size.x;
	std::vector<int> checkedTiles;
	checkedTiles.push_back(subTileIndex);
	while (pawnCount < amount)
	{
		for (size_t i = 0; i < subTiles.size(); i++)
		{
			if (subTiles.at(i).position.x <= subTiles.at(subTileIndex).position.x + subSize
				&&	subTiles.at(i).position.x >= subTiles.at(subTileIndex).position.x - subSize
				&&	subTiles.at(i).position.y <= subTiles.at(subTileIndex).position.y + subSize
				&&	subTiles.at(i).position.y >= subTiles.at(subTileIndex).position.y - subSize)
			{
				bool exists = false;
				for (size_t j = 0; j < checkedTiles.size(); j++)
				{
					if (checkedTiles.at(j) == i)
					{
						exists = true;
					}
				}
				if (!exists)
				{
					checkedTiles.push_back(i);
					if (pawnCount < amount && !exists)
					{
						for (size_t i = 0; i < subTiles.at(i).entities.size(); i++)
						{
							pawnCount++;
							foundPawns.push_back(subTiles.at(i).entities.at(i));
						}
					}
					else
					{
						break;
					}
				}
			}
		}
		if (subSize < MAPSIZE / 2)
		{
			subSize *= 2;
		}
		else
		{
			break;
		}
	}
	;
	for (size_t i = 0; i < foundPawns.size(); i++)
	{
		InputComponent* temp = (InputComponent*)EntityComponentManager::getInstance().getComponent(INPUT_COMPONENT, foundPawns[i]);

		temp->moveHere((glm::vec2)buildings[index].position);
	}
	*/
	return foundPawns;
}

void Map::placePawn(std::size_t entity)
{
	glm::vec2 position = glm::vec2(EntityComponentManager::getInstance().getEntityPos(entity).x / pow(2, 4), EntityComponentManager::getInstance().getEntityPos(entity).x / pow(2, 4));
	for (size_t x = 0; x < subTiles.size(); x++)
	{
		for (size_t y = 0; y < subTiles[x].entities.size(); y++)
		{
			if (subTiles[x].entities[y] == entity)
			{
				if (position.x >= subTiles[x].position.x - (subTiles[x].size.x / 2)
					&& position.x < subTiles[x].position.x + (subTiles[x].size.x / 2)
					&& position.y >= subTiles[x].position.y - (subTiles[x].size.y / 2)
					&& position.y < subTiles[x].position.y + (subTiles[x].size.y / 2))
				{
					return;
				}
				subTiles[x].entities.erase(subTiles[x].entities.begin() + y);

				//Switched to this way of deleting because it is faster then .erase
				//subTiles[x].entities[y] = subTiles[x].entities.back();
				//subTiles[x].entities.pop_back();
			}
		}
		if (position.x >= subTiles[x].position.x - (subTiles[x].size.x / 2)
			&& position.x < subTiles[x].position.x + (subTiles[x].size.x / 2)
			&& position.y >= subTiles[x].position.y - (subTiles[x].size.y / 2)
			&& position.y < subTiles[x].position.y + (subTiles[x].size.y / 2))
		{
			subTiles[x].entities.push_back(entity);
			return;
		}
	}
}

void Map::makePawn()
{
	std::size_t id;
	id = EntityComponentManager::getInstance().createEntity();

	EntityComponentManager::getInstance().createComponent(GRAPHICS_COMPONENT, id);
	PositionComponent* posComponent = (PositionComponent*)EntityComponentManager::getInstance().createComponent(POSITION_COMPONENT, id);
	posComponent->setUp(heightMap, size);

	EntityComponentManager::getInstance().createComponent(PHYSICS_COMPONENT, id);
	EntityComponentManager::getInstance().createComponent(INPUT_COMPONENT, id);
	EntityComponentManager::getInstance().createComponent(PATHING_COMPONENT, id);

	EntityComponentManager::getInstance().setEntityPos(id, glm::vec2(rand() % tiles.size() * pow(2, 4), rand() % tiles[0].size() * pow(2, 4)));

	entities.push_back(id);
}

void Map::checkPawns()
{
}

void Map::save()
{
	std::ofstream file("./map.dnd", std::ofstream::binary);

	std::size_t size;
	std::size_t size2;

	size = tiles.size();
	file.write((char*)&size, sizeof(size));
	for (auto i = 0; i < size; ++i)
	{
		size2 = tiles[i].size();

		file.write((char*)&size2, sizeof(size2));
		file.write((char*)&tiles[i][0], sizeof(Tile) * size2);
	}

	size = subTiles.size();
	file.write((char*)&size, sizeof(size));
	for (auto i = 0; i < size; ++i)
	{
		file.write((char*)&subTiles[i], sizeof(subTile) - sizeof(std::vector<std::size_t>));
		size2 = subTiles[i].entities.size();
		file.write((char*)&size2, sizeof(size2));
		if (size2 != 0)
		{
			file.write((char*)&subTiles[i].entities[0], sizeof(std::size_t) * size2);
		}
	}

	size = entities.size();
	file.write((char*)&size, sizeof(size));
	if (size > 0)
	{
		file.write((char*)&entities[0], sizeof(std::size_t) * size);
	}

	file.close();
}

void Map::load()
{
	std::ifstream file("./map.dnd", std::ifstream::binary);

	std::size_t size;
	std::size_t size2;

	file.read((char*)&size, sizeof(size));
	tiles.resize(size);
	for (auto i = 0; i < size; ++i)
	{
		file.read((char*)&size2, sizeof(size2));
		tiles[i].resize(size2);
		file.read((char*)&tiles[i][0], sizeof(Tile) * size2);
	}

	file.read((char*)&size, sizeof(size));
	subTiles.resize(size);
	for (auto i = 0; i < size; ++i)
	{
		file.read((char*)&subTiles[i], sizeof(subTile) - sizeof(std::vector<std::size_t>));
		file.read((char*)&size2, sizeof(size2));
		subTiles[i].entities.resize(size2);
		if (size2 != 0)
		{
			file.read((char*)&subTiles[i].entities[0], sizeof(std::size_t) * size2);
		}
	}

	file.read((char*)&size, sizeof(size));
	entities.resize(size);
	if (size > 0)
	{
		file.read((char*)&entities[0], sizeof(std::size_t) * size);
	}

	file.close();
}

bool Map::placeBuilding(sf::Vector2i pos, int type, bool canAfford, ResearchHandler* research)
{
	if (canBuildCheck(pos, type, canAfford, research))
	{
		pos.x /= size;
		pos.y /= size;
		building tempB;

		tempB.type = buildings.buildingTypes[type];
		tempB.position = pos;
		for (size_t x = 0; x < tempB.type.sizeInTiles.x; x++)
		{
			for (size_t y = 0; y < tempB.type.sizeInTiles.y; y++)
			{
				tiles.at(pos.x + x).at(pos.y + y).buildable = false;
			}
		}
		buildings.buildings.push_back(tempB);
		//		findPawns(buildings.size() - 1, tempB.type.maxPawns);
		return true;
	}
	return false;
}

bool Map::canBuildCheck(sf::Vector2i pos, int type, bool canAfford, ResearchHandler* research)
{
	pos.x /= size;
	pos.y /= size;
	bool researched = true;
	for (int i = 0; i < buildings.buildingTypes[type].levels[0].required.size(); ++i)
	{
		if (!research->isResearched(buildings.buildingTypes[type].levels[0].required[i]->getName()))
		{
			researched = false;
			break;
		}
	}
	if (pos.x + buildings.buildingTypes[type].sizeInTiles.x <= tiles.size() && pos.x >= 0
		&& pos.y + buildings.buildingTypes[type].sizeInTiles.y <= tiles[0].size() && pos.y >= 0
		&& canAfford && researched)
	{
		bool closedOff = false;
		bool hasResource = false;

		for (size_t x = 0; x < buildings.buildingTypes[type].sizeInTiles.x; x++)
		{
			for (size_t y = 0; y < buildings.buildingTypes[type].sizeInTiles.y; y++)
			{
				if (!tiles.at(pos.x + x).at(pos.y + y).buildable)
				{
					closedOff = true;
				}
				if (buildings.buildingTypes[type].tileReq == tiles.at(pos.x + x).at(pos.y + y).res || buildings.buildingTypes[type].tileReq == Resources::NUM_OF_RESOURCES)
				{
					hasResource = true;
				}
			}
		}
		if (hasResource && !closedOff)
		{
			return true;
		}
	}

	return false;
}

ItemSet Map::getOutPut(int i, int level)
{
	ItemSet outPut = buildings.buildingTypes[i].levels[level].outPut;
	return outPut;
}

Building Map::getBuildingType(int index)
{
	return buildings.buildingTypes[index];
}

std::vector<Building>& Map::getBuildingTypes()
{
	return buildings.buildingTypes;
}

void Map::getBuildings(Resources* resources, ResearchHandler * research)
{
	Buildings temp(resources, research);
	buildings = temp;
}

sf::Vector2f Map::convertIdToPos(int id)
{
	int y = id % (tiles.size());
	int x = (id / tiles.size()) % tiles.size();

	return sf::Vector2f(x * size + size / 2, y * size + size / 2);
}

Buildings * Map::getBuildingHandler()
{
	return &buildings;
}