#pragma once

#include <GL\glew.h>
//#include <SFML\Window.hpp>
//#include <SFML\Graphics\RectangleShape.hpp>
//#include <SFML/Graphics/Vertex.hpp>
#include <glm\glm.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <cstdint>

#include <vector>
#include <iostream>
#include <random>
#include <fstream>
#include <memory>

#include "Camera.h"
#include "Resources.h"
#include "EntityComponentManager.h"
#include "AStar.h"
#include "diamondSquare.h"
#include "rainfall.h"
#include "terrainGen.h"
#include "Mesh.h"
#include "terrainGenerator.h"
#include "textureHandler.h"
#include "GrassBundle.h"
#include "Building.h"
#include "Buildings.h"

//static int MAPSIZE = 128;
const int MAXPAWNS = 3;

const 	std::vector<sf::Vector2i> posNeg = { { 1, 1 },{ -1, 1 },{ 1, -1 },{ -1, -1 } };

enum terrain
{
	//POLAR_DESERT,
	//TUNDRA,
	//ARCTIC_TUNDRA,
	//TEMPERATE_DESERT,
	GRASSLANDS,
	//WETLANDS,
	//DESERT,
	//SAVANNAH,
	//TROPICAL_WETLANDS,

	SEA_WATER,
	FRESH_WATER,

	NUM_OF_TERRAIN_TYPES
};

/**
 * WalkSpeed is stored independently of terrain (the determining factor behind walkSpeed)
 * in order to avoid using switches every time the speed needs to be extracted.
 */

struct Tile
{
	terrain ter;
	int walkSpeed;
	Resources::Resource res;
	int height;

	bool buildable;
};

/**
 * Subtiles differ from
 in that they are the tiles the world is sub-divided into
 * for the sake of searching for pawns, while normal tiles will be used mainly for
 * placing buildings, pathfinding and various local or specific tasks
 */

struct subTile
{
	/**
	* This might require some explaining. This is the way which corner of the higher
	* sub-tile this would be. 1, 1 refers to top left, -1, -1 refers to bottom right.
	* This is used to search for the sub-tiles related to the sub-tile in question for the
	* purposes of merging.
	*/
	sf::Vector2i posNeg;
	sf::Vector2i size;
	sf::Vector2i position;
	std::vector<std::size_t> entities;
};

class AStar;
class GrassBundle;

class Map
{
	friend class AStar;

public:
	/**
	*
	*
	*
	*/
	Map();
	~Map();

	/**
	 * All this does at the moment is to check whether any subtiles have more than the maximum
	 * allowed amount of pawns, and then subdividing them. Planned features for the moment is
	 * the ability to check whether a total of subdivided tiles reach below a certain treshold
	 * and then re-merging the tiles.
	 */
	void update(Resources& resources);
	/**
	*
	*
	*
	*/
	void updateWorkerCount(int building, unsigned int newWorkerCount, Resources& resources);
	/**
	*
	*
	*
	*/
	void drawTilesOnly(GLuint shaderProgram, int houseID = -1);
	/**
	*
	*
	*
	*/
	void drawBuildings(GLuint shaderProgram, int level);
	/**
	*
	*
	*
	*/
	void drawPlaceBuildingGhost(int selectedBuildingType, sf::Vector2i buildPos, bool canAfford, GLuint shaderProgram, int level, ResearchHandler* research);
	/**
	*
	*
	*
	*/
	void drawFlora(GLuint shaderProgram, Camera& camera, double elapsedTime);

	/**
	* Subdivides a subtile into four new subtiles of equal sizes, dividing the pawns and space
	* equally. Just send in the index of the tile you wish to subdivide, and it will work its magic.
	*/
	void subDivide(int index);

	/**
	* Merges four subdivided tiles into one bigger tiles, effectively a reverse of the previous function.
	* Used if the amount of pawns within a set of subtiles end up below the limit of pawns which should be there.
	* If not all tiles are found, it runs searchForSub() to find the subtiles for that subtile. This works recursively.
	* MIGHT cause some issues if suddenly it breaches into an endless loop of recursion.
	*/
	int merge(int index);

	/**
	* By taking in simply the size of the tile above and the position of it, this function searches through all
	* existing subtiles and finds the ones that are in that position, to then merge them into the bigger tile again.
	* Calls merge at one point, it is recursive, usually this won't cause a problem, but it MIGHT if it somehow escapes
	* the limits of it and begins an infinite recursion.
	*/
	int searchForSub(sf::Vector2i size, sf::Vector2i pos);
	/**
	*
	*
	*
	*/
	int countPawnsSubtiles(sf::Vector2i size, sf::Vector2i pos);
	/**
	*
	*
	*
	*/
	int countPawns(int index);
	/**
	*
	*
	*
	*/
	std::vector<int> findPawns(int index, int amount);
	/**
	* Effectively both a boundary check and a way to place or move pawns. Takes in the pawn in question, and checks whether
	* it's in a tile, if the tile it's stored in is the one it's actually in, if not it finds out where it should be and places
	* it there.
	*/
	void placePawn(std::size_t entity);
	/**
	*
	*
	*
	*/
	void makePawn();
	/**
	*
	*
	*
	*/
	void checkPawns();

	/**
	 * Saves.
	 */
	void save();
	/**
	 * Loads.
	 */
	void load();
	/**
	*
	*
	*
	*/
	bool placeBuilding(sf::Vector2i pos, int type, bool canAfford, ResearchHandler* research);
	/**
	*
	*
	*
	*/
	ItemSet getOutPut(int i, int level);
	/**
	*
	*
	*
	*/
	bool canBuildCheck(sf::Vector2i pos, int type, bool canAfford, ResearchHandler* research);
	/**
	*
	*
	*
	*/
	Building getBuildingType(int index);
	/**
	*
	*
	*
	*/
	std::vector<Building>& getBuildingTypes();
	/**
	*
	*
	*
	*/
	void getBuildings(Resources* resources, ResearchHandler* research);
	/**
	*
	*
	*
	*/
	sf::Vector2f convertIdToPos(int id);
	Buildings* Map::getBuildingHandler();
private:
	std::vector<std::vector<Tile>> tiles;
	std::vector<std::size_t> entities;
	std::vector<subTile> subTiles;
	std::shared_ptr<std::vector<std::vector<int>>> heightMap;
	Buildings buildings;

	std::mt19937_64 randomGenerator;

	const int size = 16;

	GrassBundle* grassBundle;

	unsigned int amountOfIndices;

	GLuint vertexArrayObject, vertexBufferObject, elementBufferObject, colorBufferObject, resourceBufferObject;
	GLuint groundTextureUnit;
	GLuint terrainIndexTexture;
};