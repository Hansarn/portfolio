#include "ResearchHandler.h"

ResearchHandler::ResearchHandler()
{
	activeID = -1;
	changed = true;
	std::ifstream file("./spillprogrammering-gruppe-4-rimworld-2.0/data/Research.data");
	std::vector<std::vector<std::string>> requirements;
	std::vector<std::string> names;
	if (!file.is_open())
	{
		printf("can't load file\n");
	}
	else
	{
		std::string name, text = "", temp;
		while (temp != "*/")
		{
			std::getline(file, temp);
		}
		while (!file.eof())
		{
			std::vector<std::string> required;

			int time;
			std::getline(file, temp);
			std::getline(file, name);
			names.push_back(name);
			std::getline(file, temp);
			text.clear();
			while (temp != "}")
			{
				text.append(temp);
				text.append("\n");
				std::getline(file, temp);
			}
			std::getline(file, temp);
			try
			{
				time = std::stoi(temp);
			}
			catch (const std::invalid_argument& ia)
			{
				std::cout << "Invalid argument: " << ia.what() << '\n';
			}
			std::getline(file, temp);
			while (temp != "}")
			{
				if (temp != "none")
				{
					required.push_back(temp);
				}
				std::getline(file, temp);
			}
			requirements.push_back(required);
			Research temp2(research.size(), name, text, time);
			research.push_back(temp2);
		}
	}

	int i = 0;
	for (auto req : requirements)
	{
		for (auto s : req)
		{
			auto f = std::find(names.begin(), names.end(), s);
			if (f != names.end())
			{
				research[i].addreqid(std::distance(names.begin(), f));
			}
		}
		if (req.size() == 0)
		{
			research[i].setLevel(0);
		}
		i++;
	}
	bool numberd = false;
	std::vector<Research> tempRes;

	while (!numberd)
	{
		tempRes.clear();
		for (auto r : research)
		{
			if (r.getLevel() == -1)
			{
				tempRes.push_back(r);
			}
		}
		if (tempRes.size() == 0)
		{
			numberd = true;
		}
		else
		{
			inheritance(tempRes);
		}
	}

	file.close();
}

std::vector<Research>* ResearchHandler::getResearch()
{
	return &research;
}

void ResearchHandler::update()
{
	//add time
	if (activeID != -1)
	{
		if (research[activeID].updateTime(-1))
		{
			activeID = -1;
			changed = true;
		}
	}
}

bool ResearchHandler::setActiveResearch(int id)
{
	if (!isResearched(id) && canBeResearched(id))
	{
		activeID = id;
		return true;
	}
	return false;
}

bool ResearchHandler::isResearched(int id)
{
	return research.at(id).isFinished();
}

bool ResearchHandler::isResearched(std::string name)
{
	for (auto r : research)
	{
		if (r.getName() == name)
		{
			return r.isFinished();
		}
	}
	return false;
}

bool ResearchHandler::getChanged()
{
	return changed;
}

void ResearchHandler::setChanged(bool changed)
{
	this->changed = changed;
}

Research* ResearchHandler::getResearch(std::string name)
{
	for (size_t i = 0; i < research.size(); i++)
	{
		if (research[i].getName() == name)
		{
			return &research[i];
		}
	}
}

void ResearchHandler::inheritance(std::vector<Research> tempRes)
{
	for (int i = 0; i < tempRes.size(); i++)
	{
		int highestLevel = -1;
		bool all = true;
		std::vector<int> tempReq = tempRes[i].getReq();
		for (size_t f = 0; f < tempReq.size(); f++)
		{
			int level = research[tempReq[f]].getLevel();
			if (level == -1)
			{
				all = false;
			}
			else if (level > highestLevel)
			{
				highestLevel = level;
			}
		}
		if (all)
		{
			research[tempRes[i].getId()].setLevel(highestLevel + 1);
		}
	}
}

bool ResearchHandler::canBeResearched(int id)
{
	bool can = true;
	if (id < research.size())
	{
		std::vector<int> temp = research[id].getReq();
		for (size_t i = 0; i < temp.size(); i++)
		{
			int f = temp[i];
			if (!research[f].isFinished())
			{
				can = false;
			}
		}
	}
	else
	{
		can = false;
	}
	return can;
}