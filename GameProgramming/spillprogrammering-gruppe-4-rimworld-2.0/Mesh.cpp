#include "Mesh.h"

//Can't include tiny obj loader in header for some reason
#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

Mesh::Mesh(std::string filePath, double scale)
{
	tinyobj::attrib_t attribs;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string errorMessage;

	GLuint vertexBuffer, diffuseBuffer, specularBuffer, elementBuffer;

	std::replace(filePath.begin(), filePath.end(), '\\', '/');
	std::string baseDirectory = filePath.substr(0, filePath.rfind('/') + 1);

	if (tinyobj::LoadObj(&attribs, &shapes, &materials, &errorMessage, filePath.c_str(), baseDirectory.c_str()))
	{
		glGenVertexArrays(1, &vertexArrayObject);
		glGenBuffers(1, &vertexBuffer);
		glGenBuffers(1, &diffuseBuffer);
		glGenBuffers(1, &specularBuffer);
		glGenBuffers(1, &elementBuffer);
		glBindVertexArray(vertexArrayObject);

		for (std::size_t i = 0; i < attribs.vertices.size(); ++i)
		{
			attribs.vertices[i] *= scale;
		}

		std::vector<tinyobj::real_t> diffuseColors;
		std::vector<tinyobj::real_t> specularColors;
		diffuseColors.resize(attribs.vertices.size());
		specularColors.resize(attribs.vertices.size());
		std::vector<int> indices;
		unsigned int totalIndices = 0;
		for (int i = 0; i < shapes.size(); ++i)
		{
			subShapes shape;
			shape.startingIndex = totalIndices;
			totalIndices += shapes[i].mesh.indices.size();
			shape.numOfIndices = totalIndices - shape.startingIndex;
			shape.textureId = textureHandler::loadFromFile(baseDirectory + materials[shapes[i].mesh.material_ids[0]].diffuse_texname);
			this->shapes.push_back(shape);

			for (int j = 0; j < shapes[i].mesh.indices.size(); ++j)
			{
				indices.push_back(shapes[i].mesh.indices[j].vertex_index);

				for (int k = 0; k < 2; ++k)
				{
					diffuseColors[(shapes[i].mesh.indices[j].vertex_index * 3) + k] = materials[shapes[i].mesh.material_ids[j / 3]].diffuse[k];
					specularColors[(shapes[i].mesh.indices[j].vertex_index * 3) + k] = materials[shapes[i].mesh.material_ids[j / 3]].specular[k];
				}
			}
		}

		amountOfIndices = indices.size();

		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, attribs.vertices.size() * sizeof(attribs.vertices[0]) + attribs.texcoords.size() * sizeof(tinyobj::real_t), NULL, GL_STATIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, attribs.vertices.size() * sizeof(tinyobj::real_t), &attribs.vertices[0]);
		if (attribs.texcoords.size() != 0)
		{
			glBufferSubData(GL_ARRAY_BUFFER, attribs.vertices.size() * sizeof(tinyobj::real_t), attribs.texcoords.size() * sizeof(tinyobj::real_t), &attribs.texcoords[0]);

			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void*)(attribs.vertices.size() * sizeof(tinyobj::real_t)));
		}

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);

		glBindBuffer(GL_ARRAY_BUFFER, diffuseBuffer);
		glBufferData(GL_ARRAY_BUFFER, diffuseColors.size() * sizeof(diffuseColors[0]), &diffuseColors[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GL_FLOAT), NULL);

		glBindBuffer(GL_ARRAY_BUFFER, specularBuffer);
		glBufferData(GL_ARRAY_BUFFER, specularColors.size() * sizeof(specularColors[0]), &specularColors[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GL_FLOAT), NULL);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(indices[0]), &indices[0], GL_STATIC_DRAW);

		glBindVertexArray(0);

		path = filePath;
	}
	else
	{
		std::cout << errorMessage << std::endl;
	}
}

Mesh::~Mesh()
{}

void Mesh::draw()
{
	glBindVertexArray(vertexArrayObject);

	for (std::size_t i = 0; i < shapes.size(); ++i)
	{
		textureHandler::bindTexture(shapes[i].textureId, GL_TEXTURE_2D, 0);

		glDrawElements(GL_TRIANGLES, shapes[i].numOfIndices, GL_UNSIGNED_INT, (void*)(shapes[i].startingIndex * sizeof(GLuint)));
	}

	glDrawElements(GL_TRIANGLES, amountOfIndices, GL_UNSIGNED_INT, NULL);

	glBindVertexArray(0);
}