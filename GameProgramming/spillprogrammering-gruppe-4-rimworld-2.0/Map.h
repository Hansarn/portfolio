#pragma once

#include <GL\glew.h>
//#include <SFML\Window.hpp>
//#include <SFML\Graphics\RectangleShape.hpp>
//#include <SFML/Graphics/Vertex.hpp>
#include <glm\glm.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <cstdint>

#include <vector>
#include <iostream>
#include <random>
#include <fstream>
#include <memory>

#include "Camera.h"
#include "Resources.h"
#include "EntityComponentManager.h"
#include "AStar.h"
#include "diamondSquare.h"
#include "rainfall.h"
#include "Mesh.h"
#include "terrainGenerator.h"
#include "textureHandler.h"
#include "GrassBundle.h"
#include "Building.h"
#include "Buildings.h"

//static int MAPSIZE = 128;
const int MAXPAWNS = 3;

const 	std::vector<sf::Vector2i> posNeg = { { 1, 1 },{ -1, 1 },{ 1, -1 },{ -1, -1 } };

enum terrain
{
	//POLAR_DESERT,
	//TUNDRA,
	//ARCTIC_TUNDRA,
	//TEMPERATE_DESERT,
	GRASSLANDS,
	//WETLANDS,
	//DESERT,
	//SAVANNAH,
	//TROPICAL_WETLANDS,

	SEA_WATER,
	FRESH_WATER,

	NUM_OF_TERRAIN_TYPES
};

/**
 * WalkSpeed is stored independently of terrain (the determining factor behind walkSpeed)
 * in order to avoid using switches every time the speed needs to be extracted.
 */

struct Tile
{
	terrain ter;
	int walkSpeed;
	Resources::Resource res;
	int height;

	bool buildable;
};

/**
 * Subtiles differ from normal tiles
 * in that they are the tiles the world is sub-divided into
 * for the sake of searching for pawns, while normal tiles will be used mainly for
 * placing buildings, pathfinding and various local or specific tasks
 */

struct subTile
{
	/**
	* This might require some explaining. This is the way which corner of the higher
	* sub-tile this would be. 1, 1 refers to top left, -1, -1 refers to bottom right.
	* This is used to search for the sub-tiles related to the sub-tile in question for the
	* purposes of merging.
	*/
	sf::Vector2i posNeg;
	sf::Vector2i size;
	sf::Vector2i position;
	std::vector<std::size_t> entities;
};

class AStar;
class GrassBundle;

class Map
{
	friend class AStar;

public:
	/**
	*	Initiates map and everything related to it, sets the resources for the tiles,
	*	sets the properties for how tiles are drawn
	*/
	Map();
	~Map();

	/**
	 *	Updates food and kills entities
	 *	@param the resources (where food is found)
	 */
	void update(Resources& resources);
	/**
	*	Updates the amount of workers
	*	@param the building to update worker count on
	*	@param new amount of workers
	*	@param the resources
	*/
	void updateWorkerCount(int building, unsigned int newWorkerCount, Resources& resources);
	/**
	*	Draws the tiles
	*	@param the shaderprogram in use
	*	@param the ID for houses
	*/
	void drawTilesOnly(GLuint shaderProgram, int houseID = -1);
	/**
	*	Draws the buildings
	*	@param the shaderprogram
	*	@param the level of the building
	*/
	void drawBuildings(GLuint shaderProgram, int level);
	/**
	*	Draws a ghost for the building you're trying to place, red if you can't build and blue if you can
	*	@param the type you're trying to place
	*	@param the position you're trying to build at
	*	@param prechecked bool for whether you can afford building here
	*	@param the shaderprogram in use
	*	@param the level of the building you're trying to place (always the first here, unnecessary?)
	*	@param the research handler to find which researches have been completed
	*/
	void drawPlaceBuildingGhost(int selectedBuildingType, sf::Vector2i buildPos, bool canAfford, GLuint shaderProgram, int level, ResearchHandler* research);
	/**
	*	Draws the grass
	*	@param the shaderprogram being used
	*	@param the camera used in the game
	*	@param time elapsed
	*/
	void drawFlora(GLuint shaderProgram, Camera& camera, double elapsedTime);

	/**
	*	Initiates a new peasant
	*/
	void makePawn();
	/**
	 * Saves.
	 */
	void save();
	/**
	 * Loads.
	 */
	void load();
	/**
	*	Function for placing a building. Runs 'canBuildCheck'.
	*	@param the position you're trying to place the building at
	*	@param the index of the building type you're trying to place
	*	@param prechecked bool for whether you can afford building here
	*	@param list of researches to check if you've researched what you need to
	*	@return bool saying whether you can build or not
	*/
	bool placeBuilding(sf::Vector2i pos, int type, bool canAfford, ResearchHandler* research);
	/**
	*	Getter for the output of a certain building
	*	@param index of the building type in question
	*	@param index for which level the building is
	*	@return an itemset (vectors of tradegoods, resources, and amounts of both)
	*/
	ItemSet getOutPut(int i, int level);
	/**
	*	A check for whether you can build a certain building at the given location
	*	@param the position you're trying to place the building at
	*	@param the index of the building type you're trying to place
	*	@param prechecked bool for whether you can afford building here
	*	@param list of researches to check if you've researched what you need to
	*	@return bool saying whether you can build or not
	*/
	bool canBuildCheck(sf::Vector2i pos, int type, bool canAfford, ResearchHandler* research);
	/**
	*	Getter for a specified building based on the index of it
	*	@param the index of the building type in the buildings vector
	*	@return the building type in question
	*/
	Building getBuildingType(int index);
	/**
	*	Getter for the types of buildings (buildings in Buildings.h)
	*	@return the building types
	*/
	std::vector<Building>& getBuildingTypes();
	/**
	*	Initializes the building handler (buildings)
	*	@param the resources, tradegoods and relevant functions
	*	@param the researches and relevant functions
	*/
	void getBuildings(Resources* resources, ResearchHandler* research);
	/**
	*	Function to get the actual position based on an id
	*	@param the id of what you want the position for
	*	@return the position
	*/
	sf::Vector2f convertIdToPos(int id);

	/**
	*	Simple getter for the buildingHandler (buildings)
	*	@return the building handler
	*/
	Buildings* Map::getBuildingHandler();
private:
	std::vector<std::vector<Tile>> tiles;
	std::vector<std::size_t> entities;
	std::vector<subTile> subTiles;
	std::shared_ptr<std::vector<std::vector<int>>> heightMap;
	Buildings buildings;

	std::mt19937_64 randomGenerator;

	const int size = 16;

	GrassBundle* grassBundle;

	unsigned int amountOfIndices;

	GLuint vertexArrayObject, vertexBufferObject, elementBufferObject, colorBufferObject, resourceBufferObject;
	GLuint groundTextureUnit;
	GLuint terrainIndexTexture;
};
