#include "InputComponent.h"

InputComponent::InputComponent(std::size_t& entityId) : ComponentBase(entityId)
{
}

InputComponent::InputComponent()
{}

InputComponent::~InputComponent()
{
}

void InputComponent::recieveMessage(char message[], void * data)
{
	if (strcmp(message, "InputCompleted") == 0)
	{
		if (despawnFlag)
		{
			EntityComponentManager::getInstance().sendMessageToComponent(GRAPHICS_COMPONENT, parentId, "Despawn");
		}
	}
}

void InputComponent::moveHere(glm::vec2& pos, bool despawn)
{
	despawnFlag = despawn;
	EntityComponentManager::getInstance().sendMessageToComponent(PATHING_COMPONENT, parentId, "MoveHere", &pos);
	EntityComponentManager::getInstance().sendMessageToComponent(GRAPHICS_COMPONENT, parentId, "Spawn");
}