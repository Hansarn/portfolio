#pragma once
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>

#include "EventQueue.h"
#include "GUIEvent.h"
#include "Map.h"

enum types;

#define BuildingHandlerPerShow 6

class CityBuildingGui
{
public:

	/*
	* Sets up the cityBuildingGUI
	* @param EventQueue og type GUIEvent
	* @param Vector of BuildingType
	*/
	void citySetUp(EventQueue<GUIEvent>* eventQueue, std::vector<Building>* buildings, Resources* resources);
	/*
	* Sets the visability of the cityBuildingGUI based on param
	* @param true if the building should be seen false if not
	*/
	void activateCityBuildingGui(bool active);
	/*
	* Adds the cityBuildingGUI to a sfg::Box and infoWindow to desktop
	* @param the Box you want to attach it too
	* @param the dekstop you want the info window to be added too
	*/
	void AddToBox(sfg::Box::Ptr box, sfg::Desktop* desktop);
	/*
	* Activates the list of all buildings or removes the list and the information of one building
	* @param true if you want it to show false if you want to close it
	*/
	void activateBuildMenu(bool active);
	/**
	*  removes information of the building type
	*/
	void removeBuildingInfo();
	/**
	* displays infomation about a built building.
	* @param the building that you want to display information about.
	*/
	void displayBuilding(building* b);
private:
	enum Direction
	{
		UP,
		DOWN
	};
	void addBuildingButton(EventQueue<GUIEvent>* eventQueue, const sf::String &label = L"");
	void addBuildingButtonWithImage(const sf::String &fileName, EventQueue<GUIEvent>* eventQueue, const sf::String &label = L"");

	void DisplayBuildTable(bool show);
	void moveBuildList(Direction direction);
	void setUpClickBuilding(sfg::Box::Ptr topBox, EventQueue<GUIEvent>* eventQueue);
	void infoMenu(bool show, int buildingNr, int level);
	void show(int type);
	std::vector<sfg::Button::Ptr> buttons;

	int counter, page;
	bool activated;
	sfg::Table::Ptr buildTable;
	sfg::Box::Ptr mainBox;
	sfg::Box::Ptr buildTableBox;
	sfg::Box::Ptr menu;
	sfg::Button::Ptr buildingButton;

	sfg::Window::Ptr infoWindow;
	sfg::Box::Ptr buildingBox;
	sfg::Box::Ptr underBox;
	sfg::Box::Ptr buildingPointBox;

	sfg::SpinButton::Ptr spin;
	sfg::Label::Ptr maxVillager;

	sfg::Frame::Ptr buildingFrame;
	sfg::Label::Ptr BuildingInfoLabel;
	std::vector<Building>* buildings;

	std::vector<sfg::Label::Ptr> output;
	std::vector<sfg::Label::Ptr> outputForInfo;
	std::vector<sfg::Label::Ptr> cost;

	Resources* resources;

	sfg::Label::Ptr pawn;

	bool toggleMenu;
};
