#include "PhysicsComponent.h"

#include "EntityComponentManager.h"

PhysicsComponent::PhysicsComponent(std::size_t& entityId) : ComponentBase(entityId)
{
	speed = 100.0f;
}

PhysicsComponent::PhysicsComponent()
{}

PhysicsComponent::~PhysicsComponent()
{}

void PhysicsComponent::recieveMessage(char message[], void* data)
{
	if (strcmp(message, "MoveHere") == 0)
	{
		//glm::vec3 pos = EntityComponentManager::getInstance().getEntityPos(parentId)
		glm::vec2 delta = *(glm::vec2*)data - glm::vec2(EntityComponentManager::getInstance().getEntityPos(parentId));

		dist = sqrtf(delta.x * delta.x + delta.y * delta.y);

		velocity = delta / dist * speed;
	}
	else if (strcmp(message, "Stop") == 0)
	{
		velocity = { 0.0f, 0.0f };
	}
}

void PhysicsComponent::update(float deltaTime)
{
	glm::vec2 offset = velocity * deltaTime;

	if (dist > 0.0f)
	{
		dist -= sqrtf(offset.x * offset.x + offset.y * offset.y);

		if (dist < 0.1f)
		{
			EntityComponentManager::getInstance().sendMessageToComponent(PATHING_COMPONENT, parentId, "ReachedDestination");
		}
	}
	EntityComponentManager::getInstance().sendMessageToComponent(POSITION_COMPONENT, parentId, "PositionOffset", &offset);
}