#include "PopUpGui.h"

void PopUpGui::SetUp()
{
	window = sfg::Window::Create(sfg::Window::BACKGROUND);

	sf::Vector2f pos = sf::Vector2f(400, 300);
	text = sfg::Label::Create("");
	auto textBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);

	auto close = sfg::Button::Create("Close");
	close->GetSignal(sfg::Widget::OnLeftClick).Connect([this] {
		window->Show(false);
	});

	textBox->Pack(text);
	textBox->Pack(close, false, false);
	window->Add(textBox);
	window->SetAllocation(sf::FloatRect(1600 / 2 - pos.x / 2, 900 / 2 - pos.y / 2, pos.x, pos.y));
	window->Show(false);
}

void PopUpGui::addToDesktop(sfg::Desktop * desktop)
{
	desktop->Add(window);
	this->desktop = desktop;
}

void PopUpGui::activatePopUp(bool active, std::string msg)
{
	window->Show();
	desktop->BringToFront(window);
	text->SetText(msg);
}

void PopUpGui::update()
{
}