#include "WorldMapGui.h"

void WorldMapGui::worldMapSetUp(EventQueue<GUIEvent>* eventQueue)
{
	auto button1 = sfg::Button::Create("Explore");
	auto button2 = sfg::Button::Create("Building");
	auto button3 = sfg::Button::Create("Diplomacy");
	auto button4 = sfg::Button::Create("Empire");
	table = sfg::Table::Create();

	button1->GetSignal(sfg::Widget::OnLeftClick).Connect([eventQueue] {
		GUIEvent event;
		event.type = GUIEvent::EXPLORE;
		eventQueue->pushEvent(event); });

	table->Attach(button1, sf::Rect<sf::Uint32>(0, 0, 1, 1));
	table->Attach(button2, sf::Rect<sf::Uint32>(0, 1, 1, 1));
	table->Attach(button3, sf::Rect<sf::Uint32>(0, 2, 1, 1));
	table->Attach(button4, sf::Rect<sf::Uint32>(0, 3, 1, 1));
	table->SetAllocation(sf::FloatRect(0, 300, 300, 300));
}

void WorldMapGui::activateWorldMapGui(bool active)
{
	table->Show(active);
}

void WorldMapGui::AddToBox(sfg::Box::Ptr box)
{
	box->Pack(table);
}