#include "GrassBundle.h"

GrassBundle::GrassBundle()
{}

GrassBundle::GrassBundle(std::vector<std::vector<terrain>>& map, std::vector<std::vector<int>>& height, int tileSize)
{
	const float density = 4.0f;

	randomGen::PerlinNoise perlin;

	for (std::size_t i = 0; i < map.size(); ++i)
	{
		for (std::size_t j = 0; j < map[0].size(); ++j)
		{
			if (map[j][i] == terrain::GRASSLANDS)
			{
				for (float x = perlin.noise0_1(i) / 2.0f; x < tileSize - 0.5; x += density + perlin.noise0_1(i + x) - 0.5f)
				{
					for (float y = perlin.noise0_1(j) / 2.0f; y < tileSize - 0.5; y += density + perlin.noise0_1(j + y) - 0.5f)
					{
						glm::vec3 position;
						position.x = i * tileSize + x + (perlin.noise(i + x + j + y) / 10.0f);
						position.z = j * tileSize + y;

						int xx = position.x / tileSize;
						int yy = position.z / tileSize;
						float deltaX = (position.x / (float)tileSize - xx);
						float deltaY = (position.z / (float)tileSize - yy);

						float lerp1 = height[xx][yy] + deltaX * (height[xx + 1][yy] - height[xx][yy]);
						float lerp2 = height[xx][yy + 1] + deltaX * (height[xx + 1][yy + 1] - height[xx][yy + 1]);

						position.y = lerp1 + deltaY * (lerp2 - lerp1);

						positions.push_back(position);
					}
				}
			}
		}
	}

	GLuint vertexBuffer;
	glGenVertexArrays(1, &vertexArrayObject);
	glGenBuffers(1, &vertexBuffer);

	glBindVertexArray(vertexArrayObject);

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * positions.size() * 3, &positions[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);

	//for (int i = 0; i < 4; ++i)
	//{
	//	GLuint matrixPos = 1 + i;
	//
	//	glEnableVertexAttribArray(matrixPos);
	//	glVertexAttribPointer(matrixPos, 4, GL_FLOAT, GL_FALSE, 16 * sizeof(GLfloat), (GLvoid*)(sizeof(GLfloat) * i * 4));
	//	glVertexAttribDivisor(matrixPos, 1);
	//}

	glBindVertexArray(0);

	grassTexture = textureHandler::loadFromFile("./spillprogrammering-gruppe-4-rimworld-2.0/rimworld 2 sprites/temporaryGrass.png");
}

GrassBundle::~GrassBundle()
{
}

void GrassBundle::draw(GLuint shaderProgram, Camera& camera, double timeElapsed)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	textureHandler::bindTexture(grassTexture, GL_TEXTURE_2D, 0);
	glUniform1i(glGetUniformLocation(shaderProgram, "texture0"), 0);

	glUniform3fv(glGetUniformLocation(shaderProgram, "cameraPos"), 1, glm::value_ptr(camera.getPos()));

	glUniform1f(glGetUniformLocation(shaderProgram, "fTimePassed"), float(timeElapsed));

	glBindVertexArray(vertexArrayObject);

	glDrawArrays(GL_POINTS, 0, positions.size());

	glBindVertexArray(0);
	glDisable(GL_BLEND);
}