#include <GL\glew.h>
//#include <SFML\Graphics.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include <deque>
#include <random>
#include <time.h>
#include <thread>

#include "EntityComponentManager.h"
#include "Gui.h"
#include "Map.h"
#include "AStar.h"
#include "EventQueue.h"
#include "WorldMap.h"
#include "diamondSquare.h"
#include "CivHandler.h"
#include "Camera.h"
#include "Shader.h"
#include "Mesh.h"
#include "Trade.h"
#include "ResearchHandler.h"

//template<size_t N>
//struct Factorial
//{
//	enum {Value = N * Factorial<N - 1>::Value };
//};
//
//template<>
//struct Factorial<0>
//{
//	enum {Value = 1};
//};

struct buildingTypePrices
{
	std::vector<Resources::Resource> resWares;
	std::vector<Resources::TradeGoods> tradeWares;
	std::vector<int> resPrice, tradePrice;
};

void main()
{
	try
	{
		bool running = true;
		sf::ContextSettings settings(24, 8, 0, 0, 0, sf::ContextSettings::Core);

		sf::Window window(sf::VideoMode(1600, 900), "temp rimworld 2", 7U, settings);
		window.setActive(true);

		GLenum glewError = glewInit();
		if (glewError != GLEW_OK)
		{
			printf("Glew didn't initialize\n");
			running = false;
			system("pause");
		}

		meshManager::loadMesh("./spillprogrammering-gruppe-4-rimworld-2.0/models/mine/mine.obj");

		Camera camera(window.getSize().x / window.getSize().y, glm::vec3(50.0f, 400.0f, 50.0f), glm::vec3(0.0f, -1.0f, 0.5f));
		Shader mapShader("./spillprogrammering-gruppe-4-rimworld-2.0/shaders/mapShader");
		Shader basicShader("./spillprogrammering-gruppe-4-rimworld-2.0/shaders/pawnShader");
		Shader pickerShader("./spillprogrammering-gruppe-4-rimworld-2.0/shaders/mapPickerShader");
		Shader buildmodeShader("./spillprogrammering-gruppe-4-rimworld-2.0/shaders/buildmodeBuildingShader");
		Shader grassShader("./spillprogrammering-gruppe-4-rimworld-2.0/shaders/grassShader", true);
		Shader buildingPickerShader("./spillprogrammering-gruppe-4-rimworld-2.0/shaders/buildingPickerShader");

		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		float scrollSpeed = 1000.0f;
		float minCameraHeight = 200.0f;
		float maxCameraHeight = 1000.0f;

		int ticks = 0;
		bool tickFlag = false;
		int tickTime = 2; //time needed for tick to go true
		sf::Clock clock;

		sf::Event e;
		GUIEvent guiEvent;

		Map map;

		bool buildMode = false;
		int buildingToPlace = 0;

		CivHandler civHandler(15);
		Resources resources;
		ResearchHandler research;
		Trade trade(&resources, &civHandler);

		map.getBuildings(&resources, &research);

		std::vector<buildingTypePrices> prices;

		for (size_t i = 0; i < map.getBuildingTypes().size(); i++)
		{
			buildingTypePrices tempPrices;
			ItemSet price = map.getBuildingType(i).levels[0].price;
			for (size_t j = 0; j < map.getBuildingType(i).levels[0].price.resWares.size(); j++)
			{
				tempPrices.resWares.push_back(map.getBuildingType(i).levels[0].price.resWares[j].resItem);
				tempPrices.resPrice.push_back(map.getBuildingType(i).levels[0].price.resWares[j].amount);
			}
			for (size_t j = 0; j < map.getBuildingType(i).levels[0].price.tradeWares.size(); j++)
			{
				tempPrices.tradeWares.push_back(map.getBuildingType(i).levels[0].price.tradeWares[j].tradeItem);
				tempPrices.tradePrice.push_back(map.getBuildingType(i).levels[0].price.tradeWares[j].amount);
			}
			prices.push_back(tempPrices);
		}

		Gui gui(&civHandler, &map.getBuildingTypes(), &resources, &trade, &research);
		sfg::SFGUI* sfgui = gui.getSFGUI();
		sfg::Desktop* desktop = gui.getDesktop();
		bool guiFocus = false;

		double time = 0.0;
		double totalTimeElapsed = 0.0;
		sf::Clock testClock;
		WorldMap worldMap;

		AStar::setMap(&map);
		Toggle state = gui.getToggle();

		sf::Vector2i mousePos = { 0,0 };
		int currentPickedId = 0;
		int currentPickedBuilding;

		while (running)
		{
			time = testClock.restart().asSeconds();
			totalTimeElapsed += time;
			while (gui.events.pullEvent(guiEvent))
			{
				if (guiEvent.type == GUIEvent::BUILDING)
				{
					//		printf("Building building %s\n", map.getBuildingType(guiEvent.building.id).name);
					buildMode = true;

					buildingToPlace = guiEvent.building.id;
				}
				else if (guiEvent.type == GUIEvent::SAVE)
				{
					EntityComponentManager::getInstance().writeToFile();
					map.save();
				}
				else if (guiEvent.type == GUIEvent::LOAD)
				{
					EntityComponentManager::getInstance().loadFromFile();
					map.load();
				}
				else if (guiEvent.type == GUIEvent::QUIT)
				{
					running = false;
				}
				else if (guiEvent.type == GUIEvent::FOCUS)
				{
					guiFocus = guiEvent.focus.focus;
				}
				else if (guiEvent.type == GUIEvent::BACK)
				{
					gui.escManu();
				}
				else if (guiEvent.type == GUIEvent::EXPLORE)
				{
					worldMap.update(guiEvent);
				}
				else if (guiEvent.type == GUIEvent::ACTIVATEBUILDINGMENU)
				{
					gui.displayBuildingMenu(guiEvent.active.active);
				}
				else if (guiEvent.type == GUIEvent::ASSIGNED_WORKER_NUMBER_CHANGED)
				{
					map.updateWorkerCount(currentPickedBuilding, guiEvent.worker.newAmount, resources);
				}
				else if (guiEvent.type == GUIEvent::NEW_PAWN)
				{
					map.makePawn();
				}
			}
			while (window.pollEvent(e))
			{
				desktop->HandleEvent(e);
				if (e.type == sf::Event::Closed)
				{
					running = false;
				}
				if (!guiFocus)
				{
					if (e.type == sf::Event::MouseMoved && buildMode)
					{
						glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
						glClear(GL_COLOR_BUFFER_BIT);

						sf::Vector2i pos = sf::Mouse::getPosition(window);

						pickerShader.bind();
						camera.bindUniforms(pickerShader.getProgram());

						map.drawTilesOnly(pickerShader.getProgram(), -1);

						glFlush();
						glFinish();

						unsigned char data[4];
						glReadPixels(pos.x, window.getSize().y - pos.y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, data);

						if (data[0] != 255 && data[1] != 255 && data[2] != 255)
						{
							currentPickedId = data[0] * 256 * 256 +
								data[1] * 256 +
								data[2];
						}
					}

					if (e.type == sf::Event::MouseButtonPressed)
					{
						if (e.mouseButton.button == sf::Mouse::Left)
						{
							sf::Vector2f actuallPos = map.convertIdToPos(currentPickedId);

							glm::vec2 temp;
							temp.x = actuallPos.x;
							temp.y = actuallPos.y;

							if (buildMode)
							{
								if (map.placeBuilding((sf::Vector2i)actuallPos,
									buildingToPlace,
									resources.canAfford(prices.at(buildingToPlace).resWares,
										prices.at(buildingToPlace).resPrice,
										prices.at(buildingToPlace).tradeWares,
										prices.at(buildingToPlace).tradePrice),
									&research))
								{
									buildMode = false;

									ItemSet temp = map.getBuildingType(buildingToPlace).levels[0].outPut;

									resources.buyBuilding(prices.at(buildingToPlace).resWares, prices.at(buildingToPlace).resPrice, prices.at(buildingToPlace).tradeWares, prices.at(buildingToPlace).tradePrice);
								}
							}
							else
							{
								glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
								glClear(GL_COLOR_BUFFER_BIT);

								sf::Vector2i pos = sf::Mouse::getPosition(window);

								buildingPickerShader.bind();
								camera.bindUniforms(buildingPickerShader.getProgram());

								map.drawBuildings(buildingPickerShader.getProgram(), 0);

								glFlush();
								glFinish();

								unsigned char data[4];
								glReadPixels(pos.x, window.getSize().y - pos.y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, data);

								if (data[0] != 255 && data[1] != 255 && data[2] != 255)
								{
									currentPickedBuilding = data[0] * 256 * 256 +
										data[1] * 256 +
										data[2];
									gui.displayBuilding(&map.getBuildingHandler()->buildings[currentPickedBuilding]);
								}
							}
						}
						if (e.mouseButton.button == sf::Mouse::Right)
						{
							buildMode = false;
							gui.removeBuildingInfo();
						}
					}
				}
				if (e.type == sf::Event::MouseWheelMoved)
				{
					camera.move(glm::vec3(0.0, -e.mouseWheel.delta * 10.0f, 0.0));
					if (camera.getPos().y < minCameraHeight)
					{
						camera.move(glm::vec3(0.0, -camera.getPos().y + minCameraHeight, 0.0));
					}
					else if (camera.getPos().y > maxCameraHeight)
					{
						camera.move(glm::vec3(0.0, -camera.getPos().y + maxCameraHeight, 0.0));
					}
				}
				if (e.type == sf::Event::KeyPressed)
				{
					if (e.key.code == sf::Keyboard::Escape)
					{
						gui.escManu();
					}
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				float modifier = 1.0f;
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
				{
					modifier = 10.0f;
				}

				camera.move(glm::vec3(-scrollSpeed * ((camera.getPos().y - minCameraHeight + 10) / maxCameraHeight), 0.0f, 0.0f) * (float)time * modifier);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				float modifier = 1.0f;
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
				{
					modifier = 10.0f;
				}

				camera.move(glm::vec3(scrollSpeed * ((camera.getPos().y - minCameraHeight + 10) / maxCameraHeight), 0.0f, 0.0f) * (float)time * modifier);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				float modifier = 1.0f;
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
				{
					modifier = 10.0f;
				}

				camera.move(glm::vec3(0.0f, 0.0f, scrollSpeed * ((camera.getPos().y - minCameraHeight + 10) / maxCameraHeight)) * (float)time * modifier);
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				float modifier = 1.0f;
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
				{
					modifier = 10.0f;
				}

				camera.move(glm::vec3(0.0f, 0.0f, -scrollSpeed * ((camera.getPos().y - minCameraHeight + 10) / maxCameraHeight)) * (float)time * modifier);
			}

			/*
			* set the flag to one ever X sec
			*sets it to 0 afterwards
			* framerate independant
			*/
			if (ticks > tickTime)
			{
				clock.restart();
				tickFlag = true;
				resources.update();
				research.update();
				civHandler.update();
				map.update(resources);
			}
			tickFlag = false;

			desktop->Update(time);

			gui.update(EntityComponentManager::getInstance().getSize());
			std::vector<PhysicsComponent>* physicComponents = EntityComponentManager::getInstance().getPhysicComponents();

			while (time > 0.0)
			{
				for (auto it = physicComponents->begin(); it != physicComponents->end(); ++it)
				{
					it->update(std::min(1.0 / 60, time));
				}

				time -= 1.0 / 60;
			}

			glEnable(GL_DEPTH_TEST);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			state = gui.getToggle();

			if (state == Toggle::CITYMAP)
			{
				mapShader.bind();
				camera.bindUniforms(mapShader.getProgram());
				//pickerShader.bind();
				//camera.bindUniforms(pickerShader.getProgram());

				if (buildMode)
				{
					map.drawTilesOnly(mapShader.getProgram(), buildingToPlace);

					buildmodeShader.bind();
					camera.bindUniforms(buildmodeShader.getProgram());
					map.drawPlaceBuildingGhost(buildingToPlace, (sf::Vector2i)map.convertIdToPos(currentPickedId),
						resources.canAfford(prices.at(buildingToPlace).resWares,
							prices.at(buildingToPlace).resPrice,
							prices.at(buildingToPlace).tradeWares,
							prices.at(buildingToPlace).tradePrice),
						buildmodeShader.getProgram(), 0, &research);

					basicShader.bind();
					camera.bindUniforms(basicShader.getProgram());
					map.drawBuildings(basicShader.getProgram(), 0);
				}
				else
				{
					map.drawTilesOnly(mapShader.getProgram());

					basicShader.bind();
					camera.bindUniforms(basicShader.getProgram());
					map.drawBuildings(basicShader.getProgram(), 0);

					std::vector<GraphicComponent>* components = EntityComponentManager::getInstance().getGraphicComponents();
					for (auto it = components->begin(); it != components->end(); ++it)
					{
						it->draw(basicShader.getProgram());
					}

					grassShader.bind();

					camera.bindUniforms(grassShader.getProgram());
					map.drawFlora(grassShader.getProgram(), camera, totalTimeElapsed);
				}
			}
			else if (state == Toggle::TRADEMAP)
			{
			}
			else
			{
				//worldMap.draw(window);
			}

			glDisable(GL_DEPTH_TEST);
			sfgui->Display(window);
			window.display();

			sf::Time elapsed1 = clock.getElapsedTime();
			ticks = elapsed1.asSeconds();
			//std::cout << tick << " ";
		}
	}
	catch (const std::exception& e)
	{
		printf("Exception thrown: %s\n", e.what());
		system("pause");
	}
}