#pragma once
#include <SFML\Graphics.hpp>
#include <vector>
#include <fstream>
#include <string>
#include <iostream>

class Resources
{
public:
	enum Resource
	{
		WOOD,
		STONE,
		IRON_ORE,
		COAL,
		BUILDING_MATERIAL,
		NUM_OF_RESOURCES
	};
	enum TradeGoods
	{
		//FOOD
		FISH,
		MEAT,
		VEGGIES,
		GRAIN,
		BREAD,
		FLOUR,

		PLANKS,
		IRON_BAR,
		BRICKS,
		LEATHER,
		CLOTH,
		CLOTHING,
		WOOL,
		CLAY,

		IRON_TOOLS,
		IRON_WEAPONS,
		CHARCOAL,
		FIREWOOD,

		FURNITURE,
		POTTERY,
		SALT,
		SPICE,
		TOBACCO,
		CIGARS,
		BEER,
		WHISKEY,
		NUM_OF_TRADEGOODS
	};
	Resources();

	/*
	* change the Resource by x amount
	* @param type of Resource you want to change
	* @param the amount you want the type to change by
	* @return true if the type is >= 0
	*/
	bool addResource(Resource type, int number);
	/*
	* sets the resource to x amount
	* @param type of Resource you want to change
	* @param the amount you want the type to change to
	*/
	void setResource(Resource type, int number);
	/*
	* get the
	* @param type of Resource you want to retrive
	* @return Resource value
	*/
	int getResource(Resource type);
	/*
	* get the resource with
	* @param as name
	*/
	Resource getResourceFromName(std::string name);
	/*
	* increase resource per tick by x amount
	* @param type of resource you want to increase
	* @param amount you want to increase by
	*/
	void addResourcePerTick(Resource type, int number);
	void addResourceLossPerTick(Resource type, int number);
	/*
	* change the tradegoods by x amount
	* @param type of tradegoods you want to change
	* @param the amount you want the type to change by
	* @return true if the type is >= 0
	*/
	bool addTradeGood(TradeGoods type, int number);
	/*
	* sets the tradegoods to x amount
	* @param type of Resource you want to change
	* @param the amount you want the type to change to
	*/
	void setTradeGood(TradeGoods type, int number);
	/*
	* get the
	* @param type of tradegoods you want to retrive
	* @return tradegoods value
	*/
	int getTradeGoods(TradeGoods type);
	/*
	* get tradegoods from
	* @param of tradegoods
	*/
	TradeGoods getTradeGoodsFromName(std::string name);
	/*
	* increase tradegoods per tick by x amount
	* @param type of tradegoods you want to increase
	* @param amount you want to increase by
	*/
	void addTradeGoodsPerTick(TradeGoods type, int number);
	/**
	* increase tradegoods lost per tick by x amount
	* @param type of tradegoods loos you want to increase
	* @param amount you want to increase by
	*/
	void addTradeGoodsLossPerTick(TradeGoods type, int number);
	/*
	* get a vector with resource names
	* @return a vector with resource names
	*/
	std::vector<sf::String>* getResourceName();
	/*
	* get a vector with tradegoods names
	* @return a vector with tradegoods names
	*/
	std::vector<sf::String>* gettradeGoodsName();
	/*
	* get the amount of gold
	* @return an int with the current amount of gold
	*/
	int getGold();
	/*
	* change gold by x amount
	* @param the amount you want the current gold amount changed by
	*/
	void addGold(int amount);
	/*
	* checks if you can afford the change in gold amount
	* @param the amount you want the current gold amount changed by
	* @return true if gold is >= 0 when the changes has been done
	*/
	bool canAfford(int amount);
	/**
	* checks if you can afford
	* @param the res you want to trade
	* @param the price of res
	* @param the trade goods you want to trade
	* @param the price of trade goods
	*/
	bool canAfford(std::vector<Resource> res, std::vector<int> resPrice, std::vector<TradeGoods> tradeWares, std::vector<int> tradePrice);
	/**
	* updates the resources
	*/
	void update();
	/**
	* pays for a building
	* @param the type of res
	* @param the price of res
	* @param the type of tradegoods
	* @param the price of trade goods
	*/
	void buyBuilding(std::vector<Resource> res, std::vector<int> resPrice, std::vector<TradeGoods> tradeWares, std::vector<int> tradePrice);
	/**
	* get an int pointer to the res at i
	* @param the id of the res
	*/
	int* resArray(int i);
	/**
	* get an int pointer to the trade goods at i
	* @param the id of the trade goods
	*/
	int* tgArray(int i);
	/**
	* int pointer to the gold var
	*/
	int* goldPointer();
	/**
	* gets an int pointer with the res per tick
	* @ param the id of the res
	*/
	int* getResPerTick(int i);
	/**
	* gets an int pointer with the trade goods per tick
	* @ param the id of the trade goods
	*/
	int* getTradePerTick(int i);

private:
	int gold;
	int resources[NUM_OF_RESOURCES];
	int tradeGoods[NUM_OF_TRADEGOODS];

	int resourcesPerTick[NUM_OF_RESOURCES];
	int tradeGoodsPerTick[NUM_OF_TRADEGOODS];

	int resourceLossPerTick[NUM_OF_RESOURCES];
	int tradeGoodsLossPerTick[NUM_OF_TRADEGOODS];

	std::vector<sf::String> resourcesName;
	std::vector<sf::String> tradeGoodsName;
};
