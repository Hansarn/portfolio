#pragma once
#include <SFML\Graphics.hpp>
#include <vector>
#include <fstream>
#include <string>
#include <iostream>

#include "Research.h"

class ResearchHandler
{
public:
	/**
	* constructor for the researchhandler. Reads all research from file.
	*/
	ResearchHandler();
	/**
	* returns a pointer to a vector with all research objects
	*/
	std::vector<Research>* getResearch();
	/**
	* updates the research
	*/
	void update();
	/**
	* sets which research is activ
	* @param the id of the research
	*/
	bool setActiveResearch(int id);
	/**
	* checks if a research can be researcheed
	* @param the id of the research
	*/
	bool canBeResearched(int id);
	/*
	* checks if a research is finished
	* @param id of the research
	* @return true if the research is finished
	*/
	bool isResearched(int id);

	/*
	* use id if you got one or is search on name
	*/
	bool isResearched(std::string name);
	/**
	* returns true if any changes has been done.
	*/
	bool getChanged();
	/**
	* sets the changed status
	* @ param The change bool
	*/
	void setChanged(bool changed);
	/**
	* gets a research by name
	* @param the name of the research
	*/
	Research* getResearch(std::string name);

private:
	void inheritance(std::vector<Research> tempRes);

	std::vector<Research> research;
	int activeID;
	bool changed;
};
