#pragma once

#include <cstddef>

enum componentTypes
{
	GRAPHICS_COMPONENT,
	INPUT_COMPONENT,
	PHYSICS_COMPONENT,
	POSITION_COMPONENT,
	PATHING_COMPONENT,
	NUMBER_OF_COMPONENTS
};

struct GameEntity
{
	/**
	* The entities id
	*/
	std::size_t id;

	/**
	* Components id for all components owned by this entity
	*/
	std::size_t components[NUMBER_OF_COMPONENTS];
};