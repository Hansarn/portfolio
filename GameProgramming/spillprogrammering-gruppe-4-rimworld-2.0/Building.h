#pragma once

#include "BuildingLevel.h"

enum Type
{
	HOUSE,
	RESOURCE,
	GOVERNMENT,
	CHURCH
};

class Building
{
public:
	std::vector<BuildingLevel> levels;
	sf::Vector2i sizeInTiles;
	Resources::Resource tileReq;
	Type type;

	Building();

	~Building();
};
