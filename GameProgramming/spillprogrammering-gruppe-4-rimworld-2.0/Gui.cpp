#include "Gui.h"

Gui::Gui(CivHandler *civ, std::vector<Building>* buildings, Resources* resources, Trade* trade, ResearchHandler* res)
{
	toggle = CITYMAP;

	this->resources = resources;
	this->trade = trade;

	if (!desktop.LoadThemeFromFile("spillprogrammering-gruppe-4-rimworld-2.0/MyTheme.theme"))
	{
		printf("ERROR THEME DOES NOT LOAD");
	}

	font = std::make_shared<sf::Font>();
	font->loadFromFile("spillprogrammering-gruppe-4-rimworld-2.0/BLKCHCRY.TTF");
	desktop.GetEngine().GetResourceManager().AddFont("custom_font", font);

	// sets up the different menues
	city.citySetUp(&events, buildings, resources);
	research.setUp(res);
	info.setUpInfoWindow(resources, &research, &events);
	world.worldMapSetUp(&events);
	popUp.SetUp();
	menu.setUpMenu(&events);
	startMenu.setUp(&events);
	tradeGuiTest.tradeSetUp(resources, civ, trade, &popUp);

	mainMenu[WORLDMAP] = createmainMenu(WORLDMAP);
	mainMenu[TRADEMAP] = createmainMenu(TRADEMAP);
	mainMenu[CITYMAP] = createmainMenu(CITYMAP);
	auto window = sfg::Window::Create();
	window->SetPosition(sf::Vector2f(0, 0));
	window->SetStyle(window->GetStyle() ^ sfg::Window::TITLEBAR ^ sfg::Window::RESIZE);
	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	box->SetSpacing(50.f);
	box->SetRequisition(sf::Vector2f(100, 0));
	window->Add(box);
	for (size_t i = 0; i < NumberOfToggle; i++)
	{
		box->Pack(mainMenu[i], false, false);
	}
	city.AddToBox(box, &desktop);
	world.AddToBox(box);

	popUp.addToDesktop(&desktop);
	// adds everything to Desktop
	info.addWindows(&desktop);
	research.setDesktop(&desktop);
	menu.AddToDesktop(&desktop);
	//tradeGui.addToDesktop(&desktop);
	tradeGuiTest.addToDesktop(&desktop);
	desktop.Add(window);
	startMenu.AddToDesktop(&desktop);

	EventQueue<GUIEvent>* tempEvents = &events;
	box->GetSignal(sfg::Widget::OnMouseEnter).Connect([tempEvents] {
		GUIEvent event;
		event.type = GUIEvent::FOCUS;
		event.focus.focus = true;
		tempEvents->pushEvent(event); });

	box->GetSignal(sfg::Widget::OnMouseLeave).Connect([tempEvents] {
		GUIEvent event;
		event.type = GUIEvent::FOCUS;
		event.focus.focus = false;
		tempEvents->pushEvent(event); });

	toggleMap(toggle);
}

sfg::Desktop* Gui::getDesktop()
{
	return &desktop;
}

sfg::SFGUI * Gui::getSFGUI()
{
	return &sfgui;
}

void Gui::escManu()
{
	if (menu.toggleMenu())
	{
		for (size_t i = 0; i < NumberOfToggle; i++)
		{
			mainMenu[i]->Show(false);
		}
		city.activateCityBuildingGui(false);
		world.activateWorldMapGui(false);
		//tradeGui.activateTradeGui(false);
		tradeGuiTest.activateTradeGui(false);
		info.showWindow(false);
		research.show(false);
	}
	else
	{
		toggleMap(toggle);
	}
}

Toggle Gui::getToggle()
{
	return toggle;
}

void Gui::update(int pawns)
{
	info.update(pawns);
	research.update();
}

void Gui::displayBuildingMenu(bool active)
{
	city.activateBuildMenu(active);
}

void Gui::removeBuildingInfo()
{
	city.removeBuildingInfo();
}

void Gui::activatePopUp(std::string text)
{
	popUp.activatePopUp(true, text);
}

void Gui::displayBuilding(building* b)
{
	city.displayBuilding(b);
}

void Gui::toggleMap(Toggle clicked)
{
	mainMenu[toggle]->Show(false);

	toggle = clicked;
	mainMenu[toggle]->Show(true);
	city.activateCityBuildingGui(toggle == CITYMAP);
	world.activateWorldMapGui(toggle == WORLDMAP);
	tradeGuiTest.activateTradeGui(toggle == TRADEMAP);
	info.showWindow(toggle == CITYMAP || toggle == TRADEMAP);
	research.show(false);
}

sfg::Table::Ptr Gui::createmainMenu(Toggle map)
{
	EventQueue<GUIEvent>* tempEvents = &events;
	sfg::Button::Ptr button;
	sfg::Button::Ptr button2;
	switch (map)
	{
	case WORLDMAP:
		button2 = sfg::Button::Create("Trade");
		button2->GetSignal(sfg::Widget::OnLeftClick).Connect([this] {this->toggleMap(TRADEMAP); });
		button = sfg::Button::Create("City");
		button->GetSignal(sfg::Widget::OnLeftClick).Connect([this] {this->toggleMap(CITYMAP); });

		break;
	case TRADEMAP:
		button = sfg::Button::Create("City");
		button->GetSignal(sfg::Widget::OnLeftClick).Connect([this] {this->toggleMap(CITYMAP); });
		button2 = sfg::Button::Create("WorldMap");
		button2->GetSignal(sfg::Widget::OnLeftClick).Connect([this] {this->toggleMap(WORLDMAP);  });
		break;
	case CITYMAP:
		button = sfg::Button::Create("Trade");
		button->GetSignal(sfg::Widget::OnLeftClick).Connect([this] {this->toggleMap(TRADEMAP); });
		button2 = sfg::Button::Create("WorldMap");
		button2->GetSignal(sfg::Widget::OnLeftClick).Connect([this] {this->toggleMap(WORLDMAP);  });
		break;
	case NumberOfToggle:
		break;
	default:
		break;
	}
	sfg::Table::Ptr temp = sfg::Table::Create();
	auto temp2 = sfg::Box::Create();

	temp->Attach(button, sf::Rect<sf::Uint32>(0, 0, 1, 1));
	temp->Attach(button2, sf::Rect<sf::Uint32>(0, 1, 1, 1));

	temp->Show(false);
	temp->SetAllocation(sf::FloatRect(0, 0, 200, 300));
	temp->SetRequisition(sf::Vector2f(0, 250));
	return temp;
}