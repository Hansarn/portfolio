#pragma once
#include<SFML\Graphics.hpp>
#include<vector>
#include<string>

class Research
{
public:
	/**
	* Creates a new research
	* @param the id of the research
	* @param name of the research
	* @param the discription
	* @param how long it takes to finish the research
	*/
	Research(int id, sf::String tittle, sf::String text, int time);
	/**
	* chechs if the research is finnished
	*/
	bool isFinished();
	/**
	* add the id of an requred research
	* @param the id of the research
	*/
	void addreqid(int id);
	/**
	* returns the requirements of a research
	*/
	std::vector<int> getReq();
	/**
	* updates the time left
	* @ param the amount to change by
	*/
	bool updateTime(int i);
	/**
	* sets the level of the research
	* @ param the level
	*/
	void setLevel(int level);
	/**
	* gets the research name
	*/
	std::string getName();
	/**
	* gets the discription
	*/
	std::string getText();
	/**
	* gets the level
	*/
	int getLevel();
	/**
	* gets the id
	*/
	int getId();

private:
	int id;
	sf::String tittle;
	sf::String text;
	std::vector<int> req;
	int time;
	int maxTime;
	int level;

	bool finished;
	bool canBeResearch;
};
