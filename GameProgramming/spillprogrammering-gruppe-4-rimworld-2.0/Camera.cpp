#include "Camera.h"

Camera::Camera(float resolution, glm::vec3 position, glm::vec3 front)
{
	lookAt(position, front);
	setProjection(0.1f, 60000.0f, resolution);
}

Camera::~Camera()
{
}

void Camera::move(glm::vec3 offset)
{
	position += offset;
	viewMatrix = glm::lookAt(position, position + front, up);
}

void Camera::lookAt(glm::vec3 position, glm::vec3 front)
{
	this->position = position;
	this->front = front;
	viewMatrix = glm::lookAt(position, position + front, up);
}

void Camera::setProjection(float cameraNear, float cameraFar, float resolution, float fov)
{
	projectionMatrix = glm::perspective(fov, resolution, cameraNear, cameraFar);
}

void Camera::bindUniforms(GLuint shaderProgram)
{
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "view"), 1, false, glm::value_ptr(viewMatrix));
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "projection"), 1, false, glm::value_ptr(projectionMatrix));
}

glm::vec3 Camera::getPos()
{
	return position;
}