#pragma once

#include <GL\glew.h>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

#include "TextureHandler.h"

/// Class for loading meshes

struct subShapes
{
	unsigned int startingIndex;
	unsigned int numOfIndices;

	GLuint textureId;
};

class Mesh
{
public:
	/**
	* Constructs a new Mesh from a filepath with a given scale
	*
	* @param filePath Path to mesh file (.obj)
	* @param scale Modifier to scale the mesh with
	*/
	Mesh(std::string filePath, double scale);
	~Mesh();

	/**
	* Draws the mesh
	*/
	void draw();

	/**
	* The path used to load this mesh
	*/
	std::string path;

private:
	GLuint vertexArrayObject;

	unsigned int amountOfIndices;
	std::vector<subShapes> shapes;
};
