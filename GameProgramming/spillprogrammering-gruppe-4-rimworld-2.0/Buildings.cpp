#include "Buildings.h"

/*
*
*	BUILDING TEMPLATE/PATTERN
*
*
*	GET THESE ONCE PER BUILDING
*	_________________________________________________________________________________________________
*	|																								|
*	| SIZE	----------------------------------------------------------------------------   TWO INTS	|
*	| REQUIRED RESOURCE	-------------------------------------------------------------------- STRING	|
*	| BUILDING TYPE	------------------------------------------------------------------------	INT	|
*	|_______________________________________________________________________________________________|
*
*
*	REPEAT THE FOLLOWING THREE TIMES FOR EACH BUILDING
*	_________________________________________________________________________________________________
*	|																								|
*	| NAME	-------------------------------------------------------------------------------- STRING	|
*	| FLAVOUR TEXT	--------------------------------------------  MULTI-LINE STRING; READ UNTIL "}"	|
*	| MAX PAWNS	----------------------------------------------------------------------------	INT	|
*	| OUTPUT RESOURCES	------------	MULTI-LINE, ONE STRING AND ONE INT PER LINE, READ UNTIL "}"	|
*	| OUTPUT TRADEGOODS	------------	MULTI-LINE, ONE STRING AND ONE INT PER LINE, READ UNTIL "}"	|
*	| RESOURCE PRICE	------------	MULTI-LINE, ONE STRING AND ONE INT PER LINE, READ UNTIL "}"	|
*	| TRADEGOODS PRICE	------------	MULTI-LINE, ONE STRING AND ONE INT PER LINE, READ UNTIL "}"	|
*	| RESOURCE USE PER TICK		----	MULTI-LINE, ONE STRING AND ONE INT PER LINE, READ UNTIL "}"	|
*	| TRADEGOODS USE PER TICK	----	MULTI-LINE, ONE STRING AND ONE INT PER LINE, READ UNTIL "}"	|
*	| MODEL NAME	--------------------------------------------------------------------	 STRING	|
*	| REQUIRED RESEARCH	----------------------------------------------------------------	 STRING	|
*	| BUILDING TIME	------------------------------------------------------------------------  FLOAT	|
*	|_______________________________________________________________________________________________|
*
*/

Buildings::Buildings(Resources* resources, ResearchHandler* research)
{
	std::cout << "Hello!\n";

	std::ifstream file("./spillprogrammering-gruppe-4-rimworld-2.0/data/Buildings.data");

	std::cout << "There's the file!\n";
	std::vector<sf::Vector2i> sizes;
	std::vector<std::string> requirements;
	std::vector<Type> types;
	std::vector<std::vector<std::string>> names;
	std::vector<std::vector<std::string>> texts;
	std::vector<std::vector<int>> maxPawns;
	std::vector<std::vector<ItemSet>> outputs;
	std::vector<std::vector<ItemSet>> prices;
	std::vector<std::vector<ItemSet>> resPerTick;
	std::vector<std::vector<std::string>> models;
	std::vector<std::vector<std::vector<std::string>>> researches;
	std::vector<std::vector<float>> buildTimes;
	if (!file.is_open())
	{
		printf("can't load file\n");
	}
	else
	{
		std::string name, text = "", temp;

		while (!file.eof())
		{
			sf::Vector2i size;
			std::getline(file, temp);
			if (temp == "")
			{
				break;
			}
			size.x = std::stoi(temp);
			std::getline(file, temp);
			size.y = std::stoi(temp);

			sizes.push_back(size);
			std::string requirement;
			std::getline(file, requirement);

			requirements.push_back(requirement);
			//	Building type
			int tempInt;
			std::getline(file, temp);
			tempInt = stoi(temp);
			types.push_back((Type)tempInt);
			text.clear();

			std::vector<std::string > tempNames;
			std::vector<std::string > tempTexts;
			std::vector<int > tempMaxPawns;
			std::vector<ItemSet > tempOutputs;
			std::vector<ItemSet > tempPrices;
			std::vector<ItemSet > tempResPerTick;
			std::vector<std::string > tempModels;
			std::vector<std::vector<std::string>> tempResearches;
			std::vector<float > tempBuildTimes;

			for (size_t i = 0; i < 3; i++)
			{
				//	Name
				std::string name;
				std::getline(file, name);
				tempNames.push_back(name);

				temp.clear();
				//	Flavourtext
				std::getline(file, temp);
				while (temp != "}")
				{
					text.append(temp);
					text.append("\n");
					temp.clear();
					std::getline(file, temp);
				}
				tempTexts.push_back(text);
				temp.clear();

				//	Max pawns

				std::getline(file, temp);
				tempInt = std::stoi(temp);
				tempMaxPawns.push_back(tempInt);

				//	Output resources

				ItemSet tempSet;

				std::vector<ResWares> tempResWares;
				ResWares tempResWare;

				std::vector<TradeWares> tempTradeWares;
				TradeWares tempTradeWare;

				std::getline(file, temp);
				while (temp != "}")
				{
					tempResWare.resItem = resources->getResourceFromName(temp);
					std::getline(file, temp);
					tempResWare.amount = std::stoi(temp);
					std::getline(file, temp);
					tempResWares.push_back(tempResWare);
				}
				std::getline(file, temp);
				tempSet.resWares = tempResWares;

				while (temp != "}")
				{
					tempTradeWare.tradeItem = resources->getTradeGoodsFromName(temp);
					temp.clear();
					std::getline(file, temp);
					tempTradeWare.amount = std::stoi(temp);
					std::getline(file, temp);
					tempTradeWares.push_back(tempTradeWare);
				}
				std::getline(file, temp);
				tempSet.tradeWares = tempTradeWares;

				tempOutputs.push_back(tempSet);

				tempSet.resWares.clear();
				tempSet.tradeWares.clear();
				tempResWares.clear();
				tempTradeWares.clear();

				while (temp != "}")
				{
					tempResWare.resItem = resources->getResourceFromName(temp);
					std::getline(file, temp);
					tempResWare.amount = std::stoi(temp);
					std::getline(file, temp);
					tempResWares.push_back(tempResWare);
				}
				std::getline(file, temp);
				tempSet.resWares = tempResWares;

				while (temp != "}")
				{
					tempTradeWare.tradeItem = resources->getTradeGoodsFromName(temp);
					temp.clear();
					std::getline(file, temp);
					tempTradeWare.amount = std::stoi(temp);
					std::getline(file, temp);
					tempTradeWares.push_back(tempTradeWare);
				}
				std::getline(file, temp);
				tempSet.tradeWares = tempTradeWares;

				tempPrices.push_back(tempSet);

				tempSet.resWares.clear();
				tempSet.tradeWares.clear();
				tempResWares.clear();
				tempTradeWares.clear();

				while (temp != "}")
				{
					tempResWare.resItem = resources->getResourceFromName(temp);
					std::getline(file, temp);
					tempResWare.amount = std::stoi(temp);
					std::getline(file, temp);
					tempResWares.push_back(tempResWare);
				}
				std::getline(file, temp);
				tempSet.resWares = tempResWares;

				while (temp != "}")
				{
					tempTradeWare.tradeItem = resources->getTradeGoodsFromName(temp);
					temp.clear();
					std::getline(file, temp);
					tempTradeWare.amount = std::stoi(temp);
					std::getline(file, temp);
					tempTradeWares.push_back(tempTradeWare);
				}

				tempSet.tradeWares = tempTradeWares;

				tempResPerTick.push_back(tempSet);

				temp.clear();

				std::getline(file, temp);
				tempModels.push_back(temp);

				temp.clear();

				std::getline(file, temp);

				std::vector<std::string> tempRes;

				while (temp != "}")
				{
					tempRes.push_back(temp);
					std::getline(file, temp);
				}
				tempResearches.push_back(tempRes);

				std::getline(file, temp);

				float tempBuildTime = std::stof(temp);

				tempBuildTimes.push_back(tempBuildTime);
			}

			names.push_back(tempNames);
			texts.push_back(tempTexts);
			maxPawns.push_back(tempMaxPawns);
			outputs.push_back(tempOutputs);
			prices.push_back(tempPrices);
			resPerTick.push_back(tempResPerTick);
			models.push_back(tempModels);

			researches.push_back(tempResearches);

			buildTimes.push_back(tempBuildTimes);
			std::getline(file, temp);
		}
	}

	file.close();

	std::cout << "Managed to read from file!\n";

	for (size_t i = 0; i < sizes.size(); i++)
	{
		Building temp;

		temp.sizeInTiles = sizes[i];
		if (requirements[i] == "None")
		{
			temp.tileReq = Resources::NUM_OF_RESOURCES;
		}
		else
		{
			temp.tileReq = resources->getResourceFromName(requirements[i]);
		}

		temp.type = types[i];

		for (size_t j = 0; j < names[i].size(); j++)
		{
			BuildingLevel tempLevel;

			tempLevel.name = names[i][j];

			tempLevel.description = texts[i][j];

			tempLevel.maxPawns = maxPawns[i][j];

			tempLevel.outPut = outputs[i][j];

			tempLevel.price = prices[i][j];

			tempLevel.consumed = resPerTick[i][j];

			tempLevel.model = "./spillprogrammering-gruppe-4-rimworld-2.0/models/" + models[i][j] + "/" + models[i][j] + ".obj";
			for (int k = 0; k < researches[i][j].size(); ++k)
			{
				if (researches[i][j][k] != "None")
				{
					tempLevel.required.push_back(research->getResearch(researches[i][j][k]));
				}
			}

			tempLevel.buildingTime = buildTimes[i][j];

			temp.levels.push_back(tempLevel);
		}
		buildingTypes.push_back(temp);
	}

	std::cout << "Buildings loaded!\n";
}

Buildings::~Buildings()
{
}