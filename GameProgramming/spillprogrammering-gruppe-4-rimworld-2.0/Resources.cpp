#include "Resources.h"

Resources::Resources()
{
	try
	{
		std::ifstream file("./spillprogrammering-gruppe-4-rimworld-2.0/data/Resources.data");
		std::vector<std::vector<std::string>> requirements;
		std::vector<std::string> names;

		std::string name, text = "", temp;
		while (temp != "*/")
		{
			std::getline(file, temp);
		}

		for (size_t i = 0; i < NUM_OF_RESOURCES; i++)
		{
			std::getline(file, temp);
			int f = temp.find("=");
			resources[i] = std::stoi(temp.substr(f + 2));
			resourcesPerTick[i] = 0;
			resourceLossPerTick[i] = 0;
			resourcesName.push_back(temp.substr(0, f - 1));
		}
		for (size_t i = 0; i < NUM_OF_TRADEGOODS; i++)
		{
			std::getline(file, temp);
			int f = temp.find("=");
			tradeGoods[i] = std::stoi(temp.substr(f + 2));
			tradeGoodsPerTick[i] = 0;
			tradeGoodsLossPerTick[i] = 0;
			tradeGoodsName.push_back(temp.substr(0, f - 1));
		}

		std::getline(file, temp);
		int f = temp.find("=");
		gold = std::stoi(temp.substr(f + 2));
	}
	catch (const std::exception& e)
	{
		printf("%s\n", e.what());
	}
}

bool Resources::addResource(Resource type, int number)
{
	if (resources[type] + number >= 0)
	{
		resources[type] += number;
		return true;
	}
	else
	{
		return false;
	}
}

void Resources::setResource(Resource type, int number)
{
	resources[type] = number;
}

int Resources::getResource(Resource type)
{
	return resources[type];
}

Resources::Resource Resources::getResourceFromName(std::string name)
{
	std::transform(name.begin(), name.end(), name.begin(), ::toupper);
	for (size_t i = 0; i < resourcesName.size(); i++)
	{
		std::string temp = resourcesName[i];
		std::transform(temp.begin(), temp.end(), temp.begin(), ::toupper);
		if (name == temp)
		{
			return (Resource)i;
		}
	}

	return NUM_OF_RESOURCES;
}

void Resources::addResourcePerTick(Resource type, int number)
{
	resourcesPerTick[type] += number;
}

void Resources::addResourceLossPerTick(Resource type, int number)
{
	resourceLossPerTick[type] += number;
}

bool Resources::addTradeGood(TradeGoods type, int number)
{
	if (tradeGoods[type] + number >= 0)
	{
		tradeGoods[type] += number;
		return true;
	}
	else
	{
		return false;
	}
}

void Resources::setTradeGood(TradeGoods type, int number)
{
	tradeGoods[type] = number;
}

int Resources::getTradeGoods(TradeGoods type)
{
	return tradeGoods[type];
}

Resources::TradeGoods Resources::getTradeGoodsFromName(std::string name)
{
	std::transform(name.begin(), name.end(), name.begin(), ::toupper);
	for (size_t i = 0; i < tradeGoodsName.size(); i++)
	{
		std::string temp = tradeGoodsName[i];
		std::transform(temp.begin(), temp.end(), temp.begin(), ::toupper);

		if (name == temp)
		{
			return (TradeGoods)i;
		}
	}
	return NUM_OF_TRADEGOODS;
}

void Resources::addTradeGoodsPerTick(TradeGoods type, int number)
{
	tradeGoodsPerTick[type] += number;
}

void Resources::addTradeGoodsLossPerTick(TradeGoods type, int number)
{
	tradeGoodsLossPerTick[type] += number;
}

std::vector<sf::String>* Resources::getResourceName()
{
	return &resourcesName;
}

std::vector<sf::String>* Resources::gettradeGoodsName()
{
	return &tradeGoodsName;
}

int Resources::getGold()
{
	return gold;
}

void Resources::addGold(int amount)
{
	if (gold + amount >= 0)
	{
		gold += amount;
	}
}

bool Resources::canAfford(int amount)
{
	return (gold + amount >= 0);
}

bool Resources::canAfford(std::vector<Resource> res, std::vector<int> resPrice, std::vector<TradeGoods> tradeWares, std::vector<int> tradePrice)
{
	for (size_t i = 0; i < res.size(); i++)
	{
		if (resources[res[i]] < resPrice[i])
		{
			return false;
		}
	}
	for (size_t i = 0; i < tradeWares.size(); i++)
	{
		if (tradeGoods[tradeWares[i]] < tradePrice[i])
		{
			return false;
		}
	}
	return true;
}

void Resources::update()
{
	for (size_t i = 0; i < NUM_OF_RESOURCES; i++)
	{
		resources[i] += resourcesPerTick[i];
		if (resources[i] - resourceLossPerTick[i] > 0)
		{
			resources[i] -= resourceLossPerTick[i];
		}
	}
	for (size_t i = 0; i < NUM_OF_TRADEGOODS; i++)
	{
		tradeGoods[i] += tradeGoodsPerTick[i];
		if (tradeGoods[i] - tradeGoodsLossPerTick[i] > 0)
		{
			tradeGoods[i] -= tradeGoodsLossPerTick[i];
		}
	}
}

void Resources::buyBuilding(std::vector<Resource> res, std::vector<int> resPrice, std::vector<TradeGoods> tradeWares, std::vector<int> tradePrice)
{
	printf("flag 1\n");
	for (size_t i = 0; i < res.size(); i++)
	{
		resources[res[i]] -= resPrice[i];
		printf("%i\n", i);
	}
	for (size_t i = 0; i < tradeWares.size(); i++)
	{
		tradeGoods[tradeWares[i]] -= tradePrice[i];
		printf("%i\n", i);
	}
}

int* Resources::resArray(int i)
{
	return &resources[i];
}

int* Resources::tgArray(int i)
{
	return &tradeGoods[i];
}

int * Resources::goldPointer()
{
	return &gold;
}

int * Resources::getResPerTick(int i)
{
	return &resourcesPerTick[i];
}

int* Resources::getTradePerTick(int i)
{
	return &tradeGoodsPerTick[i];
}