#pragma once
#include <string>
#include <SFML\Graphics.hpp>
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>
#include <LuaBridge\LuaBridge.h>
#include <time.h>

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

#include "Resources.h"
#include "Trade.h"

class Trade;
class Civ;
struct TradeSum;

struct Trait
{
	int id;
	std::string name;
	std::function<void(Civ*)> TraitFunction;
};

class Civ
{
public:
	enum Government
	{
		MONARCHY,
		REPUBLIC,
		THEOCRACY,
		NUM_OF_GOVERNMENTS
	};

	enum Vars
	{
		ROA,//ReasonableOfferAcceptance -1 to 1; -1 it will not accept a reasonable deal +1 it will be willing to accept worse deals
		ROG,//ReasonableOfferGiver -1 to 1; -1 it will not offer a reasonable deal +1 it will be offer better deals
		ND//NegotiatingDifficulty level 1 to 5 how good he is in negotiating
	};

	enum State
	{
		WAITING, // waiting for something
		BUILDING, // building a building
		TRADING, // doing trade
		READY // ready to do shit
	};

	/*
	* check if the civ wants the trade
	* @param a vector with what the player want to trade
	* @param a vector with what the player want the civ to trade
	* @param a string that returns a text with a response
	* @return true if the  civ wants the trade
	*/
	bool willTrade(std::vector<TradeSum> player, std::vector<TradeSum> civTrade, std::string& text);
	/*
	* returns the name of the civ
	* @return civ name string
	*/
	std::string getName();
	/*
	* returns the gold value of the civ
	* @return int gold value
	*/
	int getGold();
	/*
	* returns the resources of the civ
	* @return pointer to civs resources
	*/
	Resources* getResource();
	/*
	* create a civ
	* @param name of the civ
	* @param all the different traits
	*/
	Civ(std::string name, std::vector<Trait>* DifferentTraits);
	/*
	* return the civs icon
	* @return icon
	*/
	sfg::Image::Ptr getIcon();
	/**
	* gets information string about the civ
	*/
	std::string getInfoString();
	/**
	* updates the civ and make the civ do choices
	*/
	void update();
	/**
	* changes different variables by x amount
	* @ param the variable to be changed
	* @ param the amount it will be changed by
	*/
	void setVers(Vars var, float amount);

private:
	void setUpTraits();
	void setUpGoalState();
	void createBuilding(int i, int f);
	void getResourceBuildingHandler();
	void getNeeds();
	void getWants();

	int getFood();

	std::string name;
	sf::Vector2i location;

	float hostility;

	Resources resources;

	Government governmentType;

	State civState;
	int timer;

	int rank;
	sfg::Image::Ptr icon;
	std::string infoString;
	std::vector<int> traits;
	std::vector<Trait>* differentTraits;
	std::vector<float> goalState;

	// trade/ traits vars

	float opinionOfPlayer;
	float reasonableOfferAcceptance; // -1 to 1; -1 it will not accept a reasonable deal +1 it will be willing to accept worse deals
	float reasonableOfferGiver;
	float negotiatingDifficulty;
	// lua stuff

	luabridge::lua_State* L;

	float needs[4];
	float wants[7];
	float resource[4];
};
