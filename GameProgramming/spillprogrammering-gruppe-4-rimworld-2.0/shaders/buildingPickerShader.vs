#version 330 core

layout(location = 0) in vec3 vertexPos;

uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;

uniform int id;

out vec4 colors;


void main()
{
	int rValue = (id / 65536) % 256;
	int gValue = (id / 256) % 256;
	int bValue = id % 256;
	colors = vec4(rValue / 255.0f, gValue / 255.0f, bValue / 255.0f, 1.0f);
	
	gl_Position = projection * view * model * vec4(vertexPos, 1.0);
}