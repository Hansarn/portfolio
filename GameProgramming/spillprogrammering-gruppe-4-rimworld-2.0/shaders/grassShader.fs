#version 330 core

smooth in vec2 vTexCoord; 
smooth in vec3 vWorldPos; 
smooth in vec4 vEyeSpacePos; 
flat in float vLength;

out vec4 outputColor; 

uniform sampler2D texture; 
//uniform vec4 vColor; 

void main() 
{ 
   vec4 vTexColor = texture2D(texture, vTexCoord); 
   float fNewAlpha = vTexColor.a * 0.5 * (1.0 - (vLength / 1500.0f)); //vTexColor.a * 1.5 * (1.0 - (vLength / 2000.0f));                
   //float fNewAlpha = vTexColor.a * 0.2;
   if(fNewAlpha < 0.0001) 
      discard; 
    
   if(fNewAlpha > 1.0f) 
      fNewAlpha = 1.0f;    
       
   vec4 vMixedColor = vTexColor* 1.0;//vColor;  
    
   outputColor = vec4(vMixedColor.zyx, fNewAlpha); 
}