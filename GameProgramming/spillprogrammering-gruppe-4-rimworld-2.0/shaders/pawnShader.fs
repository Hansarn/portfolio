#version 330 core

out vec4 color;

in vec3 diffuseColor;
in vec3 specularColor;
in vec2 textureCoordinates;

uniform sampler2D texture0;

void main()
{
	color = vec4(texture(texture0, textureCoordinates).xyz, 1.0);
	if(color.x == 0.0 && color.y == 0.0 && color.z == 0.0)
	{
		color = vec4(diffuseColor, 1.0);
	}
}