#version 330 core

in vec2 textureCoordinates;
out vec4 color;

uniform sampler2D texture0;

void main()
{
	//color = vec4(texture(texture0, textureCoordinates.xy).xyz, 1.0);
	color = vec4(0.8, 0.5, 0.3, 1.0);
}