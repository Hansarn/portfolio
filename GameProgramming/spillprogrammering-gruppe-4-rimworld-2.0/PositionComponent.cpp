#include "PositionComponent.h"

PositionComponent::PositionComponent(std::size_t entityId) : ComponentBase(entityId)
{}

PositionComponent::PositionComponent()
{}

PositionComponent::~PositionComponent()
{}

void PositionComponent::recieveMessage(char message[], void * data)
{
	if (strcmp(message, "PositionOffset") == 0)
	{
		updatePos(*static_cast<glm::vec2*>(data));
	}
}

void PositionComponent::setUp(std::shared_ptr<std::vector<std::vector<int>>> heightMap, int tileSize)
{
	this->heightMap = heightMap;
	this->tileSize = tileSize;
}

void PositionComponent::setPosition(glm::vec2& pos)
{
	position = glm::vec3(0.0, 0.0, 0.0);

	updatePos(pos);
}

glm::vec3& PositionComponent::getPosition()
{
	return position;
}

void PositionComponent::updatePos(glm::vec2& offset)
{
	if (position.x + offset.x >= 0 && position.x + offset.x < ((int)heightMap->size() - 1) * tileSize &&
		position.z + offset.y >= 0 && position.z + offset.y < ((int)heightMap->size() - 1) * tileSize)
	{
		position.x += offset.x;
		position.z += offset.y;

		int x = (int)position.x / tileSize;
		int z = (int)position.z / tileSize;
		double deltaX = ((position.x / (float)tileSize) - x);
		double deltaZ = ((position.z / (float)tileSize) - z);

		double lerp1 = ((1.0 - deltaX) * (*heightMap)[x][z]) + (deltaX * ((*heightMap)[x + 1][z]));
		double lerp2 = ((1.0 - deltaX) * (*heightMap)[x][z + 1]) + (deltaX * ((*heightMap)[x + 1][z + 1]));

		position.y = (1.0 - deltaZ) * lerp1 + deltaZ * lerp2;

		EntityComponentManager::getInstance().sendMessageToComponent(GRAPHICS_COMPONENT, parentId, "NewPosition", &position);
	}
}