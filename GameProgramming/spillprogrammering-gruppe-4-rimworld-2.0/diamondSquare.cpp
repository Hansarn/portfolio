#include "diamondSquare.h"

namespace diamondSquare
{
	std::vector<std::vector<int>> diamondSquare(int size, int range, int values[4])
	{
		std::random_device rd;
		std::mt19937_64 randomGenerator;
		//std::string test = "test";
		//int x = std::stoi(test, nullptr);
		randomGenerator = std::mt19937_64(rd());
		std::uniform_int_distribution<int> random(-range, range);

		std::vector<std::vector<int>> map;

		int realSize = pow(2, size) + 1;

		map.resize(realSize);
		for (auto i = 0; i < map.size(); ++i)
		{
			map[i].resize(realSize);
		}

		if (values[0] >= -range)
		{
			map[0][0] = values[0];
		}
		else
		{
			map[0][0] = random(randomGenerator);
		}

		if (values[1] >= -range)
		{
			map[0][realSize - 1] = values[1];
		}
		else
		{
			map[0][realSize - 1] = random(randomGenerator);
		}

		if (values[2] >= -range)
		{
			map[realSize - 1][0] = values[2];
		}
		else
		{
			map[realSize - 1][0] = random(randomGenerator);
		}

		if (values[3] >= -range)
		{
			map[realSize - 1][realSize - 1] = values[3];
		}
		else
		{
			map[realSize - 1][realSize - 1] = random(randomGenerator);
		}

		int divider = 1;

		for (int sideLength = realSize - 1; sideLength >= 2; sideLength /= 2, divider *= 2)
		{					//	Basically runs through and keeps finding half of the previous square, meaning
			int halfSide = sideLength / 2;	//	it does indeed find the middle of each square. It does do the thing.

			for (int x = 0; x < realSize - 1; x += sideLength)
			{
				for (int y = 0; y < realSize - 1; y += sideLength)
				{
					int avg = map[x][y]
						+ map[x + sideLength][y]				//	Finds the average value of the values of four
						+ map[x][y + sideLength]				//	points which form a square.
						+ map[x + sideLength][y + sideLength];
					avg /= 4.0;
					map[x + halfSide][y + halfSide] = std::max(std::min(avg + (random(randomGenerator) / divider), range), -range);	//	It then puts the average as
				}																	//	the midpoint's value, but adds
			}																		//	a bit of random to it.
			for (int x = 0; x < realSize; x += halfSide)
			{
				for (int y = (x + halfSide) % sideLength; y < realSize; y += sideLength)
				{
					int corners = 0;

					int avg = 0.0;
					if (x - halfSide >= 0)
					{
						avg += map[x - halfSide][y];
						++corners;
					}

					if (x + halfSide < realSize)
					{
						avg += map[x + halfSide][y];
						++corners;
					}

					if (y + halfSide < realSize)
					{
						avg += map[x][y + halfSide];
						++corners;
					}

					if (y - halfSide >= 0)
					{
						avg += map[x][y - halfSide];
						++corners;
					}
					avg /= corners;

					avg += random(randomGenerator) / divider;

					map[x][y] = std::max(std::min(avg, range), -range);
				}
			}
		}

		for (auto i = 0; i < map.size(); ++i)
		{
			for (auto j = 0; j < map[i].size(); ++j)
			{
				map[i][j] += range;
			}
		}

		for (auto i = 0; i < map.size(); ++i)
		{
			for (auto j = 0; j < map[i].size(); ++j)
			{
				bool peak = true;
				bool bottom = true;
				int sum = 0;
				int amountOfTiles = 0;
				for (auto x = -1; x < 2; ++x)
				{
					for (auto y = -1; y < 2; ++y)
					{
						if ((x == 0 && y == 0) ||
							x + i < 0 || x + i >= map.size() ||
							y + j < 0 || y + j >= map[i].size())
						{
							continue;
						}

						if (map[i][j] < map[x + i][y + j])
						{
							peak = false;
						}
						if (map[i][j] > map[x + i][y + j])
						{
							bottom = false;
						}

						sum += map[x + i][y + j];
						++amountOfTiles;
					}
				}

				if (peak || bottom)
				{
					map[i][j] = sum / amountOfTiles;
				}
			}
		}

		return map;
	}
}