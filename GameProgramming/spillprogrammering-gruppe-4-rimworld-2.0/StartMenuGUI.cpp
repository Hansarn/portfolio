#include "StartMenuGUI.h"

void StartMenuGUI::setUp(EventQueue<GUIEvent>* eventQueue)
{
	window = sfg::Window::Create();
	window->SetStyle(window->GetStyle() ^ sfg::Window::TITLEBAR ^ sfg::Window::RESIZE);

	window->SetAllocation(sf::FloatRect(700, 200, 200, 100));
	window->SetRequisition(sf::Vector2f(200, 100));
	auto mainBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	window->Add(mainBox);

	auto firstBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	mainBox->Pack(firstBox);
	auto newGame = sfg::Button::Create("New Game");
	newGame->SetRequisition(sf::Vector2f(0, buttonSize));
	firstBox->Pack(newGame, false, false);

	auto loadGame = sfg::Button::Create("Load game");
	loadGame->SetRequisition(sf::Vector2f(0, buttonSize));

	loadGame->GetSignal(sfg::Widget::OnLeftClick).Connect([eventQueue] {
		//GUIEvent event;
		//event.type = GUIEvent::LOAD;
		//event.load.slot = 0;
		//eventQueue->pushEvent(event);
	});

	firstBox->Pack(loadGame, false, false);
	auto options = sfg::Button::Create("Options");
	options->SetRequisition(sf::Vector2f(0, buttonSize));
	firstBox->Pack(options, false, false);
	auto quit = sfg::Button::Create("Quit");
	quit->SetRequisition(sf::Vector2f(0, buttonSize));
	firstBox->Pack(quit, false, false);

	auto newGameBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	mainBox->Pack(newGameBox);
	newGame->GetSignal(sfg::Widget::OnLeftClick).Connect([firstBox, newGameBox] {
		firstBox->Show(false); newGameBox->Show(true);
	});

	auto combo = sfg::ComboBox::Create();
	combo->AppendItem("Easy");
	combo->AppendItem("Medium");
	combo->AppendItem("Hard");
	newGameBox->Pack(combo, false, false);
	auto check = sfg::CheckButton::Create("HardCore");
	newGameBox->Pack(check, false, false);
	auto check2 = sfg::CheckButton::Create("AutoSave");
	newGameBox->Pack(check2, false, false);

	auto start = sfg::Button::Create("Start");
	start->SetRequisition(sf::Vector2f(0, buttonSize));
	newGameBox->Pack(start, false, false);

	auto back = sfg::Button::Create("Back");
	newGameBox->Pack(back, false, false);
	back->SetRequisition(sf::Vector2f(0, buttonSize));
	back->GetSignal(sfg::Widget::OnLeftClick).Connect([firstBox, newGameBox] {
		firstBox->Show(true); newGameBox->Show(false);
	});

	window->Show(false);
	newGameBox->Show(false);
}

void StartMenuGUI::activateStartMenuGUI(bool active)
{
	window->Show(active);
}

void StartMenuGUI::AddToDesktop(sfg::Desktop * desktop)
{
	desktop->Add(window);
}