package com.example.cbk12_000.mobileproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

/**
 * a custom listener that will get information from a database and put it in to a list view
 * Created by cbk12 on 28.04.2018.
 */

class CustomListener implements ValueEventListener {

    private ListView listView;
    private Context context;
    private ArrayList<Database.Score> scores;
    private ArrayList<Integer> places;

    private boolean ownScore;
    private String userName;


    /**
     * constructor for the listener
     * @param listView the list view you want to fill
     * @param context the context
     * @param ownScore if its your own score or the top 10 list
     * @param name name of user NOT required for top 10
     */
    CustomListener(ListView listView, Context context, boolean ownScore, String name )
    {
        this.listView = listView;
        scores = new ArrayList<>();
        places = new ArrayList<>();
        this.context = context;
        this.ownScore = ownScore;
        userName = name;
    }

    /**
     * retrives information from the database
     * @param dataSnapshot
     */
    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        int place = 0;
        for(DataSnapshot dsp : dataSnapshot.getChildren())
        {
            Database.Score score = dsp.getValue(Database.Score.class);
            //if is not you won socre it will just collect all the results.
            if(!ownScore )
            {
                scores.add(score);
                places.add(place);
            }
            // if its your own score it will collect all scores with your name
            else{
                if(score.name.equals(userName))
                {
                    scores.add(score);
                    places.add(place);
                }
            }
            place++;
        }
        // to get the place people have
        for (int i = 0; i < places.size(); i++)
        {
            places.set(i, place - places.get(i));
        }
        //reverses both since they come from in reversed order
        Collections.reverse(places);
        Collections.reverse(scores);
        setUpListView();
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    /**
     * sets up the list view
     */
    private void setUpListView( )
    {
        listView.setAdapter(new BaseAdapter() {

            @Override
            public int getCount() {
                // if its ownscore we only want you to have one
                if(ownScore)
                {
                    if(scores.size() > 0) {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if(scores.size() > 10)
                {
                    return 10;
                }
                else
                {
                    return scores.size();
                }
            }

            @Override
            public Object getItem(int i) {
                return scores.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override

            public View getView(int i, View view, ViewGroup viewGroup) {
                // inflate the layout for each list row
                if (view == null) {
                    view = LayoutInflater.from(context).
                            inflate(R.layout.highscore, viewGroup, false);
                }

                TextView textViewName = view.findViewById(R.id.highscorename);
                TextView textViewScore = view.findViewById(R.id.highscorescore);
                TextView textViewNumber = view.findViewById(R.id.number);

                textViewName.setText(context.getString(R.string.by)+ " " + scores.get(i).name);
                textViewScore.setText(context.getString(R.string.score)+ " " + String.valueOf(scores.get(i).score));
                textViewNumber.setText(String.valueOf(places.get(i)));
                return view;
            }
        });
    }
}
