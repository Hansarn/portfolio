package com.example.cbk12_000.mobileproject;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * A singleton Database class called using the getInstance function.
 * The database connects anonymously to the server.
 * Created by cbk12 on 28.04.2018.
 */

public class Database {

    private static Database instance;
    private FirebaseAuth auth;
    private FirebaseDatabase db;

    private boolean loggedIn;

    private Database()
    {
        setup();
        loggedIn = false;
    }

    /**
     * Gives you the only instance of the class
     * REMEMBER: call the login function first
     * @return a Database instance
     */
    public static synchronized Database getInstance()
    {
        if(instance == null)
        {
            instance = new Database();
        }
        return instance;
    }

    /**
     * get the FirebaseAuth and FirebaseDatabase instances.
     */
    private void setup()
    {
        if(auth == null) {
            auth = FirebaseAuth.getInstance();
        }
        if(db ==null) {
            db = FirebaseDatabase.getInstance();
        }
    }

    /**
     * logs the user in to the Firebase anonymously
     * @param object is a GameMenu instance.
     */
    public void login(Object object)
    {
        //setup();
        auth.signOut();
        FirebaseUser firebaseUser = auth.getCurrentUser();
        if(firebaseUser == null)
        {
            auth.signInAnonymously().addOnCompleteListener((GameMenu)object, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful())
                    {
                        loggedIn = true;
                        System.out.println("logged in");
                    }
                    else
                    {
                        System.out.println("NOT LOGGED ON");
                        System.out.println(task.getException());
                    }
                }
            });
        }
        else{
            loggedIn = true;
            System.out.println("ALREADY LOGGED ON!");
        }
    }

    /**
     * Adds a score to the high score list.
     * @param level The name of the level
     * @param name The name of the user
     * @param score The score of the user
     */
    public void addScore(String level, String name, int score)
    {
        if(loggedIn) {
            DatabaseReference myRef = db.getReference(level);
            Score scoreClass = new Score(name, score);
            System.out.println("DatabaseReference OK");
            myRef.push().setPriority(scoreClass.score);
            myRef.push().setValue(scoreClass);
            System.out.println("Information sent");
        }
        else
        {
            System.out.println("not logged in");
        }
    }

    /**
     * Retrieves the top 10 scores of a level from the database and inserts it in to a listView
     * @param level The name of the
     * @param listView the list view you want to fill
     * @param context the context
     */
    public void getScores(String level, ListView listView, Context context)
    {
        System.out.println("BEFORE REFERENCE");
        DatabaseReference reference = db.getReference(level);
        reference.orderByChild("score").limitToLast(10).addListenerForSingleValueEvent(new CustomListener(listView, context, false, ""));
        System.out.println("AFTER REFERENCE");

    }

    /**
     * Retrieves the players best score in a level from the database and inserts it in to a listView
     * @param level The name of the level.
     * @param name The name of the player
     * @param listView The list view you want to fill
     * @param context the context
     * @return
     */
    public Score[] getScoresForUser(String level, String name, ListView listView, Context context)
    {

        DatabaseReference reference = db.getReference(level);
        reference.orderByChild("score").addValueEventListener(new CustomListener(listView, context, true, name));


        return null;
    }

    /**
     * a simple class to keep score information.
     */
    public static class Score
    {
        public String name;
        public int score;
        public Score(String name, int score)
        {
            this.name = name;
            this.score = score;
        }
        public Score()
        {

        }

    }

    /**
     * Signs out of the Database
     */
    public void onDestroy()
    {
        auth.signOut();
    }

}
