package com.example.cbk12_000.mobileproject;

import android.content.Context;

import java.util.ArrayList;

public class LevelList {
    private ArrayList<Level> levels;
    private static LevelList instance;
    private LevelList(){}

    /**
     * Gets the instance of the level list (LevelList is a singleton)
     * @return the instance of the list
     */
    public static synchronized LevelList getInstance()
    {
        if(instance == null)
        {
            instance = new LevelList();
        }
        return instance;
    }

    /**
     * Initializes the level list by loading levels from files
     * @param context context is needed for inputstream
     */
    public void init(Context context)
    {
        LevelLoader loader = new LevelLoader(context);
        levels = loader.getLevels();
    }

    /**
     * Gets the requested level
     * @param i index for the level
     * @return the level
     */
    public Level getLevel(int i)
    {
        return levels.get(i);
    }

    /**
     * Gets a list of all the level names
     * @return array of level names
     */
    public String[] getNames()
    {
        String[] names = new String[levels.size()];
        for (int i = 0; i < levels.size(); i++)
        {
            names[i] = levels.get(i).getName();
        }
        return names;
    }
}