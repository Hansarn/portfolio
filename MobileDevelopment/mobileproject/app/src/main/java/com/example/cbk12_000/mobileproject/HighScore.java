package com.example.cbk12_000.mobileproject;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class HighScore extends AppCompatActivity {

    /**
     * starts the high score activty and finds the right high score for the level
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);
        String levelName = getIntent().getStringExtra("level");

        TextView textView = findViewById(R.id.textView3);

        textView.setText(getString(R.string.highscorefor)+ " " + levelName);

        ListView highScore = findViewById(R.id.highscorelist);
        ListView ownScore = findViewById(R.id.userhighscore);

        Database.getInstance().getScores(levelName, highScore, this);
        SharedPreferences sharedPreferences = getSharedPreferences("Name", MODE_PRIVATE);

        Database.getInstance().getScoresForUser(levelName,sharedPreferences.getString("name", "anon"), ownScore, this);
    }









}
