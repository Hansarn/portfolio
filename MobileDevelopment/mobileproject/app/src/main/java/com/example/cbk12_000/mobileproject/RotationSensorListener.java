package com.example.cbk12_000.mobileproject;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.renderscript.Matrix4f;

import com.example.cbk12_000.mobileproject.Vector2D.Vector2d;

/**
 * Listener for the RotationVector sensor
 */
class RotationSensorListener implements SensorEventListener {

    private Matrix4f rotationMatrix;

    private Vector2d phoneOrientation;

    private GameView view;

    /**
     * Constructor
     * @param view The game view for the physics updates
     */
    RotationSensorListener(GameView view)
    {
        super();

        rotationMatrix = new Matrix4f();
        phoneOrientation = new Vector2d(0.0, 0.0);
        this.view = view;
    }


    /**
     * Takes sensor events. Used to figure out rotations and pass it to the game view
     * @param event The rotation event
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() != Sensor.TYPE_ROTATION_VECTOR)
        {
            return;
        }

        SensorManager.getRotationMatrixFromVector(rotationMatrix.getArray(), event.values);

        float[] orientation = new float[3];

        SensorManager.getOrientation(rotationMatrix.getArray(), orientation);

        double multiplier = 50.0;
        phoneOrientation.x = -Math.toDegrees(orientation[1]) * multiplier;
        phoneOrientation.y = -Math.toDegrees(orientation[2]) * multiplier;

        view.update(phoneOrientation);
    }

    /**
     * Called when the accuracy of the sensor changes. Not used
     * @param sensor The sensor
     * @param accuracy The new accuracy
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
