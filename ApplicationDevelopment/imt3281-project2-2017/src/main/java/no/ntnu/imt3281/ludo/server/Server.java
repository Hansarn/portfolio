package no.ntnu.imt3281.ludo.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.gui.ServerMenuController;
import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.PlayerEvent;

/**
 * This is the main class for the server.
 *
 */
public class Server extends Application {

	/** The server socket. */
	private ServerSocket socket;

	/** The client connections. */
	private ArrayList<ClientConnection> connections;

	/** The LFG list. */
	private ArrayList<String> lookingForGame;

	/** Online users. */
	private ArrayList<String> userNames;

	/** The executor service for threads. */
	private ExecutorService executorService;

	/** The shutdown boolean. */
	private boolean shutdown = false;

	/** The control for the server gui. */
	private ServerMenuController control;

	/** The room list. */
	private ArrayList<String> roomList;

	/** The database. */
	private Database db;

	/** The time when there were enough players in lfg to start a game. */
	private Long possibleToCreateGameTime;

	/** The current game id. */
	private int currentGameId = 0;

	/** The list of games. */
	private ArrayList<Game> games;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(Server.class.getName());


	/**
	 * Instantiates a new server.
	 */
	public Server() {
		try {
			Platform.setImplicitExit(true);
			roomList = new ArrayList<String>();
			userNames = new ArrayList<String>();
			lookingForGame = new ArrayList<String>();
			games = new ArrayList<Game>();

			db = new Database();
			db.connectToDatabase();

			socket = new ServerSocket(7591);
			connections = new ArrayList<ClientConnection>();

			executorService = Executors.newCachedThreadPool();
			loginService();
			setupGame();
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}
	
	
	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Starts a new thread for connections.
	 */
	public void loginService() {
		executorService.execute(() -> {
			while (!shutdown) {
				try {

					Socket s = socket.accept();
					ClientConnection client = new ClientConnection(s);

					synchronized (connections) {
						connections.add(client);
					}
					waitForMessage(client);

				} catch (SocketException e) {
					LOGGER.log(Level.WARNING, e.getMessage(), e);
				} catch (IOException e) {
					LOGGER.log(Level.WARNING, e.getMessage(), e);
				}
			}
		});

	}

	/**
	 * Starts a new thread for client-server communication. One thread per client
	 *
	 * @param client
	 *            The client
	 */
	public void waitForMessage(ClientConnection client){
		executorService.execute(() -> {
			while (!shutdown) {
				try {
					String message;
					message = client.input.readLine();
					if (message == null) {
						for (int i = 0; i < games.size(); ++i) {
							games.get(i).nukePlayer(client.name);

						}
						for (int i = 0; i < userNames.size(); ++i) {
							if (userNames.get(i).equals(client.name)) {
								userNames.remove(i);
							}
						}
						for (int i = 0; i < connections.size(); ++i) {
							if (connections.get(i).equals(client)) {
								connections.remove(i);
								Platform.runLater(new Runnable() {
									@Override
									public void run() {
										control.removeUser(client.name);
									}
								});
							}
						}
						break;

					}

					if (message.startsWith("Login:")) {
						login(message, client);
					} else if (message.startsWith("Register:")) {
						register(message, client);
					} else if (message.startsWith("Chat:")) {
						message = message.substring(5);
						handleChat(message, client);
					} else if (message.startsWith("Chatroom:")) {
						message = message.substring(9);
						chatroom(message, client);
					} else if (message.startsWith("RoomList:")) {
						StringBuilder temp = new StringBuilder("RoomList:");
						for (int i = 0; i < roomList.size(); i++) {
							temp.append(roomList.get(i));
							temp.append(":");
						}
						client.sendText(temp.toString());
					} else if (message.startsWith("LFG:")) {
						synchronized (lookingForGame) {
							if (!lookingForGame.contains(client.name)) {
								lookingForGame.add(client.name);
								if (lookingForGame.size() == 2) {
									possibleToCreateGameTime = System.nanoTime() / 1000000000;
								}
							}
						}
					} else if (message.startsWith("DiceThrow:")) {
						message = message.substring(10);

						int gameId = Integer.parseInt(message);
						Game game = null;

						for (int i = 0; i < games.size(); ++i) {
							if (games.get(i).gameId == gameId) {
								game = games.get(i);
								break;
							}
						}

						if (game != null) {
							game.throwDice();
						}
					} else if (message.startsWith("GameChat:")) {
						message = message.substring(9);
						int index = message.indexOf(":");
						String name = message.substring(0, index);
						int gameId = Integer.parseInt(name);
						for (int i = 0; i < games.size(); ++i) {
							if (games.get(i).gameId == gameId) {
								games.get(i).sendGameMessage(message, client);
								break;
							}
						}
					} else if (message.startsWith("GameMove:")) {
						message = message.substring(9);
						int gameId = Integer.parseInt(message);
						int oldPos = Integer.parseInt(client.input.readLine());
						int newPos = Integer.parseInt(client.input.readLine());
						for (int i = 0; i < games.size(); ++i) {
							if (games.get(i).gameId == gameId) {
								games.get(i).movePiece(oldPos, newPos, client);
								break;
							}
						}
					} else if (message.startsWith("ChallengePlayer:")) {
						StringBuilder userList = new StringBuilder();
						userList.append("Challengers:");

						for (int i = 0; i < userNames.size(); ++i) {
							if (!userNames.get(i).equals(client.name)) {
								userList.append(userNames.get(i));
								userList.append(":");
							}
						}

						client.sendText(userList.toString());
					} else if (message.startsWith("ChallengeThesePeeps:")) {
						message = message.substring(20);

						String[] users = message.split(":");
						ClientConnection[] cons = new ClientConnection[4];
						int helper = 0;

						for (int i = 0; i < connections.size(); ++i) {
							for (int j = 0; j < users.length; ++j) {
								if (connections.get(i).getName().equals(users[j])) {
									cons[helper++] = connections.get(i);
								}
							}
						}
						cons[helper++] = client;

						games.add(new Game(currentGameId++, users.length + 1, cons));
					}
				} catch (IOException e) {
					LOGGER.log(Level.WARNING, e.getMessage(), e);
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(Server.class.getResource("../gui/ServerMenu.fxml"));
		loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));

		Scene scene = new Scene((Pane) loader.load());
		primaryStage.setScene(scene);
		primaryStage.show();
		control = loader.<ServerMenuController>getController();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.application.Application#stop()
	 */
	@Override
	public void stop() {
		shutdown = true;
		try {
			for (int i = 0; i < connections.size(); i++) {
				connections.get(i).close();
			}
			socket.close();
		} catch (SocketException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
		executorService.shutdown();
	}

	/**
	 * Login a client.
	 *
	 * @param message
	 *            The message from client
	 * @param client
	 *            The client
	 */
	private void login(String message, ClientConnection client) {
		message = message.substring(6);

		String pass;
		try {
			pass = client.input.readLine();
			boolean login = true;
			for (int i = 0; i < userNames.size(); i++) {
				if (userNames.get(i).equals(message)) {
					login = false;
				}
			}

			if (db.login(message, pass) && login) {
				client.sendText("Login successfull");
				client.setName(message);
				final String name = message;
				userNames.add(name);
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						control.addToList(name);
					}
				});
				client.sendText("TopWon:" + db.getScoreList(true));
				client.sendText("TopPlayed:" + db.getScoreList(false));
			} else {
				client.sendText("LogMessage:Wrong");
			}

		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	/**
	 * Register a new user.
	 *
	 * @param message
	 *            The message from client
	 * @param client
	 *            The client
	 */
	private void register(String message, ClientConnection client) {

		message = message.substring(9);
		String pass;
		try {
			pass = client.input.readLine();

			if (db.userExist(message)) {
				client.sendText("LogMessage:Taken");
			} else {
				client.sendText("LogMessage:Added");
				db.addUser(message, pass);
			}
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	/**
	 * Handle chat messages.
	 *
	 * @param message
	 *            The message from client
	 * @param client
	 *            The client
	 */
	private void handleChat(String message, ClientConnection client) {
		int index = message.indexOf(":");
		String name = message.substring(0, index);
		message = message.substring(index + 1);

		db.saveComment(client.name, message, name);
		String chatMessage = "Chat:" + name + ":" + client.name + ": " + message + "\n";

		for (int i = 0; i < connections.size(); i++) {
			if (connections.get(i).channels.contains(name)) {
				try {
					connections.get(i).sendText(chatMessage);
				} catch (IOException e) {
					LOGGER.log(Level.WARNING, e.getMessage(), e);
				}
			}
		}

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				control.addChat(chatMessage);
			}
		});
	}

	/**
	 * Creates a new chatroom, if it already exist, just add user to the existing
	 * one
	 *
	 * @param room
	 *            The room
	 * @param client
	 *            The client
	 */
	public void chatroom(String room, ClientConnection client) {
		if (!client.channels.contains(room)) {
			client.channels.add(room);
		}
		if (!roomList.contains(room)) {
			roomList.add(room);
		}

	}

	/**
	 * Setup a new game.
	 */
	public void setupGame() {
		executorService.execute(() -> {
			Long timeSinceWeCouldStartAGameButDidnt = null;
			while (!shutdown) {
				if (possibleToCreateGameTime != null) {
					timeSinceWeCouldStartAGameButDidnt = System.nanoTime() / 1000000000
							- possibleToCreateGameTime.longValue();
				}
				synchronized (lookingForGame) {
					if (lookingForGame.size() >= 4) {
						startGame();
						possibleToCreateGameTime = null;
						timeSinceWeCouldStartAGameButDidnt = null;
					}
					if (timeSinceWeCouldStartAGameButDidnt != null && timeSinceWeCouldStartAGameButDidnt > 5) {
						startGame();
						possibleToCreateGameTime = null;
						timeSinceWeCouldStartAGameButDidnt = null;
					}
				}
			}
		});
	}

	/**
	 * Start a game.
	 */
	private void startGame() {
		synchronized (lookingForGame) {
			synchronized (connections) {
				ClientConnection[] playerConnectionsForGame = new ClientConnection[] { null, null, null, null };

				for (int i = 0; i < 4 && i < lookingForGame.size(); ++i) {
					for (int j = 0; j < connections.size(); ++j) {
						if (connections.get(j).name.equals(lookingForGame.get(i))) {
							playerConnectionsForGame[i] = connections.get(j);
							break;
						}
					}

					if (playerConnectionsForGame[i] == null) {
						lookingForGame.remove(lookingForGame.get(i));
						--i;
					}
				}

				if (lookingForGame.size() > 1) {
					synchronized (games) {
						games.add(new Game(currentGameId++, Math.min(lookingForGame.size(), 4),
								playerConnectionsForGame));
					}
					synchronized (lookingForGame) {
						for (int i = 0; i < playerConnectionsForGame.length; ++i) {
							if (playerConnectionsForGame[i] != null
									&& lookingForGame.contains(playerConnectionsForGame[i].name)) {
								lookingForGame.remove(playerConnectionsForGame[i].name);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Helper class to manage games.
	 */
	private class Game {

		/** The game id. */
		private int gameId;

		/** The number of players. */
		private int numberOfPlayers;

		/** The players. */
		private ClientConnection[] players;

		/** The game. */
		private Ludo game;

		/** The last dice throw. */
		private int lastDiceThrow = 0;

		/**
		 * Instantiates a new game.
		 *
		 * @param gameId
		 *            the game id
		 * @param numberOfPlayers
		 *            the number of players
		 * @param players
		 *            the players
		 */
		public Game(int gameId, int numberOfPlayers, ClientConnection[] players) {
			this.gameId = gameId;
			this.numberOfPlayers = numberOfPlayers;
			this.players = players;
			game = new Ludo();
			StringBuilder playerList = new StringBuilder("");
			for (int j = 0; j < numberOfPlayers; ++j) {
				playerList.append(players[j].getName() + "\n");
				game.addPlayer(players[j].getName());
			}

			sendToAllPlayers("NewGame:" + gameId + "\n" + numberOfPlayers + "\n" + playerList.toString());

			game.addPlayerListener(pe -> Platform.runLater(() -> {

				if (pe.getState() == PlayerEvent.WON) {
					sendToAllPlayers("Won:" + gameId);
					for (int i = 0; i < numberOfPlayers; ++i) {
						db.playedGame(i == pe.getActivePlayer(), players[i].getName());
					}
					for (int i = 0; i < connections.size(); ++i) {
						
						try {
							connections.get(i).sendText("TopWon:" + db.getScoreList(true));
							connections.get(i).sendText("TopPlayed:" + db.getScoreList(false));
							connections.get(i).sendText("Chat:Global Chat:SYSTEM: Player "
									+ game.getPlayerName(pe.getActivePlayer()) + " just won a game!");
						} catch (IOException e) {
							LOGGER.log(Level.WARNING, e.getMessage(), e);
						}
					}

				}
			}));

		}

		/**
		 * Throw a dice.
		 */
		public void throwDice() {
			Random randomGen = new Random(System.currentTimeMillis());
			lastDiceThrow = randomGen.nextInt(6) + 1;
			game.throwDice(lastDiceThrow);
			sendToAllPlayers("DiceThrow:" + gameId + "\n" + lastDiceThrow);
		}

		/**
		 * Move a piece.
		 *
		 * @param oldPos
		 *            The old position
		 * @param newPos
		 *            The new position
		 * @param client
		 *            The client
		 */
		public void movePiece(int oldPos, int newPos, ClientConnection client) {
			if (game.getPlayerName(game.activePlayer()).equals(client.getName())) {
				int active = game.activePlayer();
				if (game.movePiece(game.activePlayer(), oldPos, newPos)) {
					sendToAllPlayers("PieceMoved:" + gameId + "\n" + active + "\n" + oldPos + "\n" + newPos);
				}
			}
		}

		/**
		 * Send a message to all players.
		 *
		 * @param message
		 *            The message
		 */
		private void sendToAllPlayers(String message) {
			for (int i = 0; i < numberOfPlayers; ++i) {
				if (game.getActivePlayers()[i]) {
					try {
						players[i].sendText(message);
					} catch (IOException e) {
						LOGGER.log(Level.WARNING, e.getMessage(), e);
					}
				}
			}
		}

		/**
		 * Send a game message.
		 *
		 * @param message
		 *            The message
		 * @param client
		 *            The client
		 */
		private void sendGameMessage(String message, ClientConnection client) {
			int index = message.indexOf(":");
			String name = message.substring(0, index);
			message = message.substring(index + 1);

			db.saveComment(client.name, message, name);
			String chatMessage = "GameChat:" + gameId + ":" + client.name + ": " + message;

			sendToAllPlayers(chatMessage);

			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					control.addChat(chatMessage + "\n");
				}
			});
		}

		/**
		 * Nuke a player from the game.
		 *
		 * @param name
		 *            The name
		 */
		public void nukePlayer(String name) {
			for (int i = 0; i < numberOfPlayers; ++i) {
				if (!players[i].name.equals(name)) {
					game.removePlayer(name);
					sendToAllPlayers("Leaver:" + gameId + "\n" + name);
					sendToAllPlayers("GameChat:" + gameId + ":" + "SYSTEM: Player " + name + " has left the game!");
				}
			}
		}
	}

	/**
	 * Helper class to deal with client connections.
	 */
	public static class ClientConnection {
		/** The client socket. */
		private Socket socket;

		/** The client name. */
		private String name;

		/** The input stream. */
		private BufferedReader input;

		/** The output stream. */
		private BufferedWriter output;

		/** The chat channels the client is part of. */
		private ArrayList<String> channels;

		/**
		 * Instantiates a new client connection.
		 *
		 * @param connection
		 *            The connection
		 * @throws IOException
		 *             Signals that an I/O exception has occurred.
		 */
		public ClientConnection(Socket connection) throws IOException {
			socket = connection;
			input = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_16));

			output = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), StandardCharsets.UTF_16));
			setName("");
			channels = new ArrayList<String>();
			channels.add("Global Chat");
		}

		/**
		 * Close the connection.
		 *
		 * @throws IOException
		 *             Signals that an I/O exception has occurred.
		 */
		public void close() throws IOException {
			output.close();

			socket.close();
		}

		/**
		 * Send text to client.
		 *
		 * @param text
		 *            The text
		 * @throws IOException
		 *             Signals that an I/O exception has occurred.
		 */
		public void sendText(String text) throws IOException {
			output.write(text);
			output.newLine();
			output.flush();
		}

		/**
		 * Read from input.
		 *
		 * @return The string
		 * @throws IOException
		 *             Signals that an I/O exception has occurred.
		 */
		public String read() throws IOException {
			if (input.ready()) {
				return input.readLine();
			}
			return null;
		}

		/**
		 * Gets the name of the client.
		 *
		 * @return The name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Sets the name of the client.
		 *
		 * @param name
		 *            The new name
		 */
		public void setName(String name) {
			this.name = name;
		}
	}
}
