package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * The listener interface for receiving piece events. The class that is
 * interested in processing a piece event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addPieceListener</code> method. When the piece event
 * occurs, that object's appropriate method is invoked.
 *
 * @see PieceEvent
 */
public interface PieceListener extends EventListener {

	/**
	 * Piece moved.
	 *
	 * @param pieceEvent
	 *            a new pieceEvent
	 */
	void pieceMoved(PieceEvent pieceEvent);

}
