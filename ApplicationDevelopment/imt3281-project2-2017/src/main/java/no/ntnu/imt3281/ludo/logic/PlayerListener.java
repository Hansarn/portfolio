package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * The listener interface for receiving player events. The class that is
 * interested in processing a player event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addPlayerListener</code> method. When the player event
 * occurs, that object's appropriate method is invoked.
 *
 * @see PlayerEvent
 */
public interface PlayerListener extends EventListener {

	/**
	 * Player state changed.
	 *
	 * @param event
	 *            a new player event
	 */
	void playerStateChanged(PlayerEvent event);

}
