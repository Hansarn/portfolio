package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import no.ntnu.imt3281.ludo.client.Client;

/**
 * Controller of the login screen GUI
 */
public class LoginScreenController {
	/** The login button. */
	@FXML
	private Button loginButton;

	/** The register button. */
	@FXML
	private Button registerButton;

	/** The close button. */
	@FXML
	private MenuItem closeButton;

	/** The about button. */
	@FXML
	private MenuItem aboutButton;

	/** The username field. */
	@FXML
	private TextField username;

	/** The password field. */
	@FXML
	private PasswordField password;

	/** The file dropdown menu. */
	@FXML
	private Menu fileDropdown;

	/** The help dropdown menu. */
	@FXML
	private Menu helpDropdown;

	/** The username label. */
	@FXML
	private Label usernameLabel;

	/** The password label. */
	@FXML
	private Label passwordLabel;

	/** The error message. */
	@FXML
	private Label errorMessage;

	/** To tell if the password in the password field is already encrypted. */
	private boolean passwordEncrypted = true;

	/** The client. */
	private Client client;

	/** The logger */
	private static final Logger LOGGER = Logger.getLogger(LoginScreenController.class.getName());
	
	/**
	 * Login.
	 *
	 * @param e
	 *            The ActionEvent, not used
	 */
	@FXML
	public void login(ActionEvent e) {
		client.connect();
		try {
			String encryptedPassword = password.getText();
			if (!passwordEncrypted) {
				encryptedPassword = Client.encrypt(encryptedPassword);
			}
			client.setName(username.getText());
			client.sendText("Login:" + username.getText() + "\n" + encryptedPassword);
			client.openFile(username.getText(), encryptedPassword);
		} catch (IOException ex) {
			LOGGER.log(Level.WARNING, ex.getMessage(), ex);
		}
	}

	/**
	 * Register client.
	 *
	 * @param client
	 *            The client
	 */
	public void registerClient(Client client) {
		this.client = client;
	}

	/**
	 * Registering a new user.
	 *
	 * @param e
	 *            The ActionEvent, not used
	 */
	@FXML
	public void register(ActionEvent e) {
		client.connect();
		try {
			if(Pattern.matches("^[a-zA-Z0-9]{3,40}+$", username.getText()) && Pattern.matches("\\S{3,40}", password.getText()))
			{				
				client.sendText("Register:" + username.getText() + "\n" + Client.encrypt(password.getText()));
			}
			else
			{
				setLogMessage("Invalid input");
			}
		} catch (IOException ex) {
			LOGGER.log(Level.WARNING, ex.getMessage(), ex);
		}
	}

	/**
	 * Sets the username and password.
	 *
	 * @param userName
	 *            The user name
	 * @param password
	 *            The password
	 */
	public void setUsernamePassword(String userName, String password) {
		this.username.setText(userName);
		this.password.setText(password);
	}

	/**
	 * Password changed, called when something is typed in the password field
	 */
	@FXML
	public void passwordChanged() {
		passwordEncrypted = false;
	}

	/**
	 * Sets the error messages.
	 *
	 * @param message
	 *            The error type
	 */
	public void setLogMessage(String message) {
		switch (message) {
		case "Wrong":
			errorMessage.setText("Wrong username or password");
			errorMessage.setTextFill(Color.RED);
			break;
		case "Taken":
			errorMessage.setText("Username already taken");
			errorMessage.setTextFill(Color.RED);
			break;
		case "Added":
			errorMessage.setText("Registration successful! You may now log in");
			errorMessage.setTextFill(Color.BLACK);
			break;
		case "ConnectionError:":
			errorMessage.setText("Could not connect to the server");
			errorMessage.setTextFill(Color.RED);
			break;
		case "Invalid input":
			errorMessage.setText("Username or password is not valid, username must be letters or numbers between 3 and 40 characters."
						+  "Password must be non-whitespace characters between 3 and 40 characters long");
			errorMessage.setTextFill(Color.RED);
			break;
		default:
			errorMessage.setText("Something went horribly wrong");
			errorMessage.setTextFill(Color.RED);
			break;
		}
	}
}
