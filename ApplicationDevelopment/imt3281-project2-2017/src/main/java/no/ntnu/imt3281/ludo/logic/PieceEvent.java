package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.Objects;

/**
 * The Class PieceEvent.
 */
public class PieceEvent extends EventObject implements PieceListener {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6016948672238563006L;

	/** The player. */
	private int player;

	/** The piece. */
	private int piece;

	/** The start. */
	private int start;

	/** The end. */
	private int end;

	/**
	 * Instantiates a new piece event.
	 *
	 * @param ludo
	 *            The ludo instance associated with this event
	 * @param player
	 *            the playerID
	 * @param piece
	 *            the pieceID
	 * @param start
	 *            the start position of the piece
	 * @param end
	 *            the end position of the piece
	 */
	public PieceEvent(Ludo ludo, int player, int piece, int start, int end) {

		super(ludo);
		this.player = player;
		this.piece = piece;
		this.start = start;
		this.end = end;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PieceEvent)) {
			return false;
		}
		PieceEvent event = (PieceEvent) obj;
		return player == event.player && piece == event.piece && start == event.start && end == event.end
				&& source == event.source;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(player, piece, start, end, super.getSource());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * no.ntnu.imt3281.ludo.logic.PieceListener#pieceMoved(no.ntnu.imt3281.ludo.
	 * logic.PieceEvent)
	 */
	@Override
	public void pieceMoved(PieceEvent pieceEvent) {
		player = pieceEvent.player;
		piece = pieceEvent.piece;
		start = pieceEvent.start;
		end = pieceEvent.end;
	}

	/**
	 * Gets the player.
	 *
	 * @return the player
	 */
	public int getPlayer() {
		return player;
	}

	/**
	 * Sets the player.
	 *
	 * @param player
	 *            the new player
	 */
	public void setPlayer(int player) {
		this.player = player;
	}

	/**
	 * Gets the piece.
	 *
	 * @return the piece
	 */
	public int getPiece() {
		return piece;
	}

	/**
	 * Sets the piece.
	 *
	 * @param piece
	 *            the new piece
	 */
	public void setPiece(int piece) {
		this.piece = piece;
	}

	/**
	 * Gets the start.
	 *
	 * @return the start
	 */
	public int getStart() {
		return start;
	}

	/**
	 * Sets the start.
	 *
	 * @param start
	 *            the new start
	 */
	public void setStart(int start) {
		this.start = start;
	}

	/**
	 * Gets the end.
	 *
	 * @return the end
	 */
	public int getEnd() {
		return end;
	}

	/**
	 * Sets the end.
	 *
	 * @param end
	 *            the new end
	 */
	public void setEnd(int end) {
		this.end = end;
	}

}
