package no.ntnu.imt3281.ludo.server;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Deals with the connection to the database and transfer of data
 */
public class Database {

	/** The connection to the database */
	private Connection con;

	/** The Constant JDBC_DRIVER. */
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

	/** The Constant dbUrl. */
	static final String DB_URL = "jdbc:mysql://mysql.stud.ntnu.no/hanseei_javaprosjekt2";

	/** The Constant userName. */
	static final String USER_NAME = "hanseei_java";

	/** The Constant LOGGER class for logging exceptions. */
	private static final Logger LOGGER = Logger.getLogger(Database.class.getName());

	/**
	 * Connect to database.
	 *
	 * @return true, if successful
	 */
	public boolean connectToDatabase() {

		try {
			Class.forName(JDBC_DRIVER);

			Scanner inputString;
			String password = "";

			inputString = new Scanner(new File("./topSecretDatabasePasswordDontLook.txt"), "UTF-8");

			if (inputString.hasNext()) {

				password = inputString.next();
			}

			inputString.close();

			con = DriverManager.getConnection(DB_URL, USER_NAME, password);
			return true;
		} catch (SQLException | ClassNotFoundException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
		return false;
	}

	/**
	 * Adds a user to the database.
	 *
	 * @param name
	 *            The name of the user
	 * @param password
	 *            The encrypted password
	 */
	public void addUser(String name, String password) {
		try {
			PreparedStatement prepStmt = con.prepareStatement(
					"INSERT INTO `users`(`username`,`password`, `Played`, `Won`) VALUES (?, ?, ?, ?)");
			prepStmt.setString(1, name);
			prepStmt.setString(2, password);
			prepStmt.setInt(3, 0);
			prepStmt.setInt(4, 0);
			prepStmt.execute();
			prepStmt.close();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}

	}

	/**
	 * Checks if the user exists on the database.
	 *
	 * @param name
	 *            The username
	 * @return true, user already exists
	 */
	public boolean userExist(String name) {
		try {
			PreparedStatement prepStmt = con.prepareStatement("SELECT * FROM `users` WHERE username = ?");
			prepStmt.setString(1, name);
			ResultSet rs = prepStmt.executeQuery();
			boolean exist = rs.next();
			prepStmt.close();
			return exist;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
			return false;
		}

	}

	/**
	 * Checks is a user can login
	 *
	 * @param username
	 *            The username
	 * @param password
	 *            the password
	 * @return true, username and password is right
	 */
	public boolean login(String username, String password) {
		PreparedStatement prepStmt;
		try {
			prepStmt = con.prepareStatement("SELECT * FROM `users` WHERE username = ? AND password = ?");
			prepStmt.setString(1, username);
			prepStmt.setString(2, password);
			ResultSet rs = prepStmt.executeQuery();
			boolean loggedIn = rs.next();
			prepStmt.close();
			return loggedIn;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
			return false;
		}

	}

	/**
	 * Save comment to the database.
	 *
	 * @param username
	 *            The username
	 * @param message
	 *            The message
	 * @param channel
	 *            The channel
	 */
	public void saveComment(String username, String message, String channel) {
		PreparedStatement prepStmt;
		try {
			prepStmt = con
					.prepareStatement("INSERT INTO `userChat`(`username`, `message`, `chatChannel`) VALUES (?, ?, ?)");
			prepStmt.setString(1, username);
			prepStmt.setString(2, message);
			prepStmt.setString(3, channel);
			prepStmt.execute();
			prepStmt.close();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	/**
	 * logs the game and the score
	 *
	 * @param won
	 *            if the user won
	 * @param username
	 *            The username
	 */
	public void playedGame(boolean won, String username) {
		PreparedStatement prepStmt;
		try {
			prepStmt = con.prepareStatement("UPDATE users Set Played = Played + ?, Won = Won + ? WHERE username = ? ");
			prepStmt.setInt(1, 1);
			prepStmt.setInt(2, (won) ? 1 : 0);
			prepStmt.setString(3, username);
			prepStmt.execute();
			prepStmt.close();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	/**
	 * Takes in a boolean for whether to get the "won" or "played" list, and returns
	 * the list of the top 10 usernames in each category, along with the scores or 
	 * games played from the database.
	 * 
	 * @param won if true; returns the "won" column, else returns the "played" column
	 * @return String of users and score values
	 */
	
	public String getScoreList(boolean won) {
		PreparedStatement prepStmt;
		StringBuilder sb = new StringBuilder();
		try {
			if (won) {
				prepStmt = con.prepareStatement("SELECT username, Won FROM users ORDER BY Won DESC LIMIT 10");
			} else {
				prepStmt = con.prepareStatement("SELECT username, Played FROM users ORDER BY Played DESC LIMIT 10");
			}

			ResultSet rs = prepStmt.executeQuery();

			while (rs.next()) {
				sb.append(rs.getString(1));
				sb.append(":");
				sb.append(rs.getInt(2));
				sb.append(":");
			}
			prepStmt.close();
			return sb.toString();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
			return "failed";
		}

	}

}
