package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import no.ntnu.imt3281.ludo.client.Client;

/**
 * Controller of all Ludo GUI elements.
 */
public class LudoController {

	/** Challenger list */
	@FXML
	private ListView<Label> challengerList;
	
	@FXML
	private ListView<Label> topPlayedList;
	
	@FXML
	private ListView<Label> topWonList;

	@FXML
	private Pane challengerPane;

	/** The games tabs. */
	@FXML
	private TabPane tabbedPane;

	/** The chat tabs. */
	@FXML
	private TabPane chat;

	/** The chat box. */
	@FXML
	private TextField chatField;

	/** The client. */
	private Client client;

	/** The game controllers. */
	private ArrayList<GameToBoardController> games;

	/** The logger */
	private static final Logger LOGGER = Logger.getLogger(LudoController.class.getName());

	/**
	 * Join a chat room.
	 */
	@FXML
	public void joinChatRoom() {
		TextInputDialog input = new TextInputDialog();
		input.setTitle("Join ChatRoom");
		input.setHeaderText("Join ChatRoom");
		input.setContentText("Add room name here: ");
		Optional<String> result = input.showAndWait();
		addChatTab(result);
	}

	/**
	 * Queries for the chat list
	 */
	@FXML
	public void chatList() {
		try {
			client.sendText("RoomList:");
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	/**
	 * Join a random game.
	 *
	 * @param e
	 *            The ActionEvent, not used
	 */
	@FXML
	public void joinRandomGame(ActionEvent e) {
		try {
			client.sendText("LFG:");
		} catch (IOException e1) {
			LOGGER.log(Level.WARNING, e1.getMessage(), e1);
		}
	}
	
	/**
	 * Challenge player(s)
	 *
	 * @param e
	 *            The ActionEvent, not used
	 */
	@FXML
	public void challengePlayer(ActionEvent e) {
		try {
			client.sendText("ChallengePlayer:");
		} catch (IOException e1) {
			LOGGER.log(Level.WARNING, e1.getMessage(), e1);
		}
	}

	/**
	 * Displays the list of players to challenge
	 * 
	 * @param players The players to challenge
	 * 
	 */
	public void displayChallengerList(String[] players) {
		challengerList.getItems().clear();
		challengerPane.setVisible(true);

		for (int i = 0; i < players.length; ++i) {
			challengerList.getItems().add(new Label(players[i]));
		}
	}

	/**
	 * Challenges the currently selected players
	 */
	@FXML
	public void challengeSelectedPlayers() {
		ObservableList<Label> list = challengerList.getSelectionModel().getSelectedItems();

		if (list.size() < 4 && list.isEmpty()) {
			challengerPane.setVisible(false);
			StringBuilder usernameList = new StringBuilder();
			usernameList.append("ChallengeThesePeeps:");

			for (int i = 0; i < list.size(); ++i) {
				usernameList.append(list.get(i).getText());
				usernameList.append(":");
			}

			try {
				client.sendText(usernameList.toString());
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, e.getMessage(), e);
			}

		}
	}
	
	/**
	 * Sets up the list for top 10 most games won
	 * 
	 * @param message The message containing the list 
	 */
	public void setMostWonList(String message) {
		topWonList.getItems().clear();
		String[] userNames = message.split(":");

		for (int i = 0; i < userNames.length; i += 2) {
			
			topWonList.getItems().add(new Label(userNames[i] + "[" + userNames[i+1] + "]" ));
		}
	}
	
	/**
	 * Sets up the list for top 10 most games played
	 * 
	 * @param message The message containing the list
	 */
	public void setMostPlayedList(String message) {
		topPlayedList.getItems().clear();
		String[] userNames = message.split(":");

		for (int i = 0; i < userNames.length; i += 2) {
			
			topPlayedList.getItems().add(new Label(userNames[i] + "[" + userNames[i+1] + "]" ));
		}
	}

	/**
	 * Send message to the game chat.
	 */
	@FXML
	public void sendMessage() {
		String currentChat = chat.getSelectionModel().getSelectedItem().getText();
		try {
			client.sendText("Chat:" + currentChat + ":" + chatField.getText());
			chatField.clear();
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}

	}

	/**
	 * Display new message.
	 *
	 * @param message
	 *            The new message
	 */
	public void displayMessage(String message) {
		int index = message.indexOf(":");
		String name = message.substring(0, index);
		String chatMessage = message.substring(index + 1);

		chat.getTabs().forEach(tab -> {
			if (tab.getText().equals(name)) {
				TextArea text = (TextArea) tab.getContent();

				text.appendText(chatMessage + "\n");
			}
		});

	}

	/**
	 * Register client.
	 *
	 * @param client
	 *            The client
	 */
	public void registerClient(Client client) {
		this.client = client;
		games = new ArrayList<GameToBoardController>();
		chat.setTabClosingPolicy(TabPane.TabClosingPolicy.SELECTED_TAB);
		challengerList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
	}

	/**
	 * Adds a new chat tab.
	 *
	 * @param result
	 *            The Optional string over chats
	 */
	public void addChatTab(Optional<String> result) {

		result.ifPresent(name -> {
			Tab temp = new Tab(name);
			TextArea text = new TextArea();
			text.setEditable(false);
			text.setWrapText(true);
			temp.setContent(text);
			if (!chatExists(name)) {
				chat.getTabs().add(temp);
				try {
					client.sendText("Chatroom:" + name);
				} catch (IOException e) {
					LOGGER.log(Level.WARNING, e.getMessage(), e);
				}
			}
		});
	}

	private boolean chatExists(String name) {
		boolean contains = false;
		for (int i = 0; i < chat.getTabs().size(); i++) {
			if (chat.getTabs().get(i).getText().equals(name)) {
				contains = true;
			}
		}
		return contains;
	}

	/**
	 * Adds a new game tab.
	 *
	 * @param id
	 *            The id
	 * @param nrOfPlayers
	 *            The nr of players
	 * @param players
	 *            The players
	 */
	public void addGameTab(int id, int nrOfPlayers, String[] players) {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));
		loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));

		try {
			AnchorPane gameBoard = loader.load();
			Tab tab = new Tab("Game " + id);
			tab.setContent(gameBoard);
			tabbedPane.getTabs().add(tab);

			GameBoardController controller = loader.getController();
			games.add(new GameToBoardController(controller, id));

			controller.setUpGame(id, nrOfPlayers, players, client);
		} catch (IOException e1) {
			LOGGER.log(Level.WARNING, e1.getMessage(), e1);
		}
	}

	/**
	 * Gets the game controller
	 *
	 * @param gameId
	 *            The game id
	 * @return The game controller
	 */
	public GameBoardController getGame(int gameId) {
		for (int i = 0; i < games.size(); ++i) {
			if (games.get(i).equals(gameId)) {
				return games.get(i).controller;
			}
		}
		return null;
	}

	/**
	 * A player won the game.
	 *
	 * @param id
	 *            The player id
	 */
	public void won(int id) {
		for (int i = 0; i < tabbedPane.getTabs().size(); ++i) {
			if (tabbedPane.getTabs().get(i).getText().equals("Game " + id)) {
				int notI = i;
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						tabbedPane.getTabs().remove(notI);
					}
				});

				break;
			}
		}
		for (int i = 0; i < games.size(); ++i) {
			if (games.get(i).equals(id)) {
				games.remove(i);
				break;
			}
		}
	}

	/**
	 * A controller class to keep track of games and their controllers
	 */
	public static class GameToBoardController {

		/** The game ID. */
		private int gameID;

		/** The controller. */
		private GameBoardController controller;

		/**
		 * Instantiates a new game to board controller.
		 *
		 * @param control
		 *            The controller
		 * @param gameID
		 *            The game ID
		 */
		GameToBoardController(GameBoardController control, int gameID) {
			this.gameID = gameID;
			controller = control;
		}

		@Override
		public boolean equals(Object id) {
			if (!(id instanceof Integer)) {
				return false;
			}
			return gameID == (int) id;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return Objects.hash(gameID, controller);
		}
	}
}
