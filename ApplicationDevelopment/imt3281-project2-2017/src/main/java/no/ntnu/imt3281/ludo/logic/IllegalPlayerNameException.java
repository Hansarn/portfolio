package no.ntnu.imt3281.ludo.logic;

/**
 * IllegalPlayerNameException is a class.
 */
public class IllegalPlayerNameException extends RuntimeException {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2124461718839222204L;

	/**
	 * Instantiates a new illegal player name exception.
	 */
	IllegalPlayerNameException() {
		super("Bad name");
	}
}
