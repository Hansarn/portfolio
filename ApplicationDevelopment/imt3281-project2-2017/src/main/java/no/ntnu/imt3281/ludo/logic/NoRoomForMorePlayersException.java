package no.ntnu.imt3281.ludo.logic;


/**
 * The Class NoRoomForMorePlayersException.
 */
public class NoRoomForMorePlayersException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4522105865460179112L;

	/**
	 * Instantiates a new no room for more players exception.
	 */
	public NoRoomForMorePlayersException() {
		super("No room for more players");
	}
}
