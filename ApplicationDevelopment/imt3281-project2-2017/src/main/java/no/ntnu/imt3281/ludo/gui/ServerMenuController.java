package no.ntnu.imt3281.ludo.gui;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

/**
 * Controller for server menu
 */
public class ServerMenuController {

	/** The User list menu */
	@FXML
	private ListView<Label> userList;

	/** The chat */
	@FXML
	private TextArea chat;

	/**
	 * Adds a user to the user list
	 *
	 * @param name
	 *            the name
	 */
	public void addToList(String name) {
		userList.getItems().add(new Label(name)); 
	}

	/**
	 * Adds a message to the chat
	 *
	 * @param message
	 *            the message
	 */
	public void addChat(String message) {
		chat.appendText(message);
	}

	/**
	 * Removes the user from the user list.
	 *
	 * @param name
	 *            the name
	 */
	public void removeUser(String name) {
		for (int i = 0; i < userList.getItems().size(); ++i) {
			if (userList.getItems().get(i).getText().equals(name)) {
				userList.getItems().remove(i);
			}
		}
	}
}
