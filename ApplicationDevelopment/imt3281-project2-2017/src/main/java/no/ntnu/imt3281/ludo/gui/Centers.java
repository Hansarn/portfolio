package no.ntnu.imt3281.ludo.gui;

import java.awt.Point;

/**
 * Helper class to store all board tile positions
 */
public class Centers {

	/** The points that represent tiles */
	protected static final Point[] points = new Point[92];

	/** The tile size */
	public static final int TILESIZE = 48;
	
	/** Just helps plotting points*/
	private int helper = 16;
	
	/**
	 * Set up up all points
	 */
	public Centers() {
		for (int i = 0; i < points.length; ++i) {
			points[i] = new Point();
		}

		// Red
		points[0].setLocation(554, 74);
		points[1].setLocation(554 + TILESIZE, 74 + TILESIZE);
		points[2].setLocation(554, 74 + TILESIZE * 2);
		points[3].setLocation(554 - TILESIZE, 74 + TILESIZE);

		// Blue
		points[4].setLocation(554, 506);
		points[5].setLocation(554 + TILESIZE, 506 + TILESIZE);
		points[6].setLocation(554, 506 + TILESIZE * 2);
		points[7].setLocation(554 - TILESIZE, 506 + TILESIZE);

		// Yellow
		points[8].setLocation(122, 506);
		points[9].setLocation(122 + TILESIZE, 506 + TILESIZE);
		points[10].setLocation(122, 506 + TILESIZE * 2);
		points[11].setLocation(122 - TILESIZE, 506 + TILESIZE);

		// Green
		points[12].setLocation(122, 74);
		points[13].setLocation(122 + TILESIZE, 74 + TILESIZE);
		points[14].setLocation(122, 74 + TILESIZE * 2);
		points[15].setLocation(122 - TILESIZE, 74 + TILESIZE);

		// 16-67
		while (helper < 21) {
			points[helper].setLocation(8 * TILESIZE, (helper - 15) * TILESIZE);
			++helper;
		}
		while (helper < 27) {
			points[helper].setLocation((helper - 12) * TILESIZE, 6 * TILESIZE);
			++helper;
		}
		while (helper < 29) {
			points[helper].setLocation(14 * TILESIZE, (helper - 20) * TILESIZE);
			++helper;
		}
		while (helper < 34) {
			points[helper].setLocation((42 - helper) * TILESIZE, 8 * TILESIZE);
			++helper;
		}
		while (helper < 40) {
			points[helper].setLocation(8 * TILESIZE, (helper - 25) * TILESIZE);
			++helper;
		}
		while (helper < 42) {
			points[helper].setLocation((47 - helper) * TILESIZE, 14 * TILESIZE);
			++helper;
		}
		while (helper < 47) {
			points[helper].setLocation(6 * TILESIZE, (55 - helper) * TILESIZE);
			++helper;
		}
		while (helper < 53) {
			points[helper].setLocation((52 - helper) * TILESIZE, 8 * TILESIZE);
			++helper;
		}
		while (helper < 55) {
			points[helper].setLocation(0 * TILESIZE, (60 - helper) * TILESIZE);
			++helper;
		}
		while (helper < 60) {
			points[helper].setLocation((helper - 54) * TILESIZE, 6 * TILESIZE);
			++helper;
		}
		while (helper < 66) {
			points[helper].setLocation(6 * TILESIZE, (65 - helper) * TILESIZE);
			++helper;
		}
		while (helper < 68) {
			points[helper].setLocation((helper - 59) * TILESIZE, 0 * TILESIZE);
			++helper;
		}

		// Victory roads
		while (helper < 74) {
			points[helper].setLocation(7 * TILESIZE, (helper - 67) * TILESIZE);
			++helper;
		}
		while (helper < 80) {
			points[helper].setLocation((87 - helper) * TILESIZE, 7 * TILESIZE);
			++helper;
		}
		while (helper < 86) {
			points[helper].setLocation(7 * TILESIZE, (93 - helper) * TILESIZE);
			++helper;
		}
		while (helper < 92) {
			points[helper].setLocation((helper - 85) * TILESIZE, 7 * TILESIZE);
			++helper;
		}

		// Since we use circles, we add their radius to their positions
		for (int i = 0; i < points.length; ++i) {
			points[i].translate(TILESIZE / 2, TILESIZE / 2);
		}
	}
}
