package no.ntnu.imt3281.ludo.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.Formatter;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.gui.GameBoardController;
import no.ntnu.imt3281.ludo.gui.LoginScreenController;
import no.ntnu.imt3281.ludo.gui.LudoController;

/** 
* This is the main class for the client.
*/
public class Client extends Application {
	
	/** The connection to the server. */
	private Socket connection;
	
	/** The output stream to the server. */
	private BufferedWriter output;
	
	/** The input stream from the server. */
	private BufferedReader input;

	/** The name of the client. */
	private String name;

	/** The loader for FXML stages. */
	private FXMLLoader loader;

	/** The executor service for executing threads. */
	private ExecutorService executorService;

	/** The primary stage of the application. */
	private Stage primaryStage;

	/** The Constant LOGGER class for logging exceptions. */
	private static final Logger LOGGER = Logger.getLogger(Client.class.getName());

	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			executorService = Executors.newCachedThreadPool();
			loader = new FXMLLoader(getClass().getResource("../gui/LoginScreen.fxml"));
			loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
			Platform.setImplicitExit(true);
			Scene scene = new Scene((BorderPane) loader.load());
			this.primaryStage = primaryStage;
			primaryStage.setScene(scene);
			primaryStage.show();

			LoginScreenController control = loader.<LoginScreenController>getController();
			control.registerClient(this);
			readFile();

		} catch (Exception e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	/* (non-Javadoc)
	 * @see javafx.application.Application#stop()
	 */
	@Override
	public void stop() {

		try {
			super.stop();

			if (output != null) {
				output.close();

			}
			if (connection != null) {
				connection.close();

			}

			if (input != null) {
				input.close();

			}

			executorService.shutdown();

		} catch (Exception e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	/**
	 * The main entry point of the application.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		launch(args);

	}

	/**
	 * Connect to the server.
	 */
	public void connect() {
		try {
			if (connection == null) {
				connection = new Socket("localhost", 7591);
				output = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), StandardCharsets.UTF_16));
				input = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_16));

				messageService();
			}
		} catch (IOException e) { 
			Platform.runLater(new Runnable() {
				@Override

				public void run() {
					LoginScreenController control = loader.<LoginScreenController>getController();
					control.setLogMessage("ConnectionError:");
				}
			});
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
	}

	/**
	 * Sets the name of the client.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Starts a new thread for server messages.
	 */
	public void messageService() {
		executorService.execute(() -> {
			while (!executorService.isShutdown()) {
				try {
					String message = input.readLine();

					if (message == null) {
						message = "Lost connection to server\n";
						executorService.shutdown();
					}
					if (message.startsWith("Chat:")) {
						message = message.substring(5);
						LudoController control = loader.<LudoController>getController();
						control.displayMessage(message);
					}else if (message.startsWith("GameChat:")) {
						message = message.substring(9);
						int index = message.indexOf(":");
						String nameFromMessage = message.substring(0, index);
						int gameId = Integer.parseInt(nameFromMessage);
						message = message.substring(index + 1);

						LudoController control = loader.<LudoController>getController();
						control.getGame(gameId).displayMessage(message);

					}else if (message.startsWith("Login successfull")) {

						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								loader = new FXMLLoader(getClass().getResource("../gui/Ludo.fxml"));
								loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
								Platform.setImplicitExit(true);

								try {
									Scene scene = new Scene((Pane) loader.load());
									primaryStage.setScene(scene);
									primaryStage.show();

									LudoController control = loader.<LudoController>getController();
									control.registerClient(Client.this);

								} catch (IOException e) {
									LOGGER.log(Level.WARNING, e.getMessage(), e);
								}
							}

						});

					} else if (message.startsWith("RoomList:")) {
						String roomlist = message.substring(9);
						Platform.runLater(new Runnable() {
							@Override
							public void run() {

								String[] tempArray = roomlist.split(":");

								ChoiceDialog<String> dialog = new ChoiceDialog<String>(tempArray[0], tempArray);
								dialog.setTitle("RoomList");
								dialog.setHeaderText("Select the a room");

								Optional<String> result = dialog.showAndWait();

								LudoController control = loader.<LudoController>getController();
								control.addChatTab(result);
							}

						});
					} else if (message.startsWith("NewGame:")) {
						message = message.substring(8);

						int id = Integer.parseInt(message);

						message = input.readLine();

						int nrOfPlayers = Integer.parseInt(message);

						String[] players = new String[] { null, null, null, null };

						for (int i = 0; i < nrOfPlayers; ++i) {
							players[i] = input.readLine();
						}

						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								LudoController control = loader.<LudoController>getController();
								control.addGameTab(id, nrOfPlayers, players);
							}

						});
					} else if (message.startsWith("DiceThrow:")) {
						message = message.substring(10);
						int game = Integer.parseInt(message);
						int dice = Integer.parseInt(input.readLine());
						LudoController control = loader.<LudoController>getController();
						control.getGame(game).getLudo().throwDice(dice);
					} else if (message.startsWith("PieceMoved:")) {
						message = message.substring(11);
						int gameId = Integer.parseInt(message);
						int active = Integer.parseInt(input.readLine());
						int oldPos = Integer.parseInt(input.readLine());
						int newPos = Integer.parseInt(input.readLine());

						LudoController control = loader.<LudoController>getController();
						GameBoardController game = control.getGame(gameId);

						if (!game.checkName(active)) {
							game.getLudo().movePiece(active, oldPos, newPos);
							game.setDiceValue(-1);

						}
					} else if (message.startsWith("Won:")) {
						message = message.substring(4);
						int gameId = Integer.parseInt(message);

						LudoController control = loader.<LudoController>getController();
						control.won(gameId);
					} else if (message.startsWith("Leaver:")) {
						message = message.substring(7);
						int gameId = Integer.parseInt(message);
						String nameFromMessage = input.readLine();

						LudoController control = loader.<LudoController>getController();
						GameBoardController game = control.getGame(gameId);
						game.getLudo().removePlayer(nameFromMessage);
					} else if (message.startsWith("LogMessage:")) {
						String text = message.substring(11);

						Platform.runLater(new Runnable() {
							@Override

							public void run() {
								LoginScreenController control = loader.<LoginScreenController>getController();
								control.setLogMessage(text);
							}

						});
					} else if(message.startsWith("Challengers:")) {
						message = message.substring(12);
						
						String[] userNames = message.split(":");
						
						Platform.runLater(new Runnable() {
							@Override

							public void run() {
								LudoController control = loader.<LudoController>getController();
								control.displayChallengerList(userNames);
							}
						});
						
					} else if(message.startsWith("TopWon:")) {
						
						String topWon = message.substring(7);	

						Platform.runLater(new Runnable() {

							@Override
							public void run() {
								LudoController control = loader.<LudoController>getController();
								control.setMostWonList(topWon);
							}
						});
						
					} else if(message.startsWith("TopPlayed:")) {

						String topPlayed = message.substring(10);				
						
						Platform.runLater(new Runnable() {

							@Override
							public void run() {
								LudoController control = loader.<LudoController>getController();
								control.setMostPlayedList(topPlayed);
							}
						});
					}
				} catch (SocketException e) {
					LOGGER.log(Level.WARNING, e.getMessage(), e);
				} catch (IOException e) {
					LOGGER.log(Level.WARNING, e.getMessage(), e);
				}
			}
		});
	}

	/**
	 * Send text to the server.
	 *
	 * @param text the text to send
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void sendText(String text) throws IOException {
		output.write(text);
		output.newLine();
		output.flush();
	}

	/**
	 * Open logInInfo.txt and writes the login info, this is used to remember users.
	 *
	 * @param username the username
	 * @param password the encrypted password
	 */
	public void openFile(String username, String password) {
		Formatter outputFile = null;
		try {
			outputFile = new Formatter("./logInInfo.txt", "UTF-16");

			outputFile.format("%s %s ", username, password);
			outputFile.close();
		} catch (SecurityException securityException) {
			LOGGER.log(Level.WARNING, securityException.getMessage(), securityException);
		} catch (FileNotFoundException fileNotFoundException) {
			LOGGER.log(Level.WARNING, fileNotFoundException.getMessage(), fileNotFoundException);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	/**
	 * Read logInInfo.txt and puts the username and password into the login screen
	 */
	private void readFile() {

		Scanner inputString;
		try {
			inputString = new Scanner(new File("./logInInfo.txt"), "UTF-16");

			if (inputString.hasNext()) {
				String userName, password;

				userName = inputString.next();
				password = inputString.next();
				LoginScreenController control = loader.<LoginScreenController>getController();
				control.setUsernamePassword(userName, password);
			}

			inputString.close();

		} catch (Exception e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	/**
	 * Gets the name of the client.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Encrypts a string using Caesar Ciphering.
	 *
	 * @param string the string to be encrypted
	 * @return the encrypted string
	 */
	public static String encrypt(String string) {
		char[] charArray = string.toUpperCase().toCharArray();

		for (int i = 0; i < charArray.length; ++i) {
			charArray[i] = (char) (((int) charArray[i] + (int)charArray[0] + 989 + (charArray.length * 27) - 65) % 125 + 65);
		}

		return new String(charArray);
	}
}
