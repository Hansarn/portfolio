package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.Objects;

/**
 * The Class DiceEvent is sent every time a dice is thrown.
 */
public class DiceEvent extends EventObject implements DiceListener {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4422781464649518970L;

	/** The player. */
	private int player;

	/** The number thrown. */
	private int numberThrown;

	/**
	 * Instantiates a new dice event.
	 *
	 * @param ludo
	 *            The ludo instance associated with this event
	 * @param player
	 *            The players id
	 * @param number
	 *            Dice throw value
	 */
	public DiceEvent(Ludo ludo, int player, int number) {
		super(ludo);
		this.player = player;
		numberThrown = number;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof DiceEvent)) {
			return false;
		}
		DiceEvent event = (DiceEvent) object;
		return source == event.source && player == event.player && numberThrown == event.numberThrown;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(player, numberThrown, super.getSource());
	}

	/**
	 * Gets the player.
	 *
	 * @return the player
	 */
	public int getPlayer() {
		return player;
	}

	/**
	 * Sets the player.
	 *
	 * @param player
	 *            The new player value
	 */
	public void setPlayer(int player) {
		this.player = player;
	}

	/**
	 * Gets the number thrown.
	 *
	 * @return the number thrown
	 */
	public int getNumberThrown() {
		return numberThrown;
	}

	/**
	 * Sets the number thrown.
	 *
	 * @param numberThrown
	 *            The new number value
	 */
	public void setNumberThrown(int numberThrown) {
		this.numberThrown = numberThrown;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * no.ntnu.imt3281.ludo.logic.DiceListener#diceThrown(no.ntnu.imt3281.ludo.logic
	 * .DiceEvent)
	 */
	@Override
	public void diceThrown(DiceEvent diceEvent) {
		player = diceEvent.player;
		numberThrown = diceEvent.numberThrown;
	}

}
