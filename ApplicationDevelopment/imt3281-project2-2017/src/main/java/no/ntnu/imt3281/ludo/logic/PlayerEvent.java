package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.Objects;

/**
 * The Class PlayerEvent.
 */
public class PlayerEvent extends EventObject implements PlayerListener {

	/** The Constant PLAYING State. */
	public static final int PLAYING = 0;

	/** The Constant WAITING State. */
	public static final int WAITING = 1;

	/** The Constant LEFTGAME State. */
	public static final int LEFTGAME = 2;

	/** The Constant WON State. */
	public static final int WON = 3;

	/** The active player. */
	private int activePlayer;

	/** The state. */
	private int state;

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5281879852881454680L;

	/**
	 * Instantiates a new player event.
	 *
	 * @param ludo
	 *            The ludo instance associated with this event
	 * @param player
	 *            the playerID
	 * @param state
	 *            the players state
	 */
	public PlayerEvent(Ludo ludo, int player, int state) {
		super(ludo);
		activePlayer = player;
		this.state = state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * no.ntnu.imt3281.ludo.logic.PlayerListener#playerStateChanged(no.ntnu.imt3281.
	 * ludo.logic.PlayerEvent)
	 */
	@Override
	public void playerStateChanged(PlayerEvent event) {
		// Not used
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PlayerEvent)) {
			return false;
		}

		PlayerEvent event = (PlayerEvent) obj;
		return activePlayer == event.activePlayer && state == event.state && source == event.source;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(activePlayer, state, super.getSource());
	}

	/**
	 * Gets the active player.
	 *
	 * @return the active player
	 */
	public int getActivePlayer() {
		return activePlayer;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public int getState() {
		return state;
	}

}
