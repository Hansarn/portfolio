package no.ntnu.imt3281.ludo.logic;

/**
 * The Class NotEnoughPlayersException.
 */
public class NotEnoughPlayersException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6059726896903311886L;

	/**
	 * Instantiates a new not enough players exception.
	 */
	public NotEnoughPlayersException() {
		super("Not enough players");
	}

}
