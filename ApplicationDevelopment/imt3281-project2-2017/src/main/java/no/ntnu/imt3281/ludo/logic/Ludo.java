package no.ntnu.imt3281.ludo.logic;

import java.util.ArrayList;

/**
 * The Class Ludo acts as the ludo logic.
 */
public class Ludo {

	/** The Constant RED Player. */
	public static final int RED = 0;

	/** The Constant BLUE Player. */
	public static final int BLUE = 1;

	/** The Constant YELLOW Player. */
	public static final int YELLOW = 2;

	/** The Constant GREEN Player. */
	public static final int GREEN = 3;

	/** The number of players. */
	private int nrOfPlayers;

	/** The players name. */
	private String[] playerNames;

	/** The status of the game. */
	private String status;

	/** The active players. */
	private boolean[] activePlayers;

	/** The pieces. */
	private int[][] pieces;

	/** The blocked. */
	private boolean[] blocked;

	/** Current player. */
	private int turn;

	/** Amounts of dice thrown for current player. */
	private int thrown;

	/** The winner. */
	private int winner;

	/** The start. */
	private boolean start;

	/** The dice listeners. */
	private ArrayList<DiceListener> diceListeners;

	/** The piece listeners. */
	private ArrayList<PieceListener> pieceListeners;

	/** The player listeners. */
	private ArrayList<PlayerListener> playerListeners;

	/**
	 * Instantiates a new ludo.
	 */
	public Ludo() {
		init();
	}

	/**
	 * Instantiates a new ludo.
	 *
	 * @param player0
	 *            the name of player 0
	 * @param player1
	 *            the name of player 1
	 * @param player2
	 *            the name of player 2
	 * @param player3
	 *            the name of player 3
	 * @throws NotEnoughPlayersException
	 *             if number of players is under 2
	 */
	public Ludo(String player0, String player1, String player2, String player3) throws NotEnoughPlayersException {

		init();

		playerNames = new String[] { player0, player1, player2, player3 };

		status = "Initiated";
		for (int i = 0; i < playerNames.length; i++) {
			if (playerNames[i] != null) {
				nrOfPlayers++;
				activePlayers[i] = true;
			}
		}
		if (nrOfPlayers < 2) {
			throw new NotEnoughPlayersException();
		}

	}

	/**
	 * Instantiates a new ludo.
	 * 
	 */
	private void init() {
		turn = RED;
		thrown = 0;
		nrOfPlayers = 0;
		playerNames = new String[4];
		activePlayers = new boolean[] { false, false, false, false };
		pieces = new int[4][4];
		status = "Created";
		winner = -1;
		diceListeners = new ArrayList<DiceListener>();
		pieceListeners = new ArrayList<PieceListener>();
		playerListeners = new ArrayList<PlayerListener>();
	}

	/**
	 * Nr of players.
	 *
	 * @return number of players
	 */
	public int nrOfPlayers() {

		return nrOfPlayers;
	}

	/**
	 * Gets the player name.
	 *
	 * @param playerID
	 *            Id of the player
	 * @return the player name or null if not found
	 */
	public String getPlayerName(int playerID) {
		if (playerID >= nrOfPlayers) {
			return null;
		}

		if (!activePlayers[playerID]) {

			return "Inactive: " + playerNames[playerID];
		} else {
			return playerNames[playerID];
		}
	}

	/**
	 * Adds the player.
	 *
	 * @param playerName
	 *            Name of the new player
	 * @throws NoRoomForMorePlayersException
	 *             no room for more players exception
	 * @throws IllegalPlayerNameException
	 *             Thrown if name does not pass our filter
	 */
	public void addPlayer(String playerName) throws NoRoomForMorePlayersException, IllegalPlayerNameException {
		if (playerName.contains("SYSTEM") || playerName.length() > 20 || playerName.length() < 0) {
			throw new IllegalPlayerNameException();
		}

		if (nrOfPlayers < 4) {
			playerNames[nrOfPlayers] = playerName;
			activePlayers[nrOfPlayers] = true;
			nrOfPlayers++;
		} else {
			throw new NoRoomForMorePlayersException();
		}
		status = "Initiated";
	}

	/**
	 * Removes a player with the param name.
	 *
	 * @param playerName
	 *            The name of the player
	 * @throws NoSuchPlayerException
	 *             In case no player with that name exist
	 */
	public void removePlayer(String playerName) throws NoSuchPlayerException {
		int removedPlayer = -1;
		boolean removedTurn = false;
		for (int i = 0; i < nrOfPlayers; i++) {
			if (playerNames[i].equals(playerName)) {
				if (turn == i) {
					turn = (turn + 1) % nrOfPlayers;
					while (!activePlayers[turn]) {
						turn = (turn + 1) % nrOfPlayers;
					}
					thrown = 0;
					removedTurn = true;
				}
				activePlayers[i] = false;
				removedPlayer = i;

			}
		}
		if (removedPlayer == -1) {
			throw new NoSuchPlayerException();
		}
		for (int i = 0; i < playerListeners.size(); i++) {

			playerListeners.get(i).playerStateChanged(new PlayerEvent(this, removedPlayer, PlayerEvent.LEFTGAME));
			if (removedTurn) {
				playerListeners.get(i).playerStateChanged(new PlayerEvent(this, turn, PlayerEvent.PLAYING));
			}
		}
	}

	/**
	 * Gets the number of active players.
	 *
	 * @return number of active players
	 */
	public int activePlayers() {
		int active = 0;
		for (int i = 0; i < nrOfPlayers; i++) {
			if (activePlayers[i]) {
				active++;
			}
		}
		return active;
	}

	/**
	 * Gets the position of a players piece.
	 *
	 * @param playerID
	 *            id of player
	 * @param piece
	 *            id
	 * @return the position
	 */
	public int getPosition(int playerID, int piece) {
		return pieces[playerID][piece];
	}

	/**
	 * Active player.
	 *
	 * @return Active player id
	 */
	public int activePlayer() {
		return turn;
	}

	/**
	 * Changes the turn to the next active player.
	 */
	private void nextTurn() {
		int lastTurn = turn;
		turn = (turn + 1) % nrOfPlayers;
		while (!activePlayers[turn]) {
			turn = (turn + 1) % nrOfPlayers;
		}
		thrown = 0;

		for (int i = 0; i < playerListeners.size(); i++) {
			playerListeners.get(i).playerStateChanged(new PlayerEvent(this, lastTurn, PlayerEvent.WAITING));
			playerListeners.get(i).playerStateChanged(new PlayerEvent(this, turn, PlayerEvent.PLAYING));

		}
	}

	/**
	 * Plays out a dice throw.
	 *
	 * @param diceThrow
	 *            the dice throw
	 * @return the dice throw
	 */
	public int throwDice(int diceThrow) {

		status = "Started";
		for (int i = 0; i < diceListeners.size(); i++) {
			diceListeners.get(i).diceThrown(new DiceEvent(this, turn, diceThrow));
		}

		thrown++;
		start = true;
		blocked = new boolean[] { false, false, false, false };
		for (int i = 0; i < 4; i++) {
			if (pieces[turn][i] != 0) {
				start = false;
			} else if (diceThrow != 6) {
				blocked[i] = true;
			}
		}
		if (!start) {
			boolean stuck = false;
			for (int i = 0; i < 4; i++) {
				if (!(pieces[turn][i] + diceThrow <= 59)) {
					blocked[i] = true;
				}
			}

			for (int i = 0; i < 4; ++i) {
				if (i == turn) {
					continue;
				}
				for (int j = 0; j < 4; ++j) {
					for (int k = j + 1; k < 4; ++k) {
						if (pieces[i][j] == pieces[i][k] && pieces[i][j] != 0 && pieces[i][j] < 53) {
							for (int h = 0; h < 4; ++h) {
								if (userGridToLudoBoardGrid(i, pieces[i][j]) > userGridToLudoBoardGrid(turn,
										pieces[turn][h])
										&& userGridToLudoBoardGrid(i, pieces[i][j]) <= userGridToLudoBoardGrid(turn,
												pieces[turn][h] + diceThrow)) {
									blocked[h] = true;

								}
							}
						}
					}
				}
			}

			stuck = blocked[0] && blocked[1] && blocked[2] && blocked[3];

			if (stuck) {
				nextTurn();
			}
			if (diceThrow == 6 && thrown == 3) {
				nextTurn();
			}
		} else if (thrown == 3 && diceThrow != 6) {
			nextTurn();
		}

		return diceThrow;
	}

	/**
	 * Move piece.
	 *
	 * @param player
	 *            player that makes the move
	 * @param start
	 *            where the piece is at the start
	 * @param end
	 *            where the piece is at the end
	 * @return true, if move can be done successfully
	 */
	public boolean movePiece(int player, int start, int end) {

		boolean validMove = false;
		int currentPiece = -5;

		if (activePlayers[player]) {
			for (int i = 0; i < pieces[player].length; i++) {
				if (pieces[player][i] == start && end <= 59 && !blocked[i]) {
					if (start == 0) {
						end = 1;
					}

					currentPiece = i;
					validMove = true;
					pieces[player][i] = end;

					break;
				}
			}
		}
		if (validMove) {
			for (int i = 0; i < pieceListeners.size(); i++) {
				pieceListeners.get(i).pieceMoved(new PieceEvent(this, player, currentPiece, start, end));
			}

			if (end - start < 6 || thrown == 3 || start == 0) {
				nextTurn();
			}
			boolean done = true;
			for (int j = 0; j < 4; j++) {
				if (pieces[player][j] != 59) {
					done = false;
				}

				for (int i = 0; i < 4; ++i) {
					if (j == player && i == currentPiece) {
						continue;
					}

					if (userGridToLudoBoardGrid(j, pieces[j][i]) == userGridToLudoBoardGrid(player, end)
							&& j != player) {
						for (int k = 0; k < pieceListeners.size(); k++) {
							pieceListeners.get(k).pieceMoved(new PieceEvent(this, j, i, pieces[j][i], 0));
						}
						pieces[j][i] = 0;
					}
				}
			}
			if (done) {
				status = "Finished";
				winner = player;

				for (int i = 0; i < playerListeners.size(); i++) {
					playerListeners.get(i).playerStateChanged(new PlayerEvent(this, player, PlayerEvent.WON));

				}
			}
		}

		return validMove;
	}

	/**
	 * Worth noting that we don't actually use this function
	 * 
	 * @return the int
	 */
	public int throwDice() {
		return throwDice(1);
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Gets the winner.
	 *
	 * @return the winner
	 */
	public int getWinner() {

		return winner;
	}

	/**
	 * User grid to ludo board grid.
	 *
	 * @param player
	 *            The playersID
	 * @param pos
	 *            the players relative position on the board
	 * @return the actual position on the board
	 */
	public int userGridToLudoBoardGrid(int player, int pos) {
		final int[] conversionTableNormalBoard = new int[] { 0, 13, 26, 39 };

		final int[] conversionTableVictoryRoad = new int[] { 14, 20, 26, 32 };

		if (pos == 0) {
			return player * 4;
		} else if (pos < 54) {
			return (conversionTableNormalBoard[player] + pos - 1) % 52 + 16;
		} else {
			return (conversionTableVictoryRoad[player] + pos);
		}
	}

	/**
	 * Adds the dice listener.
	 *
	 * @param diceListener
	 *            the dice listener
	 */
	public void addDiceListener(DiceListener diceListener) {
		diceListeners.add(diceListener);

	}

	/**
	 * Adds the piece listener.
	 *
	 * @param pieceListener
	 *            the piece listener
	 */
	public void addPieceListener(PieceListener pieceListener) {
		pieceListeners.add(pieceListener);
	}

	/**
	 * Adds the player listener.
	 *
	 * @param playerListener
	 *            the player listener
	 */
	public void addPlayerListener(PlayerListener playerListener) {
		playerListeners.add(playerListener);
	}

	/**
	 * Gets the piece position.
	 *
	 * @param player
	 *            the player
	 * @param piece
	 *            the piece
	 * @return the piece position
	 */
	public int getPiecePosition(int player, int piece) {
		return pieces[player][piece];
	}

	/**
	 * Checks if is start.
	 *
	 * @return true, if is start
	 */
	public boolean isStart() {
		return start;
	}

	/**
	 * Checks if is on start.
	 *
	 * @param playerID
	 *            the player ID
	 * @return true, if is on start
	 */
	public boolean isOnStart(int playerID) {
		for (int i = 0; i < 4; i++) {
			if (pieces[playerID][i] != 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Gets the active players.
	 *
	 * @return the active players
	 */
	public boolean[] getActivePlayers() {
		return activePlayers;
	}

}
