package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * The listener interface for receiving dice events. The class that is
 * interested in processing a dice event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addDiceListener</code> method. When the dice event occurs,
 * that object's appropriate method is invoked.
 *
 * @see DiceEvent
 */
public interface DiceListener extends EventListener {

	/**
	 * add dice event.
	 *
	 * @param diceEvent
	 *            the dice event
	 */
	void diceThrown(DiceEvent diceEvent);
}
