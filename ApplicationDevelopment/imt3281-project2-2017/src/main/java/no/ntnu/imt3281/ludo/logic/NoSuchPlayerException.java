package no.ntnu.imt3281.ludo.logic;

/**
 * The Class NoSuchPlayerException.
 */
public class NoSuchPlayerException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5091427374476073040L;

	/**
	 * Instantiates a new no such player exception.
	 */
	public NoSuchPlayerException() {
		super("Nobody with that name is playing with you");
	}
}
