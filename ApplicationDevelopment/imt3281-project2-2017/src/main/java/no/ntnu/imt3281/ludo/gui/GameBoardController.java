package no.ntnu.imt3281.ludo.gui;

import java.awt.Point;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import no.ntnu.imt3281.ludo.client.Client;
import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.logic.PlayerEvent;

/**
 * Controller for the gui related to the gameboard.
 */
public class GameBoardController {

	/** The player 1 name. */
	@FXML
	private Label player1Name;

	/** The player 1 active state. */
	@FXML
	private ImageView player1Active;

	/** The player 2 name. */
	@FXML
	private Label player2Name;

	/** The player 2 active state. */
	@FXML
	private ImageView player2Active;

	/** The player 3 name. */
	@FXML
	private Label player3Name;

	/** The player 3 active state. */
	@FXML
	private ImageView player3Active;

	/** The player 4 name. */
	@FXML
	private Label player4Name;

	/** The player 4 active state. */
	@FXML
	private ImageView player4Active;

	/** The dice thrown image. */
	@FXML
	private ImageView diceThrown;

	/** The throw the dice button. */
	@FXML
	private Button throwTheDice;

	/** The chat area for ingame chat. */
	@FXML
	private TextArea chatArea;

	/** The chat box. */
	@FXML
	private TextField textToSay;

	/** The send text button. */
	@FXML
	private Button sendTextButton;

	/** The overlaying pane*/
	@FXML
	private AnchorPane pane;

	/** The pieces. */
	private Circle[][] pieces = new Circle[4][4];

	/** The tiles */
	private Centers centers = new Centers();

	/** The ludo game */
	private Ludo ludo;

	/** The client */
	private Client client;

	/** The game id */
	private int gameId;

	/** The dice value. */
	private int diceValue = -1;

	/** The hasThrown bool, used to check if the user can press throw dice again */
	private boolean hasThrown = false;
	
	/** The logger*/
	private static final Logger LOGGER = Logger.getLogger(GameBoardController.class.getName());

	/**
	 * Sets up the game.
	 *
	 * @param id The id of the game
	 * @param nrOfPlayers The number of players
	 * @param names The names of the players
	 * @param client The client
	 */
	public void setUpGame(int id, int nrOfPlayers, String[] names, Client client) {
		ludo = new Ludo(names[0], names[1], names[2], names[3]);
		this.client = client;
		this.gameId = id;

		player1Name.setText(names[0]);
		player2Name.setText(names[1]);

		if (nrOfPlayers > 2) {
			player3Name.setText(names[2]);
		}
		if (nrOfPlayers > 3) {
			player4Name.setText(names[3]);
		}

		for (int i = 0; i < 4; ++i) {
			Paint paint = null;
			switch (i) {
			case 0:
				paint = Paint.valueOf("ff0000");
				break;

			case 1:
				paint = Paint.valueOf("5050ff");
				break;

			case 2:
				paint = Paint.valueOf("ffff00");
				break;

			case 3:
				paint = Paint.valueOf("00ff00");
				break;
				
			default:
				paint = Paint.valueOf("ffff00");
				break;
			}

			for (int j = 0; j < 4; ++j) {

				pieces[i][j] = new Circle(20.0, paint);
				pieces[i][j].setCenterX(centers.points[i * 4 + j].getX() - j);
				pieces[i][j].setCenterY(centers.points[i * 4 + j].getY() - j);
				pieces[i][j].setEffect(new DropShadow());
				pieces[i][j].setOnMouseClicked(e -> clickOnPiece(e));

				pane.getChildren().add(pieces[i][j]);
			}

		}
		ludo.addDiceListener(de -> Platform.runLater(() -> {
			diceThrown.setImage(
					new Image(getClass().getResourceAsStream("/images/dice" + de.getNumberThrown() + ".png")));
			diceValue = de.getNumberThrown();
		}));
		ludo.addPlayerListener(pe -> Platform.runLater(() -> {
			changeActive(pe.getActivePlayer(), pe.getState());
			if (pe.getState() != PlayerEvent.LEFTGAME) {
				hasThrown = false;
				diceValue = -1;
			}

		}));
		ludo.addPieceListener(pl -> Platform.runLater(() -> {
			Point point = centers.points[ludo.userGridToLudoBoardGrid(pl.getPlayer(), pl.getEnd())];
			pieces[pl.getPlayer()][pl.getPiece()].setCenterX(point.getX());
			pieces[pl.getPlayer()][pl.getPiece()].setCenterY(point.getY());

		}));
		player1Active.setVisible(true);
		player2Active.setVisible(false);
		player3Active.setVisible(false);
		player4Active.setVisible(false);
	}

	/**
	 * Click on piece will move the piece if the piece can do a valid move.
	 *
	 * @param e The mouseEvent
	 */
	private void clickOnPiece(MouseEvent e) {
		if (ludo.getPlayerName(ludo.activePlayer()).equals(client.getName()) && diceValue != -1) {
			for (int x = 0; x < 4; ++x) {
				if (pieces[ludo.activePlayer()][x] == (Circle) e.getSource()) {
					int pos = ludo.getPiecePosition(ludo.activePlayer(), x);
					if (!(pos == 0 && diceValue != 6) && ludo.movePiece(ludo.activePlayer(), pos, pos + diceValue)) {
						try {
							client.sendText("GameMove:" + gameId + "\n" + pos + "\n" + (pos + diceValue));
							hasThrown = false;
							diceValue = -1;

						} catch (IOException e1) {
							LOGGER.log(Level.WARNING, e1.getMessage(), e1);
						}
						break;
					}
				}
			}
		}
	}

	/**
	 * Throw dice if the client is supposed to throw a die.
	 */
	@FXML
	public void throwDice() {
		if (ludo.getPlayerName(ludo.activePlayer()).equals(client.getName()) && (!hasThrown || (ludo.isOnStart(ludo.activePlayer()) && diceValue != 6))) {
			try {
				hasThrown = true;
				client.sendText("DiceThrow:" + gameId);
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, e.getMessage(), e);
			}
			
		}

	}

	/**
	 * Sends a message to the server
	 */
	@FXML
	public void sendMessage() {
		String currentChat = Integer.toString(gameId);
		try {
			client.sendText("GameChat:" + currentChat + ":" + textToSay.getText());
			textToSay.clear();
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	/**
	 * Display a message in the chat.
	 *
	 * @param message
	 *            The message to display
	 */
	public void displayMessage(String message) {
		String chatMessage = message;
		chatArea.appendText(chatMessage + "\n");
	}

	/**
	 * Change active player sprites.
	 *
	 * @param activePlayer
	 *            The new active player
	 * @param state
	 *            The change state
	 */
	private void changeActive(int activePlayer, int state) {
		player1Active.setVisible(false);
		player2Active.setVisible(false);
		player3Active.setVisible(false);
		player4Active.setVisible(false);
		if (state == PlayerEvent.PLAYING) {
			switch (activePlayer) {
			case 0:
				player1Active.setVisible(true);
				break;
			case 1:
				player2Active.setVisible(true);
				break;
			case 2:
				player3Active.setVisible(true);
				break;
			case 3:
				player4Active.setVisible(true);
				break;
				
			default:
				break;
			}
		}

	}

	/**
	 * Gets the ludo game.
	 *
	 * @return The ludo game
	 */
	public Ludo getLudo() {
		return ludo;
	}

	/**
	 * Check if player is also client
	 *
	 * @param player
	 *            The player id
	 * @return true, if player is also client
	 */
	public boolean checkName(int player) {
		return ludo.getPlayerName(player).equals(client.getName());
	}

	/**
	 * Sets the dice value.
	 *
	 * @param diceValue
	 *            The new dice value
	 */
	public void setDiceValue(int diceValue) {
		this.diceValue = diceValue;
	}

}