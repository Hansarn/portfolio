#include <SFML/Graphics.hpp>
#include "bug.h"
#include "enviro.h"
#include "globals.h"
#pragma once

int main()
{
	sf::Font font;
	//	Boolean to determine whether to update screen or not
	bool visual = true;
	//	Attempts to load in Consolas font
	if (!font.loadFromFile("Consolas.ttf"))
	{
		std::cout << "Couldn't find font!\n";
	}
	srand((int)time(NULL));
	sf::RenderWindow window((sf::VideoMode(WINX + 800, WINY)), "bugI/O");

	enviro Env(font);
	sf::Clock deltaClock;

	while (window.isOpen())
	{
		sf::Time dt = deltaClock.restart();
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			Env.keypressHandler(&window, &event, sf::Mouse::getPosition(window), font);
			if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::V)
			{
				visual = !visual;
			}
		}

		Env.update(&event, dt, font);
		if (visual)
		{
			window.clear();

			Env.draw(&window, font);
			window.display();
		}

		//	if (counter >= QUITTIMER)
		//	{
		//		return 0;
		//	}
	}

	return 0;
}