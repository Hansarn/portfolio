#pragma once

//DON'T CHANGE THESE
#define INPUTSIZE 18
#define OUTPUTSIZE 9

//Amount of vision pixels (don't change though)
#define VISIONPIXELS 5

//Amount and size of hidden layers, higher numbers = cooler, but also = worse performance
#define HIDDENLAYERS 3
#define HIDDENSIZE 15

//Minimum and maximum amount of bugs
#define MINBUGS 5
#define MAXBUGS 500

//How long between each birth
#define BIRTHTIMER 300
#define BIRTHRATE 0.75

//Mutation chance (percent) and change per mutation
#define MUTRATE 10
#define MUTCHANGE 0.3f

//Grass regrowth rate
#define REGROWTHRATE 0.04f

//Quit upon x ticks (~60 ticks per second)
#define QUITTIMER 30000

//Food values (doesn't account for diet multiplier)
#define ATTACKGAIN 0.06f
#define GROUNDGAIN 0.04f

//Tile amount (optimally numbers 800 is divisible by (or generally TILESX needs to be something WINX is divisible by, and same with Y (For your computer's sake, keep it below 80)))
#define TILESX 20
#define TILESY 20

//Pixelsizes
#define WINX 800
#define WINY 800