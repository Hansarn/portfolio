#include <SFML\Graphics.hpp>
#include <vector>
#include <cmath>
#include "Node.h"
#pragma once
class brain
{
public:
	std::vector<std::vector<Node>> hLayers;
	std::vector<std::vector<float>> hOutputs;
	std::vector<std::vector<float>> outv;
	std::vector<Node> outLayer;

	brain()
	{
		//	Sets up all nodes for hidden and output layers

		hLayers.push_back(std::vector<Node>());
		for (size_t i = 0; i < HIDDENSIZE; i++)
		{
			Node temp = Node(INPUTSIZE);
			hLayers.at(0).push_back(temp);
		}

		for (size_t i = 1; i < HIDDENLAYERS; i++)
		{
			hLayers.push_back(std::vector<Node>());
			for (size_t j = 0; j < HIDDENSIZE; j++)
			{
				Node temp = Node(HIDDENSIZE);
				hLayers.at(i).push_back(temp);
			}
		}

		for (size_t i = 0; i < OUTPUTSIZE; i++)
		{
			Node temp = Node(HIDDENSIZE);
			outLayer.push_back(temp);
		}
	}
	~brain();

	//	The main activity function for the brain
	std::vector<float> rollSafe(std::vector<float> in, std::vector<float> out);

	void mutate();
};
