#include "bug.h"
#include <ctime>
#include <iostream>

bug::bug(sf::Font font)
{
	//	Sets up everything related to the bug

	velocity = -1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (1 - (-1))));
	rotate = -0.5f + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0.5 - (-0.5))));
	energy = -1 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (0 - (-1))));
	float x = (float)(rand() % WINX);
	float y = (float)(rand() % WINY);
	attack = false;
	body.setRadius(6.5);
	body.setOrigin(6.5, 6.5);
	body.setPosition(x, y);
	body.setRotation(180);

	generation = 1;
	parent = 0;
	//	Vision pixels referts to the bug's 'eyes'
	for (size_t i = 0; i < VISIONPIXELS; i++)
	{
		visionPixels.push_back(sf::Vector2f(0, 0));
		vision.push_back(sf::Vector3f(0, 0, 0));
		colors.push_back(sf::Color(0, 0, 0));
	}
	visionWrap();
	for (size_t i = 0; i < INPUTSIZE; i++)
	{
		input.push_back(0);
	}
	for (size_t i = 0; i < OUTPUTSIZE; i++)
	{
		output.push_back(0);
	}
	body.setFillColor(sf::Color(255, 0, ((int)energy + 1) * 127));
	spawn = false;
	spawnTime = 0;
	line[0].position = sf::Vector2f(body.getPosition().x, body.getPosition().y);
	line[0].color = sf::Color::Blue;
	line[1].position = visionPixels.at(2);
	line[1].color = sf::Color::Blue;
	visionl[0].position = sf::Vector2f(body.getPosition().x, body.getPosition().y);
	visionl[0].color = sf::Color::White;
	visionl[1].position = visionPixels.at(0);
	visionl[1].color = sf::Color::White;
	visionr[0].position = sf::Vector2f(body.getPosition().x, body.getPosition().y);
	visionr[0].color = sf::Color::White;
	visionr[1].position = visionPixels.at(1);
	visionr[1].color = sf::Color::White;
	vore = 0;
	childrenSpawned = 0;
	//	text.setFont(font);
	text.setFillColor(sf::Color(255, 255, 255));
	text.setPosition(body.getPosition().x, body.getPosition().y);
	text.setCharacterSize(8);
	text.setString(std::to_string(0));
	bugNo = 0;
}

bug::~bug()
{
}

void bug::update(sf::Time dt, int a)
{
	//	Sets the displayed number for the bug

	std::string s = std::to_string(a);
	text.setString(std::to_string(bugNo));
	text.setFillColor(sf::Color(255, 255, 255));
	text.setCharacterSize(12);
	text.setOrigin(3, 6);
	text.setPosition(body.getPosition().x, body.getPosition().y);
	if (eat)
	{
		velocity *= 0.8f;
	}

	//	move();
	//	Consumes energy from movement
	energy -= (0.01f * abs(velocity));

	if (spawnTime < 600)
	{
		spawnTime += 1;
	}
	//	std::cout << visionPixels.at(1).x << ", " << visionPixels.at(1).y << '\n';
	visionPixels.at(0) = sf::Vector2f(body.getPosition().x + (sin(body.getRotation() + 0.4f) * 30), body.getPosition().y + (cos(body.getRotation() + 0.4f) * 30));
	visionPixels.at(1) = sf::Vector2f(body.getPosition().x + (sin(body.getRotation() - 0.4f) * 30), body.getPosition().y + (cos(body.getRotation() - 0.4f) * 30));
	visionPixels.at(2) = sf::Vector2f(body.getPosition().x + (sin(body.getRotation()) * 8), body.getPosition().y + (cos(body.getRotation()) * 8));
	visionPixels.at(3) = sf::Vector2f(body.getPosition().x + (sin(body.getRotation() + 0.4f) * 20), body.getPosition().y + (cos(body.getRotation() + 0.4f) * 20));
	visionPixels.at(4) = sf::Vector2f(body.getPosition().x + (sin(body.getRotation() - 0.4f) * 20), body.getPosition().y + (cos(body.getRotation() - 0.4f) * 20));

	//	std::cout << "g: " << (int)colors.at(1).g << '\n';
	for (size_t i = 0; i < VISIONPIXELS; i++)
	{
		vision.at(i).x = ((float)colors.at(i).r * (1.f / 127.f)) - 1;
		vision.at(i).y = ((float)colors.at(i).g * (1.f / 127.f)) - 1;
		vision.at(i).z = ((float)colors.at(i).b * (1.f / 127.f)) - 1;
	}
	//	std::cout << "o: " << vision.at(0).y << '\n';

	//	Sets the inputs for the neural network
	input.at(0) = energy;
	input.at(1) = velocity;
	input.at(2) = vore;
	int in = 2;
	for (size_t i = 0; i < VISIONPIXELS; i++)
	{
		in++;
		input.at(in) = vision.at(i).x;
		in++;
		input.at(in) = vision.at(i).y;
		in++;
		input.at(in) = vision.at(i).z;
	}

	output = bugBrain.rollSafe(input, output);
	velocity = output.at(0) * 1;
	rotate = output.at(1) * 0.03f;

	//	std::cout << output.at(2) << '\n';

	//	Performs a few checks based on results

	if (output.at(2) >= 0)
	{
		eat = true;
	}
	else
	{
		eat = false;
	}

	if (output.at(3) >= 0)
	{
		spawn = true;
	}
	if (output.at(7) >= 0)
	{
		attack = true;
	}
	else
	{
		attack = false;
	}
	vore = output.at(8);
	shade = (energy + 1) / 2;
	body.setFillColor(sf::Color((sf::Uint8)((output.at(4) + 1) * 127 * shade), (sf::Uint8)((output.at(5) + 1) * 127 * shade), (sf::Uint8)((output.at(6) + 1) * 127 * shade)));
	body.setRotation(body.getRotation() + rotate);
	body.setPosition(body.getPosition().x + (sin(body.getRotation())*velocity), body.getPosition().y + (cos(body.getRotation())*velocity));
	line[0].position = sf::Vector2f(body.getPosition().x, body.getPosition().y);
	line[1].position = visionPixels.at(2);
	visionl[0].position = sf::Vector2f(body.getPosition().x, body.getPosition().y);
	visionl[1].position = visionPixels.at(0);
	visionr[0].position = sf::Vector2f(body.getPosition().x, body.getPosition().y);
	visionr[1].position = visionPixels.at(1);
	visionWrap();
	wrap();
	energy -= 0.003f;
	if (energy < -1)
	{
		energy = -1;
	}

	//	std::cout << body.getPosition().x << ' ' << body.getPosition().y << '\n';
	//	std::cout << velocity << '\n';
	//	std::cout /*<< "inputs: " << input.at(0) << ", " << input.at(1) << ", " << input.at(2) */<< "\noutputs: " << output.at(0) << ", " << output.at(1) << ", " << output.at(2) << '\n';
}

void bug::move()
{

	//	Used to be possible to control manually, functionaly removed due to being unnecessary. Can be used for debugging.

	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		if (velocity <= 0.5f)
		{
			velocity += 0.01f;
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		if (velocity >= -0.5f)
		{
			velocity -= 0.01f;
		}
	}
	else
	{
		velocity = 0;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		rotate = 0.025f;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		rotate = -0.025f;
	}
	else
	{
		rotate = 0;
	}
	


}

void bug::wrap()
{
	//	Makes sure that if the bug tries to move outside the area it instead moves to the other side.

	if (body.getPosition().x >= WINX)
	{
		body.setPosition(0, body.getPosition().y);
	}
	if (body.getPosition().y >= WINY)
	{
		body.setPosition(body.getPosition().x, 0);
	}
	if (body.getPosition().x < 0)
	{
		body.setPosition(WINX - 1, body.getPosition().y);
	}
	if (body.getPosition().y < 0)
	{
		body.setPosition(body.getPosition().x, WINY - 1);
	}
}

void bug::setPos(int x, int y)
{
	body.setPosition((float)x, (float)y);
}

void bug::visionWrap()
{

	//	Simply ensures that when a bug tries to look outside the map, it instead looks at the other side of it.

	for (size_t i = 0; i < visionPixels.size(); i++)
	{
		if (visionPixels.at(i).x >= WINX)
		{
			visionPixels.at(i).x -= WINX;
		}
		if (visionPixels.at(i).y >= WINY)
		{
			visionPixels.at(i).y -= WINY;
		}
		if (visionPixels.at(i).x < 0)
		{
			visionPixels.at(i).x += WINX;
		}
		if (visionPixels.at(i).y < 0)
		{
			visionPixels.at(i).y += WINY;
		}
	}
}