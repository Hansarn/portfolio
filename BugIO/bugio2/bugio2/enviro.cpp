#include "enviro.h"

enviro::enviro()
{
}

enviro::enviro(sf::Font font)
{
	//	Sets up all the visuals as well as some initial values and a starting bug population
	for (size_t i = 0; i < INPUTSIZE; i++)
	{
		sf::CircleShape temp;
		temp.setFillColor(sf::Color(255, 255, 255));
		temp.setRadius(5);
		temp.setOrigin(5, 5);
		temp.setPosition(1000.0f + ((600 / (HIDDENLAYERS + 2)) / 2), ((800.0f / INPUTSIZE) * i) + ((800.0f / INPUTSIZE) / 2));
		inN.push_back(temp);
	}
	for (size_t i = 0; i < HIDDENLAYERS; i++)
	{
		hidN.push_back(std::vector<sf::CircleShape>());
		for (size_t j = 0; j < HIDDENSIZE; j++)
		{
			sf::CircleShape temp;
			temp.setFillColor(sf::Color(255, 255, 255));
			temp.setRadius(5);
			temp.setOrigin(5, 5);
			temp.setPosition(1000.0f + (((600.0f / (HIDDENLAYERS + 2.f)) * (i + 1)) + ((600.0f / (HIDDENLAYERS + 2.f)) / 2)), ((800.0f / HIDDENSIZE) * j) + ((800.0f / HIDDENSIZE) / 2));
			hidN.at(i).push_back(temp);
		}
	}
	for (size_t i = 0; i < OUTPUTSIZE; i++)
	{
		sf::CircleShape temp;
		temp.setFillColor(sf::Color(255, 255, 255));
		temp.setRadius(5);
		temp.setOrigin(5, 5);
		temp.setPosition(1000.f + (((600.f / (HIDDENLAYERS + 2.f)) * (HIDDENLAYERS + 1.f))) + ((600.0f / (HIDDENLAYERS + 2.f)) / 2), ((800.f / OUTPUTSIZE) * i) + ((800.f / OUTPUTSIZE) / 2));
		outN.push_back(temp);
	}
	for (size_t i = 0; i < INPUTSIZE; i++)
	{
		for (size_t j = 0; j < HIDDENSIZE; j++)
		{
			inV1.push_back(inN.at(i).getPosition());
			inV2.push_back(hidN.at(0).at(j).getPosition());
		}
	}
	for (size_t i = 0; i < HIDDENLAYERS - 1; i++)
	{
		hidV1.push_back(std::vector<sf::Vector2f>());
		hidV2.push_back(std::vector<sf::Vector2f>());
		for (size_t j = 0; j < HIDDENSIZE; j++)
		{
			for (size_t k = 0; k < HIDDENSIZE; k++)
			{
				hidV1.at(i).push_back(hidN.at(i).at(j).getPosition());
				hidV2.at(i).push_back(hidN.at(i + 1).at(k).getPosition());
			}
		}
	}
	for (size_t i = 0; i < HIDDENSIZE; i++)
	{
		for (size_t j = 0; j < OUTPUTSIZE; j++)
		{
			outV1.push_back(hidN.at(hidN.size() - 1).at(i).getPosition());
			outV2.push_back(outN.at(j).getPosition());
		}
	}
//	data1.open("data1.txt");
//	data2.open("data2.txt");
//	data3.open("data3.txt");
	for (size_t i = 0; i < TILESX; i++)
	{
		tilemap.push_back(std::vector<sf::Vector2f>());
		plants.push_back(std::vector<float>());
		for (size_t j = 0; j < TILESY; j++)
		{
			float x = 0;
			float y = (float)(rand() % 255);
			tilemap[i].push_back(sf::Vector2f(x, y));
			plants[i].push_back(y);
		}
	}
	avgLife = 0;
	bug temp(font);
	selected = &temp;
	background.setFillColor(sf::Color(200, 200, 200));
	button1.setFillColor(sf::Color(150, 50, 50));
	button2.setFillColor(sf::Color(50, 150, 50));
	button3.setFillColor(sf::Color(50, 50, 150));
	bugWindow.setFillColor(sf::Color(150, 150, 150));
	networkWindow.setFillColor(sf::Color(120, 120, 80));
	background.setPosition(WINX, 0);
	button1.setPosition(WINX + 15, 15);
	button2.setPosition(WINX + 15, 65);
	button3.setPosition(WINX + 15, 115);
	bugWindow.setPosition(WINX + 10, 195);
	networkWindow.setPosition(WINX + 200, 0);
	background.setSize(sf::Vector2f(200, WINY));
	button1.setSize(sf::Vector2f(170, 40));
	button2.setSize(sf::Vector2f(170, 40));
	button3.setSize(sf::Vector2f(170, 40));
	bugWindow.setSize(sf::Vector2f(180, 330));
	networkWindow.setSize(sf::Vector2f(600, WINY));
	currentBugNo = 0;
	selectedBugNo = 0;
	longestLife = 0;
	for (size_t i = bugs.size(); i < MINBUGS; i++)
	{
		bug temp(font);
		currentBugNo++;
		//	std::cout << currentBugNo << '\n';
		temp.bugNo = currentBugNo;

		bugs.push_back(temp);
	}
	extinct = false;
	extinctions = 0;
}

enviro::~enviro()
{
}

void enviro::update(sf::Event *event, sf::Time dt, sf::Font font)
{
	avgLife = 0;
	//Used to count update cycles, more accurate measure of time for environment than system time elapsed
	time++;
	int carni = 0;
	int omni = 0;
	int herbi = 0;
	int currentGen = 0;
	for (size_t i = bugs.size(); i < MINBUGS; i++)
	{
		if (!extinct)
		{
			extinct = true;
			extinctions++;
		}
		bug temp(font);
		currentBugNo++;

		temp.bugNo = currentBugNo;

		bugs.push_back(temp);
	}

	for (int i = 0; i < bugs.size(); i++)
	{

		bool capped = false;
		//	If out of energy, kill bug
		if (bugs.at(i).energy <= -1)
		{
			killBug(i);
		}

		//	Needs to go after the previous if as i could have been the last number in the list
		if (i < bugs.size())
		{
			//	According to several conditions, decides whether bug should make a child
			if (bugs.at(i).energy > BIRTHRATE && bugs.at(i).spawn && bugs.at(i).spawnTime > BIRTHTIMER && bugs.size() < MAXBUGS)
			{
				makeChild(i, font);

				extinct = false;
			}
			//	General 'maintenance'
			if (bugs.at(i).generation > currentGen)
			{
				currentGen = bugs.at(i).generation;
			}
			bugs.at(i).visionWrap();
			bugs.at(i).timeAlive++;
			avgLife += bugs.at(i).timeAlive;
			for (size_t j = 0; j < VISIONPIXELS; j++)
			{
				int x2 = (((int)bugs.at(i).visionPixels.at(j).x)) / (WINX / TILESX);
				int y2 = (((int)bugs.at(i).visionPixels.at(j).y)) / (WINY / TILESY);

				bugs.at(i).colors.at(j) = sf::Color((sf::Uint8)(tilemap.at(x2).at(y2).x), (sf::Uint8)(tilemap.at(x2).at(y2).y), (sf::Uint8)10.f);

			}
			//	Attacks a bug, which costs energy
			if (bugs.at(i).attack)
			{
				bugs.at(i).energy -= 0.01f;
			}
			//	Handles eating a bug nearby
			for (size_t j = 0; j < bugs.size(); j++)
			{
				for (size_t k = 0; k < VISIONPIXELS; k++)
				{
					if (bugs.at(i).visionPixels.at(k).x <= bugs.at(j).body.getPosition().x + bugs.at(j).body.getRadius() && bugs.at(i).visionPixels.at(k).x >= bugs.at(j).body.getPosition().x - bugs.at(j).body.getRadius() &&
						bugs.at(i).visionPixels.at(k).y <= bugs.at(j).body.getPosition().y + bugs.at(j).body.getRadius() && bugs.at(i).visionPixels.at(k).y >= bugs.at(j).body.getPosition().y - bugs.at(j).body.getRadius() &&
						i != j)
					{
						bugs.at(i).colors.at(k) = bugs.at(j).body.getFillColor();
						if (bugs.at(i).attack && k == 3)
						{
							int x4 = ((static_cast<int>(bugs.at(j).body.getPosition().x))) / (WINX / TILESX);
							int y4 = ((static_cast<int>(bugs.at(j).body.getPosition().y))) / (WINY / TILESY);
							if (tilemap.at(x4).at(y4).x >= 242)
							{
								tilemap.at(x4).at(y4).x += 10;
							}

							bugs.at(i).energy += (ATTACKGAIN * ((bugs.at(i).vore + 1) / 2));
							bugs.at(j).energy -= (0.15f * ((bugs.at(i).vore + 1) / 2));
						}
					}
				}
			}
/*			if (bugs.at(i).vore >= 0.3)
			{
				carni++;
			}
			else if (bugs.at(i).vore <= -0.3)
			{
				herbi++;
			}
			else
			{
				omni++;
			}
			*/
			eat(i);

			bugs.at(i).update(dt, i + 1);
			if (bugs.at(i).timeAlive > longestLife)
			{
				longestLife = (int)bugs.at(i).timeAlive;
			}
			if (bugs.at(i).bugNo == selectedBugNo)
			{
				selectedBugsLoc = i;
			}

		}
	}

	avgLife /= bugs.size();

	/*			//	Writes information to files every 300 update cycles, more accurate measure of time than system time due to potential hardware slowdown
					Again, disabled unless you need to write the information.
		if (t % 300 == 0)
		{
			writeToFile1(carni, omni, herbi);
			writeToFile2(currentGen);
			writeToFile3(avgLife);
		}*/
}

void enviro::eat(int i)
{
	//	Generally takes care of anything around eating grass from the ground

	//  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	//  {
	int x1 = ((static_cast<int>(bugs.at(i).body.getPosition().x))) / (WINX / TILESX);
	int y1 = ((static_cast<int>(bugs.at(i).body.getPosition().y))) / (WINY / TILESY);

	if (bugs.at(i).eat)
	{
		if (tilemap.at(x1).at(y1).x < 4 && tilemap.at(x1).at(y1).y >= 4 && bugs.at(i).energy <= 0.94)
		{
			tilemap.at(x1).at(y1).y -= 0.5;
			bugs.at(i).energy += (GROUNDGAIN * (1.0f - ((bugs.at(i).vore + 1) / 2)));
		}
		else if (tilemap.at(x1).at(y1).x >= 4 && tilemap.at(x1).at(y1).y >= 4 && bugs.at(i).energy <= 0.94)
		{
			if (bugs.at(i).vore >= 0)
			{
				tilemap.at(x1).at(y1).x -= 0.5;
				bugs.at(i).energy += (GROUNDGAIN * ((bugs.at(i).vore + 1) / 2));
			}
			else
			{
				tilemap.at(x1).at(y1).y -= 0.5;
				bugs.at(i).energy += (GROUNDGAIN * (1.0f - ((bugs.at(i).vore + 1) / 2)));
			}
		}
		else if (tilemap.at(x1).at(y1).x >= 4 && tilemap.at(x1).at(y1).y < 4 && bugs.at(i).energy <= 0.94)
		{
			tilemap.at(x1).at(y1).x -= 0.5;
			bugs.at(i).energy += (GROUNDGAIN * ((bugs.at(i).vore + 1) / 2));
		}
	}

	//  }
}

void enviro::draw(sf::RenderWindow *w, sf::Font font)
{
	//	Draws the menu and information, as well as the map and the bugs.
	w->draw(background);
	w->draw(button1);
	w->draw(button2);
	w->draw(button3);
	w->draw(bugWindow);
	w->draw(networkWindow);
	selectedBugsLoc = 0;
	for (int i = 0; i < tilemap.size(); i++)
	{
		for (int j = 0; j < tilemap.at(i).size(); j++)
		{
			if (tilemap.at(i).at(j).y < 254)
			{
				//	Handles grass regrowth
				tilemap.at(i).at(j).y += REGROWTHRATE;
			}

			sf::RectangleShape temp;
			temp.setFillColor(sf::Color((int)tilemap.at(i).at(j).x, (int)tilemap.at(i).at(j).y, 10));
			temp.setPosition((float)((i + 1) * (WINX / TILESX)), (float)((j + 1) * (WINY / TILESY)));
			temp.setOrigin((WINX / TILESX), (WINX / TILESY));
			temp.setSize(sf::Vector2f(WINX / TILESX, WINY / TILESY));
			w->draw(temp);
		}
	}
	for (int i = 0; i < bugs.size(); i++)
	{

		w->draw(bugs.at(i).body);
		w->draw(bugs.at(i).line, 2, sf::Lines);
		w->draw(bugs.at(i).visionl, 2, sf::Lines);
		w->draw(bugs.at(i).visionr, 2, sf::Lines);
		sf::Text text;
		text = bugs.at(i).text;
		text.setFont(font);
		w->draw(text);
		if (bugs.at(i).bugNo == selectedBugNo)
		{
			selectedBugsLoc = i;
			sf::Text text2;
			text2.setFont(font);
			text2.setCharacterSize(12);
			text2.setFillColor(sf::Color(0, 0, 0));
			text2.setPosition(background.getPosition().x + 18, background.getPosition().y + 200);
			text2.setString("Bug " + std::to_string(selectedBugNo));
			w->draw(text2);
			text2.setPosition(background.getPosition().x + 18, background.getPosition().y + 220);
			text2.setString("Generation " + std::to_string(bugs.at(i).generation));
			w->draw(text2);
			text2.setPosition(background.getPosition().x + 18, background.getPosition().y + 240);
			text2.setString("Parent: " + std::to_string(bugs.at(i).parent));
			w->draw(text2);
			text2.setPosition(background.getPosition().x + 18, background.getPosition().y + 260);
			text2.setString("Time alive: " + std::to_string((int)bugs.at(i).timeAlive));
			w->draw(text2);
			text2.setPosition(background.getPosition().x + 18, background.getPosition().y + 280);
			text2.setString("Children: " + std::to_string((int)bugs.at(i).childrenSpawned));
			w->draw(text2);
			text2.setPosition(background.getPosition().x + 18, background.getPosition().y + 320);
			text2.setString(" Energy: " + std::to_string(bugs.at(i).energy));
			w->draw(text2);
			text2.setPosition(background.getPosition().x + 18, background.getPosition().y + 340);
			text2.setString(" Velocity: " + std::to_string(bugs.at(i).output.at(0)));
			w->draw(text2);
			text2.setPosition(background.getPosition().x + 18, background.getPosition().y + 360);
			text2.setString(" Eat? : " + std::to_string(bugs.at(i).output.at(2)));
			w->draw(text2);
			text2.setPosition(background.getPosition().x + 18, background.getPosition().y + 380);
			text2.setString(" Reproduce? : " + std::to_string(bugs.at(i).output.at(3)));
			w->draw(text2);
			text2.setPosition(background.getPosition().x + 18, background.getPosition().y + 400);
			text2.setString(" Attack? : " + std::to_string(bugs.at(i).output.at(7)));
			w->draw(text2);
			text2.setPosition(background.getPosition().x + 18, background.getPosition().y + 420);
			text2.setString(" Diet: " + std::to_string(bugs.at(i).output.at(8)));
			w->draw(text2);

			sf::CircleShape circleTemp;
			sf::Vertex line1[2];
			sf::Vertex line2[2];
			sf::Vertex line3[2];

			circleTemp = bugs.at(i).body;
			circleTemp.setPosition(background.getPosition().x + 100, background.getPosition().y + 480);
			line1[0].position = sf::Vector2f(circleTemp.getPosition().x, circleTemp.getPosition().y);
			line1[1].position = sf::Vector2f(circleTemp.getPosition().x + (sin(circleTemp.getRotation()) * 8), circleTemp.getPosition().y + (cos(circleTemp.getRotation()) * 8));
			line2[0].position = sf::Vector2f(circleTemp.getPosition().x, circleTemp.getPosition().y);
			line2[1].position = sf::Vector2f(circleTemp.getPosition().x + (sin(circleTemp.getRotation() + 0.4f) * 35), circleTemp.getPosition().y + (cos(circleTemp.getRotation() + 0.4f) * 35));
			line3[0].position = sf::Vector2f(circleTemp.getPosition().x, circleTemp.getPosition().y);
			line3[1].position = sf::Vector2f(circleTemp.getPosition().x + (sin(circleTemp.getRotation() - 0.4f) * 35), circleTemp.getPosition().y + (cos(circleTemp.getRotation() - 0.4f) * 35));
			line1[0].color = bugs.at(i).colors.at(2);
			line1[1].color = bugs.at(i).colors.at(2);
			line2[0].color = bugs.at(i).colors.at(1);
			line2[1].color = bugs.at(i).colors.at(4);
			line3[0].color = bugs.at(i).colors.at(0);
			line3[1].color = bugs.at(i).colors.at(3);
			w->draw(circleTemp);
			w->draw(line1, 2, sf::Lines);
			w->draw(line2, 2, sf::Lines);
			w->draw(line3, 2, sf::Lines);
		}
	}
	sf::Text text;
	text.setFont(font);
	text.setFillColor(sf::Color(255, 255, 255));

	text.setPosition(button1.getPosition().x + 20, button1.getPosition().y + 5);
	text.setCharacterSize(15);
	text.setString("Nuke Population");
	w->draw(text);
	text.setPosition(button2.getPosition().x + 20, button2.getPosition().y + 5);
	text.setCharacterSize(15);
	text.setString("Reset Environment");
	w->draw(text);
	text.setPosition(button3.getPosition().x + 20, button3.getPosition().y + 5);
	text.setCharacterSize(15);
	text.setString("Clean Wipe");
	w->draw(text);
	text.setFillColor(sf::Color(0, 0, 0));
	text.setPosition(background.getPosition().x + 15, background.getPosition().y + 540);
	text.setCharacterSize(15);
	text.setString("Time: " + std::to_string(time));
	w->draw(text);
	text.setPosition(background.getPosition().x + 15, background.getPosition().y + 560);
	text.setCharacterSize(15);
	text.setString("Longest life: " + std::to_string(longestLife));
	w->draw(text);
	text.setPosition(background.getPosition().x + 15, background.getPosition().y + 580);
	text.setCharacterSize(15);
	text.setString("Bugs alive: " + std::to_string(bugs.size()));
	w->draw(text);
	text.setPosition(background.getPosition().x + 15, background.getPosition().y + 600);
	text.setCharacterSize(15);
	text.setString("Total bugs: " + std::to_string(currentBugNo));
	w->draw(text);
	text.setPosition(background.getPosition().x + 15, background.getPosition().y + 620);
	text.setCharacterSize(15);
	text.setString("Extinctions: " + std::to_string(extinctions));
	w->draw(text);
	//	std::cout << selectedBugNo << '\n';
	//	std::cout << bugs.at(selectedBugsLoc).bugBrain.hLayers.at(0).at(0).weight.size() << '\n';
	//	std::cout << inV1.size() << '\n';
	for (size_t i = 0; i < inV1.size(); i++)
	{
		sf::Vertex ver[2];

		ver[0].position = inV1.at(i);
		ver[1].position = inV2.at(i);

		ver[0].color = sf::Color((sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(0).at(i / INPUTSIZE).weight.at(i % INPUTSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(0).at(i / INPUTSIZE).weight.at(i % INPUTSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(0).at(i / INPUTSIZE).weight.at(i % INPUTSIZE) + 1) * 127));
		ver[1].color = sf::Color((sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(0).at(i / INPUTSIZE).weight.at(i % INPUTSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(0).at(i / INPUTSIZE).weight.at(i % INPUTSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(0).at(i / INPUTSIZE).weight.at(i % INPUTSIZE) + 1) * 127));
		w->draw(ver, 2, sf::Lines);
	}
	//	std::cout << "test " << hidV1.size() << '\n';
	//	std::cout << hidV1.at(0).size() << '\n';
	for (int i = 0; i < hidV1.size(); i++)
	{
		for (size_t j = 0; j < hidV1.at(i).size(); j++)
		{
			sf::Vertex ver[2];

			ver[0].position = hidV1.at(i).at(j);
			ver[1].position = hidV2.at(i).at(j);

			ver[0].color = sf::Color((sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(i + 1).at(j / HIDDENSIZE).weight.at(j % HIDDENSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(i + 1).at(j / HIDDENSIZE).weight.at(j % HIDDENSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(i + 1).at(j / HIDDENSIZE).weight.at(j % HIDDENSIZE) + 1) * 127));
			ver[1].color = sf::Color((sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(i + 1).at(j / HIDDENSIZE).weight.at(j % HIDDENSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(i + 1).at(j / HIDDENSIZE).weight.at(j % HIDDENSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.hLayers.at(i + 1).at(j / HIDDENSIZE).weight.at(j % HIDDENSIZE) + 1) * 127));
			w->draw(ver, 2, sf::Lines);
		}
	}
	for (int i = 0; i < outV1.size(); i++)
	{
		sf::Vertex ver[2];

		ver[0].position = outV1.at(i);
		ver[1].position = outV2.at(i);
		ver[0].color = sf::Color((sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.outLayer.at(i / HIDDENSIZE).weight.at(i%HIDDENSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.outLayer.at(i / HIDDENSIZE).weight.at(i%HIDDENSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.outLayer.at(i / HIDDENSIZE).weight.at(i%HIDDENSIZE) + 1) * 127));
		ver[1].color = sf::Color((sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.outLayer.at(i / HIDDENSIZE).weight.at(i%HIDDENSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.outLayer.at(i / HIDDENSIZE).weight.at(i%HIDDENSIZE) + 1) * 127), (sf::Uint8)((bugs.at(selectedBugsLoc).bugBrain.outLayer.at(i / HIDDENSIZE).weight.at(i%HIDDENSIZE) + 1) * 127));
		w->draw(ver, 2, sf::Lines);
	}
	for (int i = 0; i < inN.size(); i++)
	{
		//	std::cout << bugs.at(selectedBugsLoc).input.at(i) << '\n';
		inN.at(i).setFillColor(sf::Color((int)((bugs.at(selectedBugsLoc).input.at(i) + 1) * 127), (int)((bugs.at(selectedBugsLoc).input.at(i) + 1) * 127), (int)((bugs.at(selectedBugsLoc).input.at(i) + 1) * 127)));
		w->draw(inN.at(i));
	}
	for (int i = 0; i < hidN.size(); i++)
	{
		for (int j = 0; j < hidN.at(i).size(); j++)
		{
			hidN.at(i).at(j).setFillColor(sf::Color((int)((bugs.at(selectedBugsLoc).bugBrain.outv.at(i).at(j) + 1) * 127), (int)((bugs.at(selectedBugsLoc).bugBrain.outv.at(i).at(j) + 1) * 127), (int)((bugs.at(selectedBugsLoc).bugBrain.outv.at(i).at(j) + 1) * 127)));
			w->draw(hidN.at(i).at(j));
		}
	}
	for (int i = 0; i < outN.size(); i++)
	{
		outN.at(i).setFillColor(sf::Color((int)((bugs.at(selectedBugsLoc).output.at(i) + 1) * 127), (int)((bugs.at(selectedBugsLoc).output.at(i) + 1) * 127), (int)((bugs.at(selectedBugsLoc).output.at(i) + 1) * 127)));
		w->draw(outN.at(i));
	}
}

void enviro::keypressHandler(sf::RenderWindow *w, sf::Event *event, sf::Vector2i pos, sf::Font font)
{
	//	Handles pressing any of the menu keys and right clicking on the map

	if (event->type == sf::Event::MouseButtonReleased && event->mouseButton.button == sf::Mouse::Right && bugs.size() < MAXBUGS)
	{
		//	If you right click the map, a new bug will spawn
		bug temp(font);
		temp.body.setPosition((float)pos.x, (float)pos.y);
		currentBugNo++;
		temp.bugNo = currentBugNo;
		bugs.push_back(temp);
	}
	//	If you click the reset population button, it will kill off all current bugs. Update handles creating new bugs.
	if (pos.x >= button1.getPosition().x && pos.x <= button1.getPosition().x + button1.getSize().x && pos.y >= button1.getPosition().y && pos.y <= button1.getPosition().y + button1.getSize().y)
	{
		button1.setSize(sf::Vector2f(180, 45));
		button1.setPosition(WINX + 10, 12.5);
		button1.setFillColor(sf::Color(175, 100, 100));
		button2.setSize(sf::Vector2f(170, 40));
		button2.setPosition(WINX + 15, 65);
		button2.setFillColor(sf::Color(50, 150, 50));
		button3.setSize(sf::Vector2f(170, 40));
		button3.setPosition(WINX + 15, 115);
		button3.setFillColor(sf::Color(50, 50, 150));
		if (event->type == sf::Event::MouseButtonReleased && event->mouseButton.button == sf::Mouse::Left)
		{
			while (bugs.size() != 0)
			{
				killBug(0);
			}
			currentBugNo = 0;
		}
	}
	else if (pos.x >= button2.getPosition().x && pos.x <= button2.getPosition().x + button2.getSize().x && pos.y >= button2.getPosition().y && pos.y <= button2.getPosition().y + button2.getSize().y)
	{
		//	Resets the grass map
		button1.setSize(sf::Vector2f(170, 40));
		button1.setPosition(WINX + 15, 15);
		button1.setFillColor(sf::Color(150, 50, 50));
		button2.setSize(sf::Vector2f(180, 45));
		button2.setPosition(WINX + 10, 62.5);
		button2.setFillColor(sf::Color(100, 175, 100));
		button3.setSize(sf::Vector2f(170, 40));
		button3.setPosition(WINX + 15, 115);
		button3.setFillColor(sf::Color(50, 50, 150));
		if (event->type == sf::Event::MouseButtonReleased && event->mouseButton.button == sf::Mouse::Left)
		{
			for (size_t i = 0; i < TILESX; i++)
			{
				for (size_t j = 0; j < TILESY; j++)
				{
					tilemap.at(i).at(j) = sf::Vector2f(0, plants.at(i).at(j));
				}
			}
		}
	}
	else if (pos.x >= button3.getPosition().x && pos.x <= button3.getPosition().x + button3.getSize().x && pos.y >= button3.getPosition().y && pos.y <= button3.getPosition().y + button3.getSize().y)
	{
		//	Resets practically everything in the program
		button1.setSize(sf::Vector2f(170, 40));
		button1.setPosition(WINX + 15, 15);
		button1.setFillColor(sf::Color(150, 50, 50));
		button2.setSize(sf::Vector2f(170, 40));
		button2.setPosition(WINX + 15, 65);
		button2.setFillColor(sf::Color(50, 150, 50));
		button3.setSize(sf::Vector2f(180, 45));
		button3.setPosition(WINX + 10, 112.5);
		button3.setFillColor(sf::Color(100, 100, 175));
		if (event->type == sf::Event::MouseButtonReleased && event->mouseButton.button == sf::Mouse::Left)
		{
			while (bugs.size() != 0)
			{
				killBug(0);
			}
			for (int i = 0; i < TILESX; i++)
			{
				for (int j = 0; j < TILESY; j++)
				{
					tilemap.at(i).at(j) = sf::Vector2f(0, plants.at(i).at(j));
				}
			}
			currentBugNo = 0;
			longestLife = 0;
			time = 0;
			extinctions = 0;
		}
	}
	//	If you click on a bug's information the program will, if possible, switch the information to the bug's parent
	else if (pos.x >= bugWindow.getPosition().x && pos.x <= bugWindow.getPosition().x + bugWindow.getSize().x && pos.y >= bugWindow.getPosition().y && pos.y <= bugWindow.getPosition().y + bugWindow.getSize().y)
	{
		button1.setSize(sf::Vector2f(170, 40));
		button1.setPosition(WINX + 15, 15);
		button1.setFillColor(sf::Color(150, 50, 50));
		button2.setSize(sf::Vector2f(170, 40));
		button2.setPosition(WINX + 15, 65);
		button2.setFillColor(sf::Color(50, 150, 50));
		button3.setSize(sf::Vector2f(170, 40));
		button3.setPosition(WINX + 15, 115);
		button3.setFillColor(sf::Color(50, 50, 150));
		if (event->type == sf::Event::MouseButtonReleased && event->mouseButton.button == sf::Mouse::Left)
		{
			for (int i = 0; i < bugs.size(); i++)
			{
				if (bugs.at(i).bugNo == selectedBugNo)
				{
					for (int j = 0; j < bugs.size(); j++)
					{
						if (bugs.at(j).bugNo == bugs.at(i).parent)
						{
							selectedBugNo = bugs.at(i).parent;
							return;
						}
					}
				}
			}
		}
	}
	else
	{
		//	Sets the information shown to that of the bug you clicked
		button1.setSize(sf::Vector2f(170, 40));
		button1.setPosition(WINX + 15, 15);
		button1.setFillColor(sf::Color(150, 50, 50));
		button2.setSize(sf::Vector2f(170, 40));
		button2.setPosition(WINX + 15, 65);
		button2.setFillColor(sf::Color(50, 150, 50));
		button3.setSize(sf::Vector2f(170, 40));
		button3.setPosition(WINX + 15, 115);
		button3.setFillColor(sf::Color(50, 50, 150));
		if (event->type == sf::Event::MouseButtonReleased && event->mouseButton.button == sf::Mouse::Left)
		{
			for (int i = 0; i < bugs.size(); i++)
			{
				if (pos.x <= bugs.at(i).body.getPosition().x + bugs.at(i).body.getRadius() && pos.x >= bugs.at(i).body.getPosition().x - bugs.at(i).body.getRadius()
					&& pos.y <= bugs.at(i).body.getPosition().y + bugs.at(i).body.getRadius() && pos.y >= bugs.at(i).body.getPosition().y - bugs.at(i).body.getRadius())
				{
					selectedBugNo = bugs.at(i).bugNo;
					std::cout << bugs.at(i).bugNo << '\n';
				}
			}
		}
	}
}

/*			Functions that write data to file, necessary to record data accurately 
			but not needed unless data collection is wanted.
void enviro::writeToFile1(int a, int b, int c)
{
	data1.open("data1.txt", std::ios_base::app | std::ios_base::out);

	data1 << std::to_string(a) << "," << std::to_string(b) << "," << std::to_string(c) << '\n';

	data1.close();
}

void enviro::writeToFile2(int a)
{
	data2.open("data2.txt", std::ios_base::app | std::ios_base::out);

	data2 << std::to_string(a) << '\n';

	data2.close();
}

void enviro::writeToFile3(float a)
{
	data3.open("data3.txt", std::ios_base::app | std::ios_base::out);

	data3 << std::to_string(a) << '\n';

	data3.close();
}
*/

void enviro::killBug(int i)
{
	for (int j = i + 1; j < bugs.size(); j++)
	{
		bugs.at(j - 1) = bugs.at(j);
	}

	bugs.pop_back();
	//          std::cout << bugs.size() << '\n';
}

void enviro::makeChild(int i, sf::Font font)
{
	//	Goes through the entire process of creating a child of a parent bug, including mutation and semi-random relocation.
	int child = bugs.size() - 1;
	bugs.at(i).spawn = false;
	bugs.at(i).spawnTime = 0;
	bug temp(font);
	bugs.push_back(temp);
	bugs.at(i).energy -= 0.75;
	int x = (rand() % 160) - 80;
	int y = (rand() % 160) - 80;
	bugs.at(child).bugBrain = bugs.at(i).bugBrain;
	for (int j = 0; j < VISIONPIXELS; j++)
	{
		bugs.at(child).visionPixels.at(j);
	}
	bugs.at(child).energy = -0.5;
	bugs.at(child).vore = bugs.at(i).vore;
	bugs.at(child).body.setPosition(bugs.at(i).body.getPosition().x + x, bugs.at(i).body.getPosition().y + y);
	bugs.at(child).wrap();
	bugs.at(child).bugBrain.mutate();
	bugs.at(child).generation += bugs.at(i).generation;
	bugs.at(child).parent = bugs.at(i).bugNo;
	bugs.at(i).childrenSpawned++;
	currentBugNo++;
	bugs.at(child).bugNo = currentBugNo;
}