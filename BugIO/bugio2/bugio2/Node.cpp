#include "Node.h"
#include <iostream>

Node::Node()
{
}

Node::~Node()
{
}

float Node::compute(std::vector<float> in)
{
	//	Takes in a vector of floats already mapped to a series of predetermined weights. Activation function for this is
	//	tanH (gives an output between -1 and 1).

	float output = 0;
	for (size_t i = 0; i < in.size(); i++)
	{
		output += (in.at(i)*weight.at(i));
	}
	output += 0.3f;

	output = tanh(output);


	return output;
}


void Node::mutate()
{
	//	Mutate goes through all the weights inherited from the parent and make slight changes to a few of them on random.
	//	This is to increase genetic variety and prevent exact clones.
	int temp;
	for (size_t i = 0; i < weight.size(); i++)
	{
		temp = rand() % 100;
		if (temp < MUTRATE)
		{
			weight.at(i) += -MUTCHANGE + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MUTCHANGE - (-MUTCHANGE))));
			if (weight.at(i) > 1)
			{
				weight.at(i) = 1;
			}
			else if (weight.at(i) < -1)
			{
				weight.at(i) = -1;
			}
		}
	}
}