#include "brain.h"
#include <iostream>

brain::~brain()
{
}

std::vector<float> brain::rollSafe(std::vector<float> in, std::vector<float> out)
{
	outv.clear();

	//	The following goes through each layers, running through inputs and computing them to outputs to send to the next
	//	layer. Further information on compute() from Node.
	outv.push_back(std::vector<float>());
	for (size_t i = 0; i < HIDDENSIZE; i++)
	{
		outv.at(0).push_back(0);
		outv.at(0).at(i) = hLayers.at(0).at(i).compute(in);
	}
	for (size_t i = 1; i < HIDDENLAYERS; i++)
	{
		outv.push_back(std::vector<float>());
		for (size_t j = 0; j < HIDDENSIZE; j++)
		{
			outv.at(i).push_back(0);
			outv.at(i).at(j) = hLayers.at(i).at(j).compute(outv.at(i - 1));
		}
	}
	//	std::cout << "output:\n";
	for (size_t i = 0; i < OUTPUTSIZE; i++)
	{
		out.at(i) = outLayer.at(i).compute(outv.at(HIDDENLAYERS - 1));
		//		std::cout << out.at(i) << '\n';
	}
	//	std::cout << "break\n";
	return out;
}

void brain::mutate()
{
	for (size_t i = 0; i < HIDDENLAYERS; i++)
	{
		for (size_t j = 0; j < HIDDENSIZE; j++)
		{
			hLayers.at(i).at(j).mutate();
		}
	}
	for (size_t i = 0; i < outLayer.size(); i++)
	{
		outLayer.at(i).mutate();
	}
}