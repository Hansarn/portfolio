#include <vector>
#include "globals.h"
#include <ctime>
#include <cmath>
#include <iostream>
#pragma once
class Node
{
public:
	std::vector<float> weight;

	Node();
	Node(int size)
	{
		for (int i = 0; i < size; i++)
		{
			float temp = -10 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (10 - (-10))));
			float posneg;
			if (temp >= 0)
			{
				posneg = 1;
			}
			else
			{
				posneg = -1;
			}
			temp = ((abs(temp) - (int)abs(temp)) * posneg);

			weight.push_back(temp);
			//		std::cout << weight.at(i) << '\n';
		}
	}
	~Node();
	float compute(std::vector<float> in);
	void mutate();
};
