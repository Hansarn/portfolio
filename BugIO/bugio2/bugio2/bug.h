#include <SFML/Graphics.hpp>
#include "brain.h"
#pragma once
class bug
{
public:
	sf::CircleShape body;
	sf::Vertex line[2];
	sf::Vertex visionl[2];
	sf::Vertex visionr[2];

	std::vector<sf::Vector3f> vision;
	std::vector<sf::Vector2f> visionPixels;
	std::vector<sf::Color> colors;

	int generation;
	int parent;
	int childrenSpawned;
	float rotate;
	float velocity;
	float energy;

	brain bugBrain;
	std::vector<float> input;
	std::vector<float> output;
	bool eat;
	bool spawn;
	bool attack;
	float spawnTime;
	float shade;
	float timeAlive;
	int bugNo;
	float vore;

	sf::Text text;
	bug() {}
	bug(sf::Font font);

	~bug();
	void update(sf::Time dt, int a);
	void move();
	void wrap();
	void setPos(int x, int y);
	void visionWrap();
};
