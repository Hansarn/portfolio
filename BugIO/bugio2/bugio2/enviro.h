#include <vector>
#include <ctime>
#include <SFML\Graphics.hpp>
#include "bug.h"
#include <iostream>
#include <fstream>
#pragma once
class enviro
{
public:
//	std::ofstream data1;
//	std::ofstream data2;
//	std::ofstream data3;
	std::vector<std::vector<sf::Vector2f>> tilemap;
	std::vector<bug> bugs;
	std::vector<std::vector<float>> plants;
	std::vector<sf::CircleShape> inN;
	std::vector<std::vector<sf::CircleShape>> hidN;
	std::vector<sf::CircleShape> outN;
	std::vector<sf::Vector2f> inV1;
	std::vector<sf::Vector2f> inV2;
	std::vector<std::vector<sf::Vector2f>> hidV1;
	std::vector<std::vector<sf::Vector2f>> hidV2;
	std::vector<sf::Vector2f> outV1;
	std::vector<sf::Vector2f> outV2;

	sf::RectangleShape background;
	sf::RectangleShape button1;
	sf::RectangleShape button2;
	sf::RectangleShape button3;
	sf::RectangleShape bugWindow;
	sf::RectangleShape networkWindow;
	int currentBugNo;
	int selectedBugNo;
	int selectedBugsLoc;
	bool extinct;
	int extinctions;

	bug* selected;

	long time;
	float avgLife;
	int longestLife;

	enviro();
	enviro(sf::Font font);
	~enviro();

	void update(sf::Event *event, sf::Time dt, sf::Font font);
	void eat(int i);
	void draw(sf::RenderWindow *w, sf::Font font);
	void keypressHandler(sf::RenderWindow *w, sf::Event *event, sf::Vector2i pos, sf::Font font);
//	void writeToFile1(int a, int b, int c);
//	void writeToFile2(int a);
//	void writeToFile3(float a);
	void killBug(int i);
	void makeChild(int i, sf::Font font);
};